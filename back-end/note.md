### Create db structure

Create migration scripts
```bash
alembic revision --autogenerate
```

Use them 
```bash
alembic upgrade head
```

### Create a db backup

```bash
dcp exec db bash -c  'pg_dump -U diatomicbase -F t diatomicbase_dev | gzip >/backups/my_db-$(date +%Y-%m-%d).tar.gz'
```

### Use pipeline on cluster

The pipeline used are based on nextflow. To use it on an HPC add the following line in `~/.nextflow/config`:

```groovy
process.executor = 'theProgramManagingYourHPC'
```

the name of the HPC manager supported by nextflow is listed [here](https://www.nextflow.io/docs/latest/executor.html)
