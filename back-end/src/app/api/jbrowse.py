from typing import Dict, List
from collections import OrderedDict
from fastapi import APIRouter, HTTPException
from fastapi_sqlalchemy import db
from app import (
    RNA_SEQ_BIOPROJECT_BLACKLIST,
    RNA_SEQ_RUNS_GROUP_TABLE,
    SERVER_DATA_URL,
    LOG_FILE_PATH,
)
from app.db.models import Gene, Genome, Species
from app.api.models import Assemblies, JbrowseTrack, TextSearchIndex
from app.api.gene import (
    get_comparison_table_info,
    get_log2FC_and_pvalue,
    get_bioproject_info,
    DictReader,
)
from app.api.utils import get_species_diff_expr_path
from app.logger import create_logger


router = APIRouter()
log = create_logger("DiatOmicBase.api.jbrowse", filename=LOG_FILE_PATH)


@router.get("/assembly/{species}", response_model=List[Assemblies])
def assembly(species: str):
    species = db.session.query(Species).filter(Species.name == species).first()
    assembly = get_assemblies_data_by_name(species.assemblies, ASSEMBLIES)
    return assembly


@router.get("/tracks/{species}", response_model=List[JbrowseTrack])
def tracks(species: str):
    species = db.session.query(Species).filter(Species.name == species).first()
    tracks = get_tracks_by_assemblies_name(species.assemblies, TRACKS)
    return tracks


@router.get("/textSearchIndex/{species}", response_model=List[TextSearchIndex])
def text_search_index(species: str):
    species = db.session.query(Species).filter(Species.name == species).first()
    index = get_text_search_index_by_assemblies_name(
        species.assemblies, TEXT_SEARCH_INDEX
    )
    return index


@router.get("/defaultSession/{species}")
def defaultSession(species: str):
    species = db.session.query(Species).filter(Species.name == species).first()
    defaultSession = get_defaultSession_by_assemblies_name(
        species.assemblies,
    )
    return defaultSession


# Get the tracks for species, and the BigWig files of significant RNA_seq for the gene asked
@router.get("/tracks/{species}/{gene_id}")
def tracks_gene(species: str, gene_id: str):
    species: Species = db.session.query(Species).filter(Species.name == species).first()
    common_tracks = get_tracks_by_assemblies_name(species.assemblies, TRACKS)

    gene: Gene = db.session.query(Gene).filter(Gene.name == gene_id).first()
    specific_tracks = []
    diff_expr_path = get_species_diff_expr_path(species.name)
    diff_expression_reader = open_tsv(diff_expr_path)
    # Get the row corresponding at the gene_name given.
    # Contain all comp_id and status associated
    for row in diff_expression_reader:
        if row["GeneID"] != gene_id:
            continue
        gene_row = row

    if not gene_row:
        log.error(f"{gene_id} not found in RNA_SEQ_DIFF_EXPRESSION_OUTPUT")

    comps_id = get_comps_id_from_gene_row(gene_row)
    # For each comparison
    for comp_id in comps_id:
        # Retrieve project id, subsets name, and keyword
        (
            project_id,
            subset_1,
            subset_2,
            keyword,
            publication,
            doi,
        ) = get_comparison_table_info(comp_id)
        if not project_id:
            log.debug(f"{comp_id} not found in comparison_table, passing...")
            continue
        if project_id in RNA_SEQ_BIOPROJECT_BLACKLIST:
            log.debug(f"{project_id} is in RNA_SEQ_BIOPROJECT_BLACKLIST, passing...")
            continue
        log2FC, pvalue = get_log2FC_and_pvalue(comp_id, gene_row)
        if log2FC == "NA" or pvalue == "NA":
            continue
        if not significant(float(log2FC), float(pvalue)):
            continue
        bioproject, _, _, _, strain, _ = get_bioproject_info(project_id)
        runs = get_SRR_from_project_id_and_subset(project_id, subset_1, subset_2)
        runs = remove_runs_already_in_specific_tracks(runs, specific_tracks)
        for run in runs:
            specific_tracks.append(
                {
                    "type": "QuantitativeTrack",
                    "trackId": f'{run["sample_name"]}_{run["srr"]}',
                    "name": run["sample_name"],
                    "category": [
                        "RNA-seq",
                        keyword,
                        f"{bioproject} — {strain} — {publication}",
                    ],
                    "assemblyNames": [gene.assembly.assembly_name],
                    "adapter": {
                        "type": "BigWigAdapter",
                        "bigWigLocation": {
                            "uri": f"{SERVER_DATA_URL}/BigWig/{run['srr']}.bw"
                        },
                    },
                }
            )

    tracks = common_tracks + specific_tracks
    return tracks


def open_tsv(filename):
    try:
        reader = DictReader(open(filename, "r"), delimiter="\t")
        return reader
    except Exception as e:
        log.error(f"Error tring to read {filename}:\n{e}")


def get_comps_id_from_gene_row(gene_row) -> List[str]:
    comps_id = list(gene_row.keys())
    comps_id.remove("GeneID")
    # For each comparison 2 colums is present in diff_expression_output
    # one for the log2FoldChange, one for the pvalue.
    # This keep only the comparison name
    comps_id = list(
        set(
            [
                comp_id.replace("_log2FC", "").replace("_padj", "")
                for comp_id in comps_id
            ]
        )
    )
    return comps_id


def significant(log2FC: float, pvalue: float) -> bool:
    return pvalue < 0.05 and (log2FC < -1 or log2FC > 1)


def get_SRR_from_project_id_and_subset(project_id: str, subset_1: str, subset_2: str):
    runs_group_reader = DictReader(open(RNA_SEQ_RUNS_GROUP_TABLE, "r"), delimiter="\t")
    srrs = []
    for row in runs_group_reader:
        if row["project_id"] != project_id:
            continue
        if row["group_name"] != subset_1 and row["group_name"] != subset_2:
            continue
        srrs.append({"srr": row["run"], "sample_name": row["sample_name"]})
    return OrderedDict((frozenset(item.items()), item) for item in srrs).values()


def remove_runs_already_in_specific_tracks(runs, specific_tracks):
    list_name_tracks = [track["name"] for track in specific_tracks]
    runs_without_duplicate = [
        run for run in runs if run["sample_name"] not in list_name_tracks
    ]
    return runs_without_duplicate


def get_assemblies_data_by_name(
    assemblies: str, assemblies_data: List[Assemblies]
) -> Assemblies:
    assemblies_name = [a.assembly_name for a in assemblies]
    selected_assemblies_data = [
        assembly for assembly in assemblies_data if assembly["name"] in assemblies_name
    ]
    if selected_assemblies_data:
        return selected_assemblies_data
    raise HTTPException(
        status_code=404, detail=f"{' '.join(assemblies_name)} not found in ASSEMBLIES"
    )


def get_tracks_by_assemblies_name(
    assemblies: List[Genome], tracks: List[JbrowseTrack]
) -> List[JbrowseTrack]:
    assemblies_name = [a.assembly_name for a in assemblies]
    assemblies_in_tracks = list(tracks.keys())
    selected_tracks = [
        tracks[name] for name in assemblies_name if name in assemblies_in_tracks
    ]
    if selected_tracks:
        tracks_list_merged = [el for list in selected_tracks for el in list]
        return tracks_list_merged
    raise HTTPException(
        status_code=404, detail=f"{' '.join(assemblies_name)} not found in TRACKS"
    )


def get_text_search_index_by_assemblies_name(
    assemblies: str, textSearchIndex: List[TextSearchIndex]
) -> List[TextSearchIndex]:
    assemblies_name = [a.assembly_name for a in assemblies]
    assemblies_in_textSearchIndex = list(textSearchIndex.keys())
    selected_textSearchIndex = [
        textSearchIndex[name]
        for name in assemblies_name
        if name in assemblies_in_textSearchIndex
    ]

    if selected_textSearchIndex:
        textSearchIndex_list_merged = [
            el for list in selected_textSearchIndex for el in list
        ]
        return textSearchIndex_list_merged
    raise HTTPException(
        status_code=404, detail=f"{' '.join(assemblies_name)} not found in TRACKS"
    )


def get_defaultSession_by_assemblies_name(assemblies: str):
    assemblies_name = [a.assembly_name for a in assemblies]
    assemblies_in_defaultSession = list(DEFAULT_SESSION.keys())
    selected_defaultSession = [
        DEFAULT_SESSION[name]
        for name in assemblies_name
        if name in assemblies_in_defaultSession
    ]
    if selected_defaultSession:
        return selected_defaultSession
    raise HTTPException(
        status_code=404,
        detail=f"{' '.join(assemblies_name)} not found in DEFAULT_SESSION",
    )


ASSEMBLIES = [
    # Phaeodactylum tricornutum
    {
        "name": "ASM15095v2",
        "sequence": {
            "type": "ReferenceSequenceTrack",
            "trackId": "ASM15095v2-ReferenceSequenceTrack",
            "adapter": {
                "type": "BgzipFastaAdapter",
                "fastaLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz",
                },
                "faiLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz.fai",
                },
                "gziLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz.gzi",
                },
            },
        },
        "refNameAliases": {
            "adapter": {
                "type": "RefNameAliasAdapter",
                "location": {"uri": f"{SERVER_DATA_URL}/ASM15095v2_aliases.txt"},
            }
        },
    },
    # Thalassiosira Pseudonana
    {
        "name": "ASM14940v2",
        "sequence": {
            "type": "ReferenceSequenceTrack",
            "trackId": "ASM14940v2-ReferenceSequenceTrack",
            "adapter": {
                "type": "BgzipFastaAdapter",
                "fastaLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Tracks/Thalassiosira_pseudonana.ASM14940v2.dna.toplevel.fa.gz",
                },
                "faiLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Tracks/Thalassiosira_pseudonana.ASM14940v2.dna.toplevel.fa.gz.fai",
                },
                "gziLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Tracks/Thalassiosira_pseudonana.ASM14940v2.dna.toplevel.fa.gz.gzi",
                },
            },
        },
    },
    # Pseudo-Nitzschia
    {
        "name": "ASM90066040v1",
        "sequence": {
            "type": "ReferenceSequenceTrack",
            "trackId": "ASM90066040v1-ReferenceSequenceTrack",
            "adapter": {
                "type": "BgzipFastaAdapter",
                "fastaLocation": {
                    "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/Pseudonitzschia_multistriata.ASM90066040v1.dna.toplevel.fa.gz",
                },
                "faiLocation": {
                    "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/Pseudonitzschia_multistriata.ASM90066040v1.dna.toplevel.fa.gz.fai",
                },
                "gziLocation": {
                    "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/Pseudonitzschia_multistriata.ASM90066040v1.dna.toplevel.fa.gz.gzi",
                },
            },
        },
    },
]

TEXT_SEARCH_INDEX = {
    "ASM15095v2": [
        {
            "type": "TrixTextSearchAdapter",
            "textSearchAdapterId": "phatr-index",
            "ixFilePath": {
                "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/trix/phatr.ix",
            },
            "ixxFilePath": {
                "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/trix/phatr.ixx",
            },
            "metaFilePath": {
                "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/trix/phatr_meta.json",
            },
            "assemblyNames": ["ASM15095v2"],
        },
    ],
}

DEFAULT_SESSION = {
    # Phaeodactylum tricornutum
    "ASM15095v2": {
        "name": "Phaeo session",
        "view": {
            "id": "linearGenomeView",
            "type": "LinearGenomeView",
            "tracks": [
                {
                    "type": "ReferenceSequenceTrack",
                    "configuration": "ASM15095v2-ReferenceSequenceTrack",
                    "displays": [
                        {
                            "type": "LinearReferenceSequenceDisplay",
                            "height": 140,
                        },
                    ],
                },
                {
                    "type": "FeatureTrack",
                    "configuration": "ASM15095v2-genes",
                    "displays": [
                        {
                            "type": "LinearBasicDisplay",
                            "height": 350,
                        },
                    ],
                },
                {
                    "type": "FeatureTrack",
                    "configuration": "Phatr2_genes_filtered",
                    "displays": [
                        {
                            "type": "LinearBasicDisplay",
                            "height": 150,
                        },
                    ],
                },
            ],
        },
    },
    # Thalassiosira Pseudonana
    "ASM14940v2": {
        "name": "Thaps session",
        "view": {
            "id": "linearGenomeView",
            "type": "LinearGenomeView",
            "tracks": [
                {
                    "type": "ReferenceSequenceTrack",
                    "configuration": "ASM14940v2-ReferenceSequenceTrack",
                    "displays": [
                        {
                            "type": "LinearReferenceSequenceDisplay",
                            "height": 140,
                        },
                    ],
                },
                {
                    "type": "FeatureTrack",
                    "configuration": "ASM14940v2-genes",
                    "displays": [
                        {
                            "type": "LinearBasicDisplay",
                            "height": 350,
                        },
                    ],
                },
            ],
        },
    },
    # Pseudo-Nitzschia
    "ASM90066040v1": {
        "name": "pmultistriata session",
        "view": {
            "id": "linearGenomeView",
            "type": "LinearGenomeView",
            "tracks": [
                {
                    "type": "ReferenceSequenceTrack",
                    "configuration": "ASM90066040v1-ReferenceSequenceTrack",
                    "displays": [
                        {
                            "type": "LinearReferenceSequenceDisplay",
                            "height": 140,
                        },
                    ],
                },
                {
                    "type": "FeatureTrack",
                    "configuration": "ASM90066040v1-genes",
                    "displays": [
                        {
                            "type": "LinearBasicDisplay",
                            "height": 350,
                        },
                    ],
                },
            ],
        },
    },
}

TRACKS = {
    # Pseudo-Nitzschia
    "ASM90066040v1": [
        {
            "type": "FeatureTrack",
            "trackId": "ASM90066040v1-genes",
            "name": "Pseudo-nitzschia multistriata gene models (Ensembl)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM90066040v1"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/pmultistriata_gene_with_href.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/pmultistriata_gene_with_href.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM90066040v1-Transposable-Elements",
            "name": "Pseudo-nitzschia Repeat Elements",
            "category": [" Repeats Elements"],
            "assemblyNames": ["ASM90066040v1"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/pmultistriata_repeat_elements.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/pseudonitzschia_multistriata/Tracks/pmultistriata_repeat_elements.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
    ],
    # Thalassiosira Pseudonana
    "ASM14940v2": [
        {
            "type": "FeatureTrack",
            "trackId": "ASM14940v2-genes",
            "name": "Thaps gene models (Ensembl)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM14940v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Tracks/thaps_gene_with_href.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Tracks/thaps_gene_with_href.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM14940v2-smallRNA",
            "name": "smallRNA",
            "category": ["  smallRNA"],
            "assemblyNames": ["ASM14940v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/smallRNAs/GSM1399245_smallRNA.gff.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/smallRNAs/GSM1399245_smallRNA.gff.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "GSM1134628_Tpse_BSseq_CG",
            "name": "Tpse_BSseq_CG",
            "category": ["  Methylation"],
            "assemblyNames": ["ASM14940v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CG.bw"
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "GSM1134628_Tpse_BSseq_CHG",
            "name": "Tpse_BSseq_CHG",
            "category": ["  Methylation"],
            "assemblyNames": ["ASM14940v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHG.bw"
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "GSM1134628_Tpse_BSseq_CHH",
            "name": "Tpse_BSseq_CHH",
            "category": ["  Methylation"],
            "assemblyNames": ["ASM14940v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHH.bw"
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
    ],
    # Phaeodactylum tricornutum
    "ASM15095v2": [
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-genes",
            "name": "Phatr3 gene models (Ensembl)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr3_gene_models_with_href.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr3_gene_models_with_href.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "Phatr2_genes_filtered",
            "name": "Phatr2 gene models (JGI)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_filtered.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_filtered.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "Phatr2_genes_unfiltered",
            "name": "Phatr2 gene models unfiltered (JGI)",
            "category": ["  Genes"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_unfiltered.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_unfiltered.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-proteogenomics",
            "name": "Phaeodactylum tricornutum proteogenomics",
            "category": [" Proteogenomics"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Proteogenomics/phatr_proteogenomics_Yang.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Proteogenomics/phatr_proteogenomics_Yang.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "proteogenomics_peptides",
            "name": "Proteogenomics peptides",
            "category": [" Proteogenomics"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BedTabixAdapter",
                "bedGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Proteogenomics/proteogenomics_peptides.bed.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Proteogenomics/proteogenomics_peptides.bed.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-lincRNAs",
            "name": "Phaeodactylum tricornutum lincRNAs",
            "category": [" lincRNAs"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr_lincRNAs.gtf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr_lincRNAs.gtf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "FeatureTrack",
            "trackId": "ASM15095v2-Transposable-Elements",
            "name": "Phaeodactylum tricornutum Transposable Elements",
            "category": [" Transposable Elements"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "Gff3TabixAdapter",
                "gffGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr3_repetitive_sequences.gff3.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Tracks/phatr3_repetitive_sequences.gff3.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
            "renderer": {
                "type": "SvgFeatureRenderer",
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K4me2_lowN",
            "name": "H3K4me2_lowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K4me2_lowN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K4me2_NormalN",
            "name": "H3K4me2_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K4me2_NormalN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me2_NormalN",
            "name": "H3K9me2_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K9me2_NormalN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K27me3",
            "name": "H3K27me3",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K27me3.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9/14Ac_LowN",
            "name": "H3K9/14Ac_LowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K914Ac_LowN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9/14Ac_NormalN",
            "name": "H3K9/14Ac_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K914Ac_NormalN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me3_lowN",
            "name": "H3K9me3_lowN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K9me3_lowN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "H3K9me3_NormalN",
            "name": "H3K9me3_NormalN",
            "category": [" Histone mark"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Histone_Mark/H3K9me3_NormalN.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "LowN_BIS_methylation",
            "name": "LowN_BIS_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Methylation/LowN_BIS_methylation.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "McrBC_methylation",
            "name": "McrBC_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Methylation/McrBC_methylation.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "RepleteN_BIS_methylation",
            "name": "RepleteN_BIS_methylation",
            "category": [" Methylation"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Methylation/RepleteN_BIS_methylation.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "Dark",
            "name": "Dark",
            "category": [" Small RNA"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Dark.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "High Light",
            "name": "High Light",
            "category": [" Small RNA"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_High_Light.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "Iron Starvation",
            "name": "Iron Starvation",
            "category": [" Small RNA"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Iron_Starvation.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "Low Light",
            "name": "Low Light",
            "category": [" Small RNA"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Low_Light.bw"
                },
            },
        },
        {
            "type": "QuantitativeTrack",
            "trackId": "Normal Light",
            "name": "Normal Light",
            "category": [" Small RNA"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "BigWigAdapter",
                "bigWigLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Normal_Light.bw"
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "PT1",
            "name": "Phaeodactylum tricornutum PT1 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt1.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt1.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt2",
            "name": "Phaeodactylum tricornutum Pt2 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt2.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt2.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt3",
            "name": "Phaeodactylum tricornutum Pt3 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt3.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt3.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt4",
            "name": "Phaeodactylum tricornutum Pt4 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt4.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt4.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt5",
            "name": "Phaeodactylum tricornutum Pt5 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt5.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt5.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt6",
            "name": "Phaeodactylum tricornutum Pt6 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt6.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt6.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt7",
            "name": "Phaeodactylum tricornutum Pt7 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt7.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt7.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt8",
            "name": "Phaeodactylum tricornutum Pt8 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt8.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt8.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt9",
            "name": "Phaeodactylum tricornutum Pt9 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt9.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt9.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
        {
            "type": "VariantTrack",
            "trackId": "Pt10",
            "name": "Phaeodactylum tricornutum Pt10 Ecotype",
            "category": [" Ecotype"],
            "assemblyNames": ["ASM15095v2"],
            "adapter": {
                "type": "VcfTabixAdapter",
                "vcfGzLocation": {
                    "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt10.vcf.gz",
                },
                "index": {
                    "location": {
                        "uri": f"{SERVER_DATA_URL}/phaeodactylum_tricornutum/Ecotypes/Pt10.vcf.gz.tbi",
                    },
                    "indexType": "TBI",
                },
            },
        },
    ],
}
