from enum import Enum
from datetime import date, datetime
from typing import Any, Optional, Dict, List, Union
from pydantic import BaseModel


# TODO fix BlastInfo usage
class BlastInfo(BaseModel):
    blast_type: str
    seq: str
    evalue: Union[int, float]
    percIdentity: Union[int, float]
    ungapped: Optional[bool] = None
    wordSize: Optional[int] = None


class SortInfo(BaseModel):
    column: Optional[str]
    desc: Optional[bool]


class SeqRegion(BaseModel):
    id: int
    dna_id: int
    name: str
    length: Optional[int]
    coord_system: Optional[str]


class Species(BaseModel):
    name: str
    ensembl_name: Optional[str]
    accession: str


class GeneInfo(BaseModel):
    id: int
    name: str
    start: int
    end: int
    strand: int
    biotype: str
    source: str
    assembly_id: int
    kegg_id: Optional[int]
    description: Optional[str]
    prot_warning: Optional[bool]
    seq_region_id: int
    seq_region: SeqRegion
    alias: Any
    species: Species


class Assembly(BaseModel):
    assembly_accession: str
    assembly_date: str
    assembly_name: str
    base_pairs: Optional[int]
    genebuild_initial_release_date: str
    genebuild_last_geneset_update: str
    genebuild_method: str
    genebuild_start_date: str
    id: int


class OneSearchGene(GeneInfo):
    phatr2_names: Optional[List[str]]


class KEGG(BaseModel):
    id: int
    accession: str
    description: str


class GoTypeEnum(str, Enum):
    F = "F"
    C = "C"
    P = "P"


class GO(BaseModel):
    term: str
    type: GoTypeEnum
    annotation: str


class Domains(BaseModel):
    start: int
    end: int
    id: int
    annotation_id: int
    gene_id: int


class DomainsAnnotation(BaseModel):
    db_name: str
    description: Optional[str]
    id: int
    accession: str


class Proteogenomics(BaseModel):
    name: str
    phatr3_vs_phatr2_category: str
    potential_non_coding_gene: str
    protein_id: int
    post_translational_modification: str
    detection_in_yang_2018: bool
    gene_id: int
    id: int


class KOG(BaseModel):
    id: int
    accession: str
    description: Optional[str]
    funcatname: Optional[str]
    levelname: Optional[str]


class Alias(BaseModel):
    emlb: Optional[str]
    ncbi: Optional[str]
    ncbi_internal: Optional[str]
    uniprotkb: Optional[str]




class GeneInfoDetailled(GeneInfo):
    go: List[GO]
    KEGG: List[KEGG]
    NCBI: Optional[str]
    domains: List[Domains]
    domains_annotation: List[DomainsAnnotation]
    proteogenomics: List[Proteogenomics]
    plaza_link: str
    kog: List[KOG]
    alias: Any


class BioSamplesDragAndDrop(BaseModel):
    id: int
    name: str
    run: str


class BioProjectMetaData(BaseModel):
    BioProject: str
    Project_Id: str
    Description: str
    Nb_runs: str
    Nb_replicates: str
    Design: str
    Publication: str
    Keywords: str
    doi: str
    Strain: str
    BioSamples: List[BioSamplesDragAndDrop]
    Species: Optional[str]


class JbrowseLocation(BaseModel):
    uri: str


class AssembliesAdapter(BaseModel):
    type: str
    fastaLocation: JbrowseLocation
    faiLocation: JbrowseLocation
    gziLocation: JbrowseLocation


class AssembliesSequence(BaseModel):
    type: str
    trackId: str
    adapter: AssembliesAdapter


class Assemblies(BaseModel):
    name: str
    sequence: AssembliesSequence


class TracksIndex(BaseModel):
    location: JbrowseLocation
    indexType: str = "TBI"


class DefaultTracksAdapter(BaseModel):
    type: str = "Gff3TabixAdapter"
    gffGzLocation: JbrowseLocation
    index: TracksIndex


class VCFTracksAdapter(BaseModel):
    type: str = "Gff3TabixAdapter"
    vcfGzLocation: JbrowseLocation
    index: TracksIndex


class WiggleAdapter(BaseModel):
    type: str = "BigWig"
    bigWigLocation: JbrowseLocation


class TracksRenderer(BaseModel):
    type: str = "SvgFeatureRenderer"


class BedTracksAdapter(BaseModel):
    type: str = "BedTabixAdapter"
    bedGzLocation: JbrowseLocation
    index: TracksIndex


class JbrowseTrack(BaseModel):
    type: str = "BasicTrack"
    trackId: str = "ASM15095v2-genes"
    name: str = "Phaeodactylum tricornutum Ensembl genes"
    category: List[str] = ["Ensembl", "Genes"]
    assemblyNames: List[str] = ["ASM15095v2"]
    adapter: Union[
        DefaultTracksAdapter, VCFTracksAdapter, WiggleAdapter, BedTracksAdapter
    ]
    renderer: Optional[TracksRenderer]


class TextSearchIndex(BaseModel):
    type: str = "TrixTextSearchAdapter",
    textSearchAdapterId: str = "phatr-index"
    ixFilePath: JbrowseLocation
    ixxFilePath: JbrowseLocation
    metaFilePath: JbrowseLocation
    assemblyNames: List[str]


class GeneExpressionEnum(str, Enum):
    UP = "UP"
    DOWN = "DOWN"
    NA = "NA"
    NS = "NS"


class GeneExpression(BaseModel):
    comp: str
    comp_id: str
    keyword: str
    bioproject: str
    description: str
    status: GeneExpressionEnum
    pvalue: Union[float, str]
    log2FC: Union[float, str]
    publication: str
    doi: str
    strain: str
    design: str


class TranscriptomicsRuns(BaseModel):
    paired: bool
    name: str
    run: str
    dl: str


class TranscriptomicsComparisons(BaseModel):
    id: str
    subset1: str
    subset2: str


class DlTranscriptomics(BaseModel):
    bioproject: str
    description: str
    design: str
    publication: str
    doi: str
    runs: List[TranscriptomicsRuns]
    comparisons: List[TranscriptomicsComparisons]
    blacklisted: bool


class DiffExprAnalysisStatus(BaseModel):
    status: str
    parameter: str

class HeatmapData(BaseModel):
    id: int
    project_id: str
    comp_id: str
    subset_1: str
    subset_2: str
    keyword: str
    publication: str
    doi: str
    description: str
    species: str

class ProcessId(BaseModel):
    process_id: int
