from typing import List, Dict
from csv import DictReader
from fastapi import APIRouter
from app import (
    RNA_SEQ_BIOPROJECT_DESCRIPTION,
    RNA_SEQ_BIOPROJECT_BLACKLIST,
    REPETITIVE_ELEMENTS_TABLE,
    RNA_SEQ_RUNS_GROUP_TABLE,
    SERVER_DATA_URL,
    ECOTYPES_TABLE,
    LOG_FILE_PATH,
)
from app.logger import create_logger
from app.api.utils import transform_tsv_in_json
from app.api.models import BioProjectMetaData, BioSamplesDragAndDrop

router = APIRouter()

log = create_logger("DiatOmicBase.resources_api", LOG_FILE_PATH)


@router.get("/transcriptomics", response_model=List[BioProjectMetaData])
def rna_seq_bioproject_description(species: str = "", forResourcesPage: bool = False):
    table = open_tsv(RNA_SEQ_BIOPROJECT_DESCRIPTION)
    to_return_rna_seq_data = []
    biosample_index = 0
    for row in table:
        if species and row["species"] != species:
            continue
        # Remove blacklisted if API is not use for resources page as we keep them available on it
        if row["project_id"] in RNA_SEQ_BIOPROJECT_BLACKLIST and not forResourcesPage:
            continue
        biosamples = get_associated_biosamples(row["project_id"], biosample_index)
        # Get the last index used. Those id are needed by react-sortablejs
        if len(biosamples) > 0:
            biosample_index = biosamples[-1]["id"] + 1
        rna_data = {
            "BioProject": row["bioproject_accession"],
            "Project_Id": row["project_id"],
            "Description": row["description"],
            "Nb_runs": row["nb_runs"],
            "Nb_replicates": row["nb_replicates"],
            "Design": row["design"],
            "Publication": row["publication_resume"],
            "Keywords": row["keywords"],
            "doi": row["doi"],
            "Strain": row["strain"],
            "BioSamples": biosamples,
            "Species": row["species"],
        }
        to_return_rna_seq_data.append(rna_data)
    return to_return_rna_seq_data


def open_tsv(rna_seq_dob_path: str) -> DictReader:
    try:
        content = DictReader(open(rna_seq_dob_path, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(f"{e}\n input not found")


def get_associated_biosamples(
    project_id: int, biosample_index: int
) -> List[BioSamplesDragAndDrop]:
    table = open_tsv(RNA_SEQ_RUNS_GROUP_TABLE)
    biosample_to_return = []
    add_in_dob_present = None
    for row in table:
        if add_in_dob_present == None:
            add_in_dob_present = "add_in_dob" in list(row.keys())
        if row["project_id"] != project_id:
            continue
        if add_in_dob_present and row["add_in_dob"] == "no":
            continue
        biosample_to_return.append(
            {"id": biosample_index, "name": row["sample_name"], "run": row["run"]}
        )
        biosample_index += 1
    return biosample_to_return


@router.get("/ecotype")
def ecotypes():
    json = transform_tsv_in_json(ECOTYPES_TABLE)
    return json


@router.get("/repetitive_elements")
def repetitive_elements():
    json = transform_tsv_in_json(REPETITIVE_ELEMENTS_TABLE)
    return json


@router.get("/SERVER_DATA_URL")
def get_SERVER_DATA_URL():
    return {"url": SERVER_DATA_URL}
