import os
import re
import pandas as pd
from copy import deepcopy
from csv import DictReader
from typing import List, Dict, Callable, Optional, Any, Tuple
from sqlalchemy import desc, cast, Integer
from sqlalchemy.sql import func
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import case
from fastapi_pagination import Params, paginate
from app import RNA_SEQ_DIFF_EXPRESSION_OUTPUT
from app.db.models import (
    Gene,
    Genome,
    SeqRegion,
    GeneNameAlias,
    DomainAnnotation,
    Phatr2Proteogenomics,
)
from app.api.models import SortInfo


def sort_paginate_format_search_response(
    genes_id: List[Tuple[int]],
    params: Params,
    sortInfo: SortInfo,
    session: Session,
    additional_attributes_format: List[str] = [],
):
    sort_the_final_result = True if not sortInfo.column else False
    cleaned_id = remove_tuple_and_duplicates(genes_id)
    sorted_id = sort_genes_id(cleaned_id, sortInfo, session)
    # Paginate on the id before fetch, to be able to fetch only the needed information after
    # while all the information about total number of answer/page size/page index are correct
    # and avoid to fetch 400 pages of genes when only one is needed
    query_result = paginate(sorted_id, params)
    # Fetch the needed genes, query_result.items contains exactly one page of genes_id.
    genes = get_genes_from_genes_id(query_result.items, session)
    # Format the response and put it in place of the genes_id.
    # This way the pagination information will be correct,
    # while only the data for the page of interest is loaded
    query_result.items = format_gene_response(
        genes, additional_attributes_format, sort_the_final_result
    )
    return query_result


def remove_tuple_and_duplicates(list_tuple: List[Tuple[Any]]) -> List[Any]:
    list_without_tuple = [el for el, in list_tuple]
    return pd.unique(list_without_tuple).tolist()


def sort_genes_id(
    genes_id: List[int], sortInfo: SortInfo, session: Session
) -> List[int]:
    if not genes_id:
        return genes_id
    if sortInfo.column == "seq_region.name":
        res = (
            session.query(Gene.id)
            .join(Gene.seq_region)
            .filter(Gene.id.in_(genes_id))
            .order_by(
                # Cast the numerial part of chrom name in integer to have a pseudo natural sort
                desc_or_not(
                    cast(func.substring(SeqRegion.name, "^[0-9]+"), Integer), sortInfo
                ),
                func.coalesce(SeqRegion.name, "[^0-9_].*$", ""),
            )
        )

    elif sortInfo.column == "description":
        res = (
            session.query(Gene.id)
            .filter(Gene.id.in_(genes_id))
            .order_by(desc_or_not(Gene.search_description, sortInfo))
        )
    elif sortInfo.column == "species":
        res = (
            session.query(Gene.id)
            .join(Gene.assembly)
            .filter(Gene.id.in_(genes_id))
            .order_by(desc_or_not(Genome.species, sortInfo))
        )
    elif sortInfo.column == "phatr2":
        res = (
            session.query(Gene.id)
            .join(Gene.proteogenomics)
            .filter(Gene.id.in_(genes_id))
            .order_by(desc_or_not(Phatr2Proteogenomics.name, sortInfo))
        )
    elif sortInfo.column == "ncbi":
        res = (
            session.query(GeneNameAlias.gene_id)
            .filter(GeneNameAlias.gene_id.in_(genes_id), GeneNameAlias.origin == "ncbi")
            .order_by(desc_or_not(GeneNameAlias.name, sortInfo))
        )
    elif sortInfo.column == "name":
        res = (
            session.query(Gene.id)
            .filter(Gene.id.in_(genes_id))
            .order_by(desc_or_not(Gene.name, sortInfo))
        )
    elif sortInfo.column == "size":
        res = (
            session.query(Gene.id)
            .filter(Gene.id.in_(genes_id))
            .order_by(desc_or_not(Gene.end - Gene.start, sortInfo))
        )
    else:
        # if no sort required
        return genes_id

    cleaned_id = remove_tuple_and_duplicates(res)
    return cleaned_id


def desc_or_not(model, sortInfo: SortInfo):
    if sortInfo.desc:
        return desc(model)
    else:
        return model


def get_genes_from_genes_id(genes_id: List[int], session: Session) -> List[Gene]:
    if not session:
        raise ValueError("Need a Session object to access db")
    if not genes_id:
        return []
    # To keep the order of the sorting when fetching the genes
    ordering = case({id: index for index, id in enumerate(genes_id)}, value=Gene.id)
    return session.query(Gene).filter(Gene.id.in_(genes_id)).order_by(ordering).all()


def format_gene_response(
    genes: List[Gene], additional_attributes: List[str] = [], sort=False
) -> List[Dict[str, str]]:
    # Obtain the a dict of the value of the gene, deleting the sqlalchemy internal variable
    response_object = []
    for gene in genes:
        seq_region = deepcopy(gene.seq_region.__dict__)
        gene_dict = deepcopy(gene.__dict__)
        species = deepcopy(gene.species.__dict__)
        del gene_dict["_sa_instance_state"]
        del seq_region["_sa_instance_state"]
        del species["_sa_instance_state"]
        del species["id"]
        del gene_dict["name_alias"]
        gene_dict["seq_region"] = seq_region
        gene_dict["alias"] = gene.alias.to_dict()
        gene_dict["species"] = species
        if "go" in additional_attributes:
            gene_dict["goTerms"] = get_list_go_term(gene.go)
        if "domain_annotation" in additional_attributes:
            if "interpro" in additional_attributes:
                gene_dict[
                    "domainsAnnotationAccession"
                ] = get_list_domain_annotation_term(
                    gene.domains_annotation, filter_interpro_only
                )
            else:
                gene_dict[
                    "domainsAnnotationAccession"
                ] = get_list_domain_annotation_term(gene.domains_annotation)
        if "kegg" in additional_attributes:
            keggs = "; ".join([k.accession for k in gene.kegg])
            gene_dict["keggAccession"] = keggs
        if "phatr2" in additional_attributes:
            gene_dict["phatr2_names"] = get_phatr2_names(gene)
        response_object.append(gene_dict)
    # Sort by Chromosome
    if sort:
        response_object = sorted(
            response_object, key=lambda gene: natural_keys(gene["seq_region"]["name"])
        )
    return response_object


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    return [atoi(c) for c in re.split(r"(\d+)", text)]


def get_list_go_term(list_go):
    go_term = []
    if not list_go:
        return []
    for go in list_go:
        go_term.append(go.term)
    return go_term


def get_list_domain_annotation_term(list_domain_annotation, filter_fn=None):
    domain_annotation_accession = []
    if not list_domain_annotation:
        return []
    for domain_annotation in list_domain_annotation:
        if filter_fn and not filter_fn(domain_annotation):
            continue
        domain_annotation_accession.append(domain_annotation.accession)
    return domain_annotation_accession


def get_phatr2_names(gene: Gene) -> List[str]:
    proteogenomics: List[Phatr2Proteogenomics] = gene.proteogenomics
    return [proteo.name for proteo in proteogenomics]


def get_dict_from_list_of_objects(
    list_object, filter_function: Optional[Callable[[str], bool]] = None
):
    """Transform a list of object in list of dict.
    Can be given a filter function to remove unwanted objects

    Args:
        list_object (List): List of SQLAlchemy Objects
        filter_function (Callable[[str], bool], optional): A filter function,
            who return False if the object passed in argument should be removed, else True.
            Defaults to None.

    Returns:
        List[Dict]: A List of Dict with all the data from objects who weren't remove by the filter function
    """
    objects = []
    if len(list_object) == 0:
        return objects
    for obj in list_object:
        if filter_function and not filter_function(obj):
            continue
        temp_obj = deepcopy(obj.__dict__)
        del temp_obj["_sa_instance_state"]
        objects.append(temp_obj)
    return objects


def filter_interpro_only(domain_annotation: DomainAnnotation):
    if domain_annotation.db_name == "InterPro":
        return True
    else:
        return False


def transform_tsv_in_json(filename: str) -> Dict[str, str]:
    json = [
        {k: v for k, v in row.items()}
        for row in DictReader(open(filename), delimiter="\t", skipinitialspace=True)
    ]
    return json


def contain_mutilple_words(text: str) -> bool:
    return text.count(" ") or text.count("_")


def get_species_diff_expr_path(species_name: str) -> str:
    """As diff_expr file are species specific, return the one

    Args:
        species_name (str): The species name used to get file name

    Returns:
        str: The path to the diff_expr file
    """
    return os.path.join(
        RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
        species_name.lower().replace("-", "").replace(" ", "_")
        + "_diff_expression.tsv",
    )
