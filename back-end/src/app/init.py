ENSEMBL_REST = "http://rest.ensembl.org/"
UNIPROT_API = "https://www.uniprot.org/uniprot/"
EMAIL = "test@mail.com"

# Used by RNASeqManager to find DESeq2_precomp.R and compute diff expression analysis
DIATOMICBASE_PATH = "/users/mpb/zweig/diatomicbase-back-end"

DATABASE_URL = "postgresql://diatomicbase:diatomicbase@localhost:5432/diatomicbase_dev"
DATABASE_TEST_URL = "sqlite://"

LOG_FILE_PATH = "back_end.log"
API_DATA_FOLDER_PATH = "../data"
# Pipeline argument
PIPELINE_CPUS = 2
PIPELINE_MAX_RAM = 7
# The gtf files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
GTF_FILES = {
    "Phaeodactylum tricornutum": "Phaeodactylum_tricornutum.ASM15095v2.47.gtf",
    "Thalassiosira pseudonana": "Thalassiosira_pseudonana.ASM14940v2.52.gtf",
    "Pseudo-nitzschia multistriata": "Pseudonitzschia_multistriata.ASM90066040v1.53.gtf",
}
# The tx2gene files are expected to be found in RNA_SEQ_DOWNLOAD_DIR/genomes
TX2GENE_FILES = [
    {
        "species": "Phaeodactylum tricornutum",
        "filename": "tx2gene.csv",
    }
]
USE_FASTERQ = True
FASTERQ_TMPDIR = "/localtmp"
RNA_SEQ_COMPARISON_TABLE = "../data/rna_seq_comparisons.tsv"  # In TSV
RNA_SEQ_DIFF_EXPRESSION_OUTPUT = "../data/diff_expr/"  # Path to diff_expr of species
# Path to manually curated rna-seq metadata file
RNA_SEQ_BIOPROJECT_DESCRIPTION = "../data/rna_seq_bioproject_description.tsv"
RNA_SEQ_RUNS_GROUP_TABLE = "../data/rna_seq_runs_group.tsv"
RNA_SEQ_ENV = "singularity"  # Use by nfcore/rna-seq to determine if it should use docker/singularity/conda
RNA_SEQ_ALIGNER = "hisat2"  # Choice between STAR and hisat2
RNA_SEQ_WORK_DIR = "/usr/rna_work_dir"
RNA_SEQ_DOWNLOAD_DIR = "/usr/src/app/app/pipeline/downloads/RNA_seq"
RNA_SEQ_BIOPROJECT_BLACKLIST = [
    "101889",
    "242990",
    "310815",
    "349060",
]  # Blacklist of bioproject from processing due to weird behavior (ex: having only one fastq dl in paired rna_seq)
NXF_SINGULARITY_CACHEDIR = (
    ""  # Where nextflow stock singularity image to avoid duplication
)
SINGULARITY_TMPDIR = ""

# BLAST
BLAST_WORK_DIR = "../data/BLAST/"

# R analysis
USER_R_ANALYSIS = "../data/R_analysis/"

# COEXPRESSION NETWORK (tsv)
GENE_TO_MODULEPHAEONET = (
    "../data/phaeodactylum_tricornutum/data_for_db/gene2modulePhaeonet.tsv"
)
MODULEPHAEONET_TO_KEGG = (
    "../data/phaeodactylum_tricornutum/data_for_db/modulesPhaeonet2KEGG.tsv"
)

# Ecotypes resources data for phaeo (tsv)
ECOTYPES_TABLE = "../data/phaeodactylum_tricornutum/resources/DOB_ecotypes.tsv"

# Transposons data for resources (tsv)
REPETITIVE_ELEMENTS_TABLE = (
    "../data/phaeodactylum_tricornutum/resources/DOB_transposons.tsv"
)

# URL to the jbrowse data not handled by FastAPI server (no slash at the end)
SERVER_DATA_URL = "http://localhost:5000"

# Number of CPUs (and process) that background_worker can use
PROCESS_CPUS = 2

PHAEONET_DATA_TSV = "../data/phaeodactylum_tricornutum/resources/data_from_phaeonet.tsv"


# Gprofiler info, add new species equivalent here, needed for Kegg automatic loader
GPROFILER_SPECIES_EQUIVALENT = {
    "Phaeodactylum tricornutum": "ptricornutum",
    "Thalassiosira pseudonana": "tpseudonana",
    "Pseudo-nitzschia multistriata": "pmultistriata",
}

# PLAZA data and instance URL
PLAZA_INSTANCE = "https://bioinformatics.psb.ugent.be/plaza/versions/plaza_diatoms_01/"
PLAZA_SPECIES_EQUIVALENT = {
    "Phaeodactylum tricornutum": "ptri",
    "Thalassiosira pseudonana": "tps",
    "Pseudo-nitzschia multistriata": "pmu",
}

