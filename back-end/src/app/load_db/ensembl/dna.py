from typing import List
from app.db.models import Dna, Genome, GenomeTopLevelRegion, SeqRegion, Species
from app.load_db.ensembl.utils import EnsemblRetrieveInfo


class EnsemblRetrieveGenomeSequence(EnsemblRetrieveInfo):
    def __init__(self, session):
        super().__init__(session)

    def fetch_sequence_from_region(self, species: Species, region: str) -> str:
        sequence = self.fetch_ensembl(
            f"sequence/region/{species.ensembl_name}/{region}"
        )
        return sequence["seq"]

    def dna_seq_is_valid(self, seq: str) -> bool:
        char_allowed = set("ATGCN")
        if set(seq) <= char_allowed:
            return True
        else:
            return False

    def insert_seq_in_db(self, sequence: str) -> int:
        dna = Dna(sequence=sequence)
        self.session.add(dna)
        self.session.commit()
        self.session.refresh(dna)
        return dna.id

    def select_last_genome(self, genomes: List[Genome]) -> Genome:
        last = 0
        latest_index = 0
        for i, genome in enumerate(genomes):
            if genome.id > last:
                last = genome.id
                latest_index = i
        return genomes[latest_index]

    def update_seq_region(self, region_id: str, chrom_id: int):
        seq_region = (
            self.session.query(SeqRegion).filter(SeqRegion.id == region_id).first()
        )
        seq_region.dna_id = chrom_id
        self.session.commit()

    def run(self, species_name: str):
        """
        Using the species name, this function will retrieve the last assembly
        in the db and extract the name of the regions associated (chromosome or contig).
        It will next retrieve the regions's sequences using the ensembl's REST API,
        and insert them in the db.
        """
        species = self.session.query(Species).filter_by(name=species_name).first()
        genomes = species.assemblies
        for genome in genomes:
            # genome = self.select_last_genome(genomes)
            top_level_region: List[GenomeTopLevelRegion] = genome.top_level_region
            for chrom in top_level_region:
                if chrom.seq_region.dna_id:
                    print(f"{chrom.seq_region.name} sequence already in db, skipping")
                    continue
                else:
                    print(f"Adding {chrom.seq_region.name} sequence in db")
                seq = self.fetch_sequence_from_region(species, chrom.seq_region.name)
                if self.dna_seq_is_valid(seq):
                    chrom_id = self.insert_seq_in_db(seq)
                    self.update_seq_region(chrom.seq_region_id, chrom_id)
                else:
                    raise TypeError("Fetched sequence is not DNA")
