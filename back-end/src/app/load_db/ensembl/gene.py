import re
from typing import List, Dict, Optional
from sqlalchemy.orm import Session
from app.db.models import Gene
from app.load_db.utils import (
    get_seq_regions,
    select_last_genome,
    get_species_by_name,
    select_genomes_from_species,
    get_last_assembly_id_by_name,
    get_assembly_name_from_gene_name,
)
from app.load_db.ensembl.utils import fetch_ensembl


def fetch_genes_from_region(species: str, region: str):
    genes = fetch_ensembl(f"overlap/region/{species}/{region}?feature=gene")
    return genes


def extract_EMBL_UniProtKB_id(description: str):
    if not description:
        return ("", "")

    embl, uniprotkb = "", ""

    embl_match = re.search(r"EMBL:([^}]*)", description)
    uniprotkb_match = re.search(r"UniProtKB/TrEMBL;Acc:([^\]]*)", description)

    if not uniprotkb_match:
        uniprotkb_match = re.search(r"UniProtKB/Swiss-Prot;Acc:([^\]]*)", description)

    if embl_match:
        embl = embl_match.group(1)
    if uniprotkb_match:
        uniprotkb = uniprotkb_match.group(1)

    return embl, uniprotkb


def insert_genes_in_db(session: Session, genes, genes_additional_info):
    genes_objects = []
    for gene, gene_info in zip(genes, genes_additional_info):
        gene_object = get_gene_by_name(session, gene["id"])
        if not gene_object:
            print(f"Inserting {gene['id']} in db")
            gene_object = Gene()
        else:
            print(f"Updating {gene_object.name}")

        gene_object.name = gene["id"]
        gene_object.start = gene["start"]
        gene_object.end = gene["end"]
        gene_object.biotype = gene["biotype"]
        gene_object.strand = gene["strand"]
        gene_object.source = gene["source"]
        gene_object.description = gene["description"]
        gene_object.search_description = format_search_description(gene["description"])
        gene_object.assembly_id = gene_info["assembly_id"]
        gene_object.seq_region_id = gene_info["region_id"]
        gene_object.species_id = gene_info["species_id"]

        # Saved also as ensembl id
        gene_object.alias["ensembl"] = gene["id"]

        if gene_info["embl"]:
            gene_object.alias["embl"] = gene_info["embl"]
        if gene_info["uniprotkb"]:
            gene_object.alias["uniprotkb"] = gene_info["uniprotkb"]

        genes_objects.append(gene_object)
    session.add_all(genes_objects)
    session.commit()


def get_gene_by_name(session: Session, name: str) -> Optional[Gene]:
    return session.query(Gene).filter(Gene.name == name).first()


def retrieve_genes_and_insert_in_db(session: Session, species: str):
    # Ensembl REST API allow to retrieve feature (gene/transcript/exon/feature)
    # from species name only by region (chromosome/contig). This code will
    # retrieve the region name and fetch the feature region by region
    # to insert them in db
    species = get_species_by_name(session, species)
    genomes = select_genomes_from_species(session, species.name)
    genome = select_last_genome(genomes)
    seq_regions = get_seq_regions(genome)
    for region in seq_regions:
        print(f"Retrieving genes from chromosome/contig {region.name}")
        genes = fetch_genes_from_region(species.ensembl_name, region.name)
        # Extract info for each gene in a dict and add them in a list.
        # Allow to insert genes in bulk after, increasing the performance
        genes_additional_info = []
        for gene in genes:
            embl, uniprotkb = extract_EMBL_UniProtKB_id(gene["description"])
            # Some gene doesn't have an assembly associated, or have
            # '_bd' added at the end. this code use their name to determine
            # at which assembly they belong
            if not gene["assembly_name"] or gene["assembly_name"].endswith("bd"):
                gene["assembly_name"] = get_assembly_name_from_gene_name(gene["id"])

            assembly_id = get_last_assembly_id_by_name(session, gene["assembly_name"])

            genes_additional_info.append(
                {
                    "assembly_id": assembly_id,
                    "embl": embl,
                    "uniprotkb": uniprotkb,
                    "region_id": region.id,
                    "species_id": species.id,
                }
            )

        insert_genes_in_db(session, genes, genes_additional_info)


def format_search_description(description: str):
    if not description:
        return None
    # Remove id in the description
    clean = re.search(r"[^\[\]]*", description)
    description = description[clean.start() : clean.end()].strip()

    description = convert_roman_number(description)
    return description


def convert_roman_number(string: str) -> str:
    """Find roman numbers in string and replace them by arabic numbers

    Args:
        string (str): Input string with possible roman number

    Returns:
        str: Input string with roman number transformed in arabic number
    """
    if not string:
        return string
    roman_number = re.findall(
        r"[_\s]+(?=[MDCLXVI]+\b)M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})",
        string,
    )
    to_replace = []
    if roman_number:
        for number in roman_number:
            # The Regex find 3 groups: hundred, tens, rest
            hundred = number[0]
            tens = number[1]
            rest = number[2]
            total = hundred + tens + rest
            # C, M and D are generaly not use as number
            if total in ["C", "M", "D", ""]:
                continue
            converted = str(roman_to_int(total))
            to_replace.append({"roman": total, "arabic": converted})
    for number_to_replace in to_replace:
        regex_for_replacement = r"[_\s]+(" + number_to_replace["roman"] + r");?"
        string = replace_using_regex_group(
            regex_for_replacement, number_to_replace["arabic"], string
        )
    return string


def replace_using_regex_group(pattern: str, repl: str, string: str) -> str:
    """Replace substing using regex group

    Reasons behind this function:
        re.sub replace all the regex by the replacement,
        while I just want to replace a group inside the regex.
        This will capture the different occurence of the regex
        and replace only the group inside

    Args:
        pattern (str): A regex pattern with a group to replace
        repl (str): The value to put in place of the group
        string (str): The string on which use re.sub

    Returns:
        str: The value string with substring replaced
    """
    matchs = [match for match in re.finditer(pattern, string)]
    for match in matchs:
        pattern_w_old_group = string[match.start() : match.end()]
        new_pattern = pattern_w_old_group.replace(match.group(1), repl)
        string = re.sub(pattern_w_old_group, new_pattern, string)
    return string


def roman_to_int(roman: str) -> int:
    rom_val = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}
    int_val = 0
    for i in range(len(roman)):
        if i > 0 and rom_val[roman[i]] > rom_val[roman[i - 1]]:
            int_val += rom_val[roman[i]] - 2 * rom_val[roman[i - 1]]
        else:
            int_val += rom_val[roman[i]]
    return int_val
