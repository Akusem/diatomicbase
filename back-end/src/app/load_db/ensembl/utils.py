from sqlalchemy.orm import Session
from app import ENSEMBL_REST
from app.load_db.utils import fetch_endpoint, post_endpoint


class EnsemblRetrieveInfo:
    def __init__(self, session: Session):
        self.session = session

    def fetch_ensembl(self, request: str, content_type="application/json"):
        return fetch_endpoint(ENSEMBL_REST, request, content_type)

    def fetch_feature_from_region(self, feature: str, region: str, species: str):
        genes = self.fetch_ensembl(
            f"overlap/region/{species}/{region}?feature={feature}"
        )
        return genes


def fetch_ensembl(request: str, content_type="application/json"):
    return fetch_endpoint(ENSEMBL_REST, request, content_type)


def fetch_feature_from_region(feature: str, region: str, species: str):
    genes = fetch_ensembl(f"overlap/region/{species}/{region}?feature={feature}")
    return genes


def post_ensembl(request: str, data, content_type="application/json"):
    return post_endpoint(ENSEMBL_REST, request, data, content_type)
