from typing import List, Tuple, Dict
from xmlrpc.client import Boolean
from fastapi import Query
import pandas as pd
from gprofiler import GProfiler
from app import GPROFILER_SPECIES_EQUIVALENT
from sqlalchemy import exists
from sqlalchemy.orm import Session
from app.db.models import Gene, Kegg
from app.load_db.utils import (
    remove_duplicate_dict_in_list,
    select_genomes_from_species,
    bulk_insert_object_in_db,
    select_last_genome,
    divide_chunks,
)


class RetrieveKegg:
    def __init__(
        self,
        session: Session,
        species_name="",
        gprofiler_species_names=GPROFILER_SPECIES_EQUIVALENT,
    ):
        self.session = session
        self.gp = GProfiler(return_dataframe=True)
        self.gprofiler_species_name = gprofiler_species_names
        if species_name:
            self.run(species_name)

    def run(self, species: str) -> None:
        genes_name = self.get_all_genes_name(species)
        kegg_available = self.kegg_available_for_organism(species, genes_name)
        if not kegg_available:
            print(f"KEGG not available for {species} in gProfiler, passing...")
            return
        kegg_info = self.retrieve_kegg_info(genes_name, species)
        kegg_relevant_info = self.extract_kegg_relevant_info(kegg_info)
        kegg_not_in_db = self.get_unique_kegg_not_in_db(kegg_relevant_info)
        self.create_and_insert_kegg_object_in_db(kegg_not_in_db)
        self.create_kegg_link_with_gene(kegg_relevant_info)

    def get_all_genes_name(self, species) -> List[Gene]:
        genomes = select_genomes_from_species(self.session, species)
        genome = select_last_genome(genomes)
        genes_name: List[Tuple[Gene]] = (
            self.session.query(Gene.name).filter(Gene.assembly_id == genome.id).all()
        )
        # Extract the gene_name of their tuple
        genes_name = [gene_tuple[0] for gene_tuple in genes_name]
        return genes_name

    def kegg_available_for_organism(
        self, species: str, genes_name: List[str]
    ) -> Boolean:
        """Verify that gProfiler have KEGG annotation for the given species

        Args:
            species (str): species name
            genes_name (List[str]): List of genes name

        Returns:
            Boolean: True if kegg available else False
        """
        organism = self.get_gprofiler_species_name(species)
        try:
            query = {genes_name[0]: [genes_name[0]]}
            self.gp.profile(organism=organism, query=query, sources=["KEGG"])
        except AssertionError as error:
            if "No legal source provided" in str(error):
                return False
        return True

    def retrieve_kegg_info(self, genes_name: List[Gene], species: str) -> pd.DataFrame:
        keggs_df = []
        organism = self.get_gprofiler_species_name(species)
        # Split the genes_name to be asked to gProfiler
        # to avoid rejection of the the API call
        for i, genes_subset in enumerate(divide_chunks(genes_name, 500)):
            print(f"Retrieving kegg info {500*(i+1)}/{len(genes_name)}")
            # To have the KEGG of each individual gene we need to put
            # each one in a group. It's done by giving a Dict as query,
            # instead of a List
            # In the Dict, the keys correspond to groups name
            # and the values are a List with the gene name. ex:
            # query = {'gene_1': ['gene_1'], 'gene_2': ['gene_2']}
            # With this format each gene will return the relevant KEGG
            # information if they exist.
            query = {gene: [gene] for gene in genes_subset}
            kegg_info = self.gp.profile(
                organism=organism, query=query, sources=["KEGG"]
            )
            keggs_df.append(kegg_info)
        return pd.concat(keggs_df)

    def extract_kegg_relevant_info(
        self, kegg_info: pd.DataFrame
    ) -> List[Dict[str, str]]:
        keggs_relevant_info = []
        for i, row in kegg_info.iterrows():
            keggs_relevant_info.append(
                {
                    "accession": row["native"].replace("KEGG:", "ko"),
                    "description": row["name"],
                    "gene_name": row["query"],
                }
            )
        return keggs_relevant_info

    def get_unique_kegg_not_in_db(
        self, kegg_relevant_info: List[Dict[str, str]]
    ) -> List[Dict[str, str]]:
        kegg_not_in_db = []
        unique_kegg = list(
            {kegg["accession"]: kegg for kegg in kegg_relevant_info}.values()
        )
        for kegg in unique_kegg:
            in_db = self.session.query(
                exists().where(Kegg.accession == kegg["accession"])
            ).scalar()
            if not in_db:
                kegg_not_in_db.append(kegg)
        return remove_duplicate_dict_in_list(kegg_not_in_db)

    def create_and_insert_kegg_object_in_db(
        self, kegg_relevant_info: List[Dict[str, str]]
    ) -> None:
        kegg_objects = [
            Kegg(accession=kegg["accession"], description=kegg["description"])
            for kegg in kegg_relevant_info
        ]
        print(f"Inserting {len(kegg_objects)} KEGG in db")
        bulk_insert_object_in_db(self.session, kegg_objects)

    def get_gprofiler_species_name(self, species: str) -> str:
        try:
            gp_name = self.gprofiler_species_name[species]
        except KeyError:
            raise KeyError(f"gprofiler_species_name is not define for {species}")
        return gp_name

    def get_gene_from_name(self, gene_name: str) -> Gene:
        return self.session.query(Gene).filter(Gene.name == gene_name).first()

    def get_kegg_from_accession(self, kegg_accession: str) -> Kegg:
        return self.session.query(Kegg).filter(Kegg.accession == kegg_accession).first()

    def create_kegg_link_with_gene(
        self, kegg_relevant_info: List[Dict[str, str]]
    ) -> None:
        for kegg in kegg_relevant_info:
            kegg_object = self.get_kegg_from_accession(kegg["accession"])
            gene = self.get_gene_from_name(kegg["gene_name"])
            if kegg_object in gene.kegg:
                print(
                    f"{kegg_object.accession} already linked to {gene.name}, skipping"
                )
                continue
            else:
                print(f"Linking {kegg_object.accession} to {gene.name}")
            kegg_object.genes.append(gene)
            self.session.add(kegg_object)
        self.session.commit()
