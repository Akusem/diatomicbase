from typing import List
from app import EMAIL
from app.db import SessionLocal
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from app.load_db.ensembl.dna import EnsemblRetrieveGenomeSequence
from app.load_db.ensembl.gene import retrieve_genes_and_insert_in_db
from app.load_db.ensembl.transcript import retrieve_transcript_and_insert_in_db
from app.load_db.ensembl.exon import retrieve_exons_and_insert_in_db
from app.load_db.ensembl.cds import retrieve_cds_and_insert_in_db
from app.load_db.ensembl.variant import retrieve_variants_and_insert_in_db
from app.load_db.ensembl.xrefs import RetrieveXrefs
from app.load_db.uniprot.annotation import retrieve_annotation_and_insert_in_db
from app.load_db.uniprot.domain import retrieve_gene_domains_and_insert_in_db
from app.load_db.gprofiler.kegg import RetrieveKegg
from app.load_db.ncbi.rna_seq_metadata import NcbiRetrieveRnaSeqMetaData
from app.load_db.eggNog.kog_detail import retrieve_kog_details


def loader(steps: List[str], species: str):
    session = SessionLocal()
    if "Assembly (Ensembl)" in steps:
        # Retrieve Assembly info
        EnsemblRetrieveGenomeInfo(session).run(species)
    if "Genome Sequence (Ensembl)" in steps:
        # Retrieve DNA sequence.
        # Depends on the presence of a genome, its top_level_region and the
        # seq_region associated in the db to retrieve sequence
        # from ensembl REST API and link them to the sequences
        EnsemblRetrieveGenomeSequence(session).run(species)
    if "Genes (Ensembl)" in steps:
        # Retrieve gene in ensembl associated with the species
        # Depends on the presence of a genome, the top_level_region
        # and seq_region
        retrieve_genes_and_insert_in_db(session, species)
    if "Transcripts (Ensembl)" in steps:
        # Retrieve Trancscript in ensembl associated with the species
        # Depends on the presence of a genome, the top_level_region
        # seq_region, and gene
        retrieve_transcript_and_insert_in_db(session, species)
    if "Exons (Ensembl)" in steps:
        # Retrieve Exon in ensembl associated with the species
        # Depends on the presence of a genome, the top_level_region
        # seq_region, gene, and transcript
        retrieve_exons_and_insert_in_db(session, species)
    if "CDS (Ensembl)" in steps:
        # Retrieve CDS in ensembl associated with the species
        # Depends on the presence of a genome, the top_level_region
        # seq_region, gene, and transcript
        retrieve_cds_and_insert_in_db(session, species)
    if "Xrefs (Ensembl & NCBI)" in steps:
        # Retrieve Xrefs
        # Depends on the presence of a Genome,
        # the top_level_region, seq_region, and gene
        RetrieveXrefs(session, EMAIL).run(species)
    if "GO & Domains & KOG (Uniprot)" in steps:
        # Retrieve GO term and domains annotation associated with gene
        # in uniprot if those have an uniprotkb id.
        # Depends on gene presence in db.
        retrieve_annotation_and_insert_in_db(session, species)
    if "KOG details (eggNOG)" in steps:
        # Retrieve description of KOG from their id
        # Depends on Kog presence in db
        retrieve_kog_details(session)
    if "Domain position (UniProt)" in steps:
        # Retrieve Domains position and linked annotation for
        # genes with uniprotkb id.
        # Depends on gene and domains annotation presence in db.
        retrieve_gene_domains_and_insert_in_db(session, species)
    if "KEGG (gProfiler)" in steps:
        # Retrieve Kegg associated with gene using gProfiler API,
        # and link them
        # Depends on gene presence in db.
        RetrieveKegg(session, species)
    if "Variants" in steps:
        # Retrieve variant in ensembl using the REST API,
        # Depends on the presence of a Genome
        retrieve_variants_and_insert_in_db(session, species)
    if "RNA-seq MetaData (NCBI)" in steps:
        # Retrieve Rna-seq metadata from SRA,
        # Doesn't depend on anything
        NcbiRetrieveRnaSeqMetaData(session, species)
