import os
import sys
import gffutils
from typing import List
from sqlalchemy.orm import Session
from Bio import SeqIO
from InquirerPy import inquirer
from app.db import SessionLocal
from app.db.models import (
    Dna,
    GenomeTopLevelRegion,
    SeqRegion,
    Species,
    Genome,
    Gene,
    Transcript,
    Exon,
    Cds,
)
from app.load_db.ensembl.gene import format_search_description, get_gene_by_name
from app.load_db.ensembl.transcript import get_transcript_by_name
from app.load_db.ensembl.exon import get_exon_by_name
from app.load_db.ensembl.cds import get_cds_by_name


def manual_loader(gff_path: str, species: Species, assembly: Genome, source="GFF"):
    session: Session = SessionLocal()

    internal_db = "gff_read.db"
    gffutils.create_db(
        gff_path.strip(),
        dbfn=internal_db,
        force=True,
        keep_order=True,
        merge_strategy="merge",
        sort_attribute_values=True,
    )

    db = gffutils.FeatureDB(internal_db, keep_order=True)
    gffutils.constants.always_return_list = False

    genes = db.all_features(featuretype=["gene"])
    for gene in genes:
        attr_key = gene.attributes.keys()
        name = gene["Name"]
        source = gene["Source"] if "Source" in attr_key else source
        description = gene["Description"] if "Description" in attr_key else ""
        biotype = gene["Biotype"] if "Biotype" in attr_key else ""
        seq_region = get_or_create_seq_region_from_name(session, assembly, gene.chrom)
        gene_object = create_gene(
            session,
            species,
            assembly,
            seq_region,
            name,
            gene.start,
            gene.end,
            gene.strand,
            source,
            description,
            biotype,
        )
        if "uniprotkb" in attr_key:
            gene_object.alias["uniprotkb"] = gene["uniprotkb"]
            session.commit()
        if "embl" in attr_key:
            gene_object.alias["embl"] = gene["embl"]
            session.commit()
        if "ncbi" in attr_key:
            gene_object.alias["ncbi_display_id"] = gene["ncbi"]
            session.commit()

        for i, transcript in enumerate(
            db.children(gene, featuretype=["mRNA", "transcript"])
        ):
            print("transcript: ", transcript.id)
            attr_key = transcript.attributes.keys()
            if "Name" in attr_key:
                name = transcript["Name"]
            else:
                name = gene["Name"] + f".t{i}"
            transcript_object = create_transcript(
                session,
                gene_object,
                assembly,
                seq_region,
                name,
                transcript.start,
                transcript.end,
                transcript.strand,
                source,
            )

            for i, exon in enumerate(db.children(transcript, featuretype=["exon"])):
                print("exon: ", exon.id)
                attr_key = exon.attributes.keys()
                if "Name" in attr_key:
                    name = exon["Name"]
                else:
                    name = gene["Name"] + f".e{i}"
                exon_object = create_exon(
                    session,
                    gene_object,
                    transcript_object,
                    assembly,
                    seq_region,
                    name,
                    exon.start,
                    exon.end,
                    exon.strand,
                    exon.frame,
                    source,
                )
            for i, CDS in enumerate(db.children(transcript, featuretype=["CDS"])):
                print(CDS.id)
                attr_key = CDS.attributes.keys()
                if "Name" in attr_key:
                    name = CDS["Name"]
                else:
                    name = gene["Name"] + f".p{i}"
                CDS_object = create_cds(
                    session,
                    gene_object,
                    transcript_object,
                    assembly,
                    seq_region,
                    name,
                    CDS.start,
                    CDS.end,
                    CDS.strand,
                    CDS.frame,
                    source,
                )

    os.remove(internal_db)


def get_or_create_seq_region_from_name(
    session: Session, assembly: Genome, chrom_name: str
) -> SeqRegion:
    regions: List[SeqRegion] = [
        region.seq_region for region in assembly.top_level_region
    ]
    seq_region = [region for region in regions if region.name == chrom_name]
    if seq_region:
        return seq_region[0]
    else:
        new_region = create_region(session, assembly, chrom_name)
        return new_region


def create_region(session: Session, assembly: Genome, chrom_name: str) -> SeqRegion:
    new_region = SeqRegion(name=chrom_name)
    # Add and get region id
    session.add(new_region)
    session.commit()
    session.refresh(new_region)
    # Determine if it's a chromosome or supercontig
    if chrom_name.startswith("chr_") or chrom_name[0].isdigit():
        coord_system = "chromosome"
    else:
        coord_system = "supercontig"
    print(
        f"Creating new '{coord_system.capitalize()}' named '{chrom_name}' for '{assembly.assembly_name}'"
    )

    # Add TopLevelRegion that point to SeqRegion
    assembly.top_level_region.append(
        GenomeTopLevelRegion(
            coord_system=coord_system,
            genome_id=assembly.id,
            seq_region_id=new_region.id,
        )
    )
    session.commit()
    # Ensure it's up to date
    session.refresh(assembly)
    return new_region


def create_gene(
    session: Session,
    species: Species,
    assembly: Genome,
    seq_region: SeqRegion,
    name: str,
    start: int,
    end: int,
    strand: str,
    source="GFF",
    description="",
    biotype="",
) -> int:
    gene_object = get_gene_by_name(session, name)
    if not gene_object:
        print(f"Inserting {name} in db")
        gene_object = Gene()
    else:
        print(f"Updating {gene_object.name}")

    gene_object.name = name
    gene_object.start = start
    gene_object.end = end
    gene_object.strand = 1 if strand == "+" else -1
    gene_object.biotype = biotype
    gene_object.source = source
    gene_object.description = description
    gene_object.search_description = format_search_description(description)
    gene_object.assembly_id = assembly.id
    gene_object.seq_region_id = seq_region.id
    gene_object.species_id = species.id

    session.add(gene_object)
    session.commit()
    session.refresh(gene_object)
    return gene_object


def create_transcript(
    session: Session,
    gene: Gene,
    assembly: Genome,
    seq_region: SeqRegion,
    name: str,
    start: int,
    end: int,
    strand: str,
    source="GFF",
    biotype="",
):
    transcript_object = get_transcript_by_name(session, name)
    if not transcript_object:
        print(f"Inserting {name} in db")
        transcript_object = Transcript()
    else:
        print(f"Updating {transcript_object.name}")

    transcript_object.name = name
    transcript_object.start = start
    transcript_object.end = end
    transcript_object.strand = 1 if strand == "+" else -1
    transcript_object.biotype = biotype
    transcript_object.source = source
    transcript_object.gene_id = gene.id
    transcript_object.assembly_id = assembly.id
    transcript_object.seq_region_id = seq_region.id

    session.add(transcript_object)
    session.commit()
    session.refresh(transcript_object)
    return transcript_object


def create_exon(
    session: Session,
    gene: Gene,
    transcript: Transcript,
    assembly: Genome,
    seq_region: SeqRegion,
    name: str,
    start: int,
    end: int,
    strand: str,
    phase: int,
    source="GFF",
):
    exon_object = get_exon_by_name(session, name)
    if not exon_object:
        print(f"Inserting {name} in db")
        exon_object = Exon()
    else:
        print(f"Updating {exon_object.name}")

    exon_object.name = name
    exon_object.start = start
    exon_object.end = end
    exon_object.strand = 1 if strand == "+" else -1
    exon_object.phase = -1 if phase == "." else phase
    exon_object.source = source
    exon_object.gene_id = gene.id
    exon_object.transcript_id = transcript.id
    exon_object.assembly_id = assembly.id
    exon_object.seq_region_id = seq_region.id

    session.add(exon_object)
    session.commit()
    session.refresh(exon_object)
    return exon_object


def create_cds(
    session: Session,
    gene: Gene,
    transcript: Transcript,
    assembly: Genome,
    seq_region: SeqRegion,
    name: str,
    start: int,
    end: int,
    strand: str,
    phase: int,
    source="GFF",
):
    cds_object = get_cds_by_name(session, name)
    if not cds_object:
        print(f"Inserting {name} in db")
        cds_object = Cds()
    else:
        print(f"Updating {cds_object.name}")
    print("name:", name)
    cds_object.name = name
    cds_object.start = start
    cds_object.end = end
    cds_object.strand = 1 if strand == "+" else -1
    cds_object.phase = -1 if phase == "." else phase
    cds_object.source = source
    cds_object.gene_id = gene.id
    cds_object.transcript_id = transcript.id
    cds_object.assembly_id = assembly.id
    cds_object.seq_region_id = seq_region.id

    session.add(cds_object)
    session.commit()
    session.refresh(cds_object)
    return cds_object


def assembly_sequence_loader(session: Session, assembly: Genome, fasta_path: str):
    records = SeqIO.parse(fasta_path, "fasta")
    print(
        "Do you want to insert theses region (chromosome or super_contig) and their sequence in db ?"
    )
    for record in records:
        seq_name = record.id.replace("|", " ").split()[0]
        print(seq_name)
    confirm = inquirer.confirm("Confirm ?", default=False).execute()
    if not confirm:
        sys.exit(0)

    records = SeqIO.parse(fasta_path, "fasta")
    for record in records:
        seq_name = record.id.replace("|", " ").split()[0]
        region: SeqRegion = get_or_create_seq_region_from_name(
            session, assembly, seq_name
        )
        dna = Dna(sequence=str(record.seq))
        session.add(dna)
        session.commit()
        session.refresh(dna)
        region.dna_id = dna.id
        region.top_level_regions.length = len(record.seq)
        session.commit()
        print(f"'{seq_name}' sequence added in db")
