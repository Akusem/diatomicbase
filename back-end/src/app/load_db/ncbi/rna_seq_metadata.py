import csv
from datetime import datetime
from typing import List, Dict

import xmltodict
from Bio import Entrez
from sqlalchemy.orm import Session

from app import EMAIL
from app.db.models import SraExperiment, SraStudy
from app.load_db.utils import bulk_insert_object_in_db


class NcbiRetrieveRnaSeqMetaData:
    def __init__(self, session: Session, species_name="", email: str = EMAIL):
        self.sra_experiments = []
        self.sra_studies = []
        self.session = session
        Entrez.email = email
        # If species is given, launch the main function
        if species_name:
            self.run(species_name)

    def run(self, species_name: str):
        """Retrieve Rna-seq metadata from SRA and insert them as
        SraStudy or SraExperiment row in db if not already present

        Args:
            species_name (str): Name of the species from which 'run' finds
                RNA-seq data in SRA
        """
        esearch_records = self.fetch_sra_id_from_species(species_name)
        sra_xml = self.fetch_sra_xml_from_sra_records(esearch_records)
        sra_runinfo = self.fetch_sra_runinfo_from_sra_records(esearch_records)
        sra_runinfo = self.filter_non_rna_seq_row(sra_runinfo)

        self.create_sra_studies(sra_runinfo)
        self.insert_all_sra_study_in_db()
        self.update_sra_studies_with_title_and_abstract(sra_xml)

        self.create_new_sra_experiment(sra_runinfo, species_name)
        self.bulk_insert_sra_experiment_in_db()

        print("RNA-seq metadata fetched and inserted in db")

    def fetch_sra_id_from_species(self, species_name: str):
        """Use esearch to retrieve all sra id linked to a species and return records readen

        Args:
            species_name (str): The species name

        Returns:
            Dict: esearch records readen
        """
        search_res = Entrez.esearch(db="sra", term=species_name, retmax=10000)
        records = Entrez.read(search_res)
        return records

    def fetch_sra_xml_from_sra_records(self, records):
        """Fetch SRA xml from a records dict from esearch and return
        the xml transform in a dict

        Args:
            records ([type]): A records dict return by esearch

        Returns:
            OrderedDict: A dict representation of the xml
        """
        sra_xml = Entrez.efetch(db="sra", id=records["IdList"])
        return xmltodict.parse(sra_xml.read())

    def fetch_sra_runinfo_from_sra_records(self, records) -> csv.DictReader:
        """Use efetch to retrieve all experiment in SRA from the species given
        in csv format and return them using csv.DictReader, fetch from ncbi the runinfo
        data in csv format

        Args:
            records: Name of the species from which the function finds
                RNA-seq data in SRA

        Returns:
            csv.DictReader: The SRA experiments runinfo read by csv.DictReader for
                easier parsing
        """
        sra = Entrez.efetch(
            db="sra", id=records["IdList"], retmode="csv", rettype="runinfo"
        )
        return csv.DictReader(sra)

    def filter_non_rna_seq_row(self, sra: csv.DictReader) -> List[Dict[str, str]]:
        """Keep only Rna-seq row in SRA runinfo csv

        Args:
            sra (csv.DictReader): The SRA experiments runinfo read by csv.DictReader

        Returns:
            List[Dict[str, str]]:
        """
        sra_filtered = [row for row in sra if row["LibraryStrategy"] == "RNA-Seq"]
        return sra_filtered

    def create_sra_studies(self, sra: csv.DictReader) -> None:
        """Taking an SRA runinfo csv parsed, create each SraStudy objects
        if they doesn't exist

        Args:
            sra (csv.DictReader): The SRA experiments runinfo read by csv.DictReader
        """
        for row in sra:
            if self.not_in_sra_studies(row["SRAStudy"]):
                print(f"Adding {row['SRAStudy']} ")
                self.sra_studies.append(
                    self.create_sra_study_object(accession=row["SRAStudy"])
                )

    def not_in_sra_studies(self, study_accession: str) -> bool:
        """Test if the study accession is present in self.sra_studies and in db

        Args:
            study_accession (str): An SRA study accession

        Returns:
            bool: True if the id given is not in the studies, False overwise
        """
        already_in_db = (
            self.session.query(SraStudy)
            .filter(SraStudy.accession == study_accession)
            .first()
        )
        if already_in_db:
            return False
        for study in self.sra_studies:
            if study.accession == study_accession:
                return False
        return True

    @staticmethod
    def create_sra_study_object(accession: str, title: str = "") -> SraStudy:
        """Create an SraStudy object

        Args:
            accession (str): Accession of the study.
            title (str, optional): Title of the study.

        Returns:
            SraStudy: An instance of SraStudy
        """
        if title:
            sra_study = SraStudy(accession=accession, title=title)
        else:
            sra_study = SraStudy(accession=accession)
        return sra_study

    def update_sra_studies_with_title_and_abstract(self, sra_xml) -> None:
        """Add title and abstract to sra studies. the SRA studies need to be
        already inserted in db

        Args:
            sra_xml (OrderedDict): A Dict representation of an SRA XML return by efetch
        """
        sra_studies_already_update = []
        if len(sra_xml["EXPERIMENT_PACKAGE_SET"]["EXPERIMENT_PACKAGE"]) > 1:
            for sra in sra_xml["EXPERIMENT_PACKAGE_SET"]["EXPERIMENT_PACKAGE"]:
                study_accession = sra["STUDY"]["IDENTIFIERS"]["PRIMARY_ID"]
                library_strategy = sra["EXPERIMENT"]["DESIGN"]["LIBRARY_DESCRIPTOR"][
                    "LIBRARY_STRATEGY"
                ]
                if library_strategy != "RNA-Seq":
                    continue
                if study_accession in sra_studies_already_update:
                    continue
                sra_study_object = self.get_sra_study_by_accession(study_accession)
                if not sra_study_object:
                    raise IndexError("SRA study to update not found in db")
                sra_study_object.title = sra["STUDY"]["DESCRIPTOR"]["STUDY_TITLE"]
                if "STUDY_ABSTRACT" in sra["STUDY"]["DESCRIPTOR"].keys():
                    sra_study_object.abstract = sra["STUDY"]["DESCRIPTOR"][
                        "STUDY_ABSTRACT"
                    ]
                self.session.commit()
                sra_studies_already_update.append(study_accession)
        else:
            sra = sra_xml["EXPERIMENT_PACKAGE_SET"]["EXPERIMENT_PACKAGE"]["EXPERIMENT"]
            study_accession = sra["STUDY"]["IDENTIFIERS"]["PRIMARY_ID"]
            library_strategy = sra["EXPERIMENT"]["DESIGN"]["LIBRARY_DESCRIPTOR"][
                "LIBRARY_STRATEGY"
            ]
            if library_strategy == "RNA-Seq":
                sra_study_object = self.get_sra_study_by_accession(study_accession)
                if not sra_study_object:
                    raise IndexError("SRA study to update not found in db")
                sra_study_object.title = sra["STUDY"]["DESCRIPTOR"]["STUDY_TITLE"]
                if "STUDY_ABSTRACT" in sra["STUDY"]["DESCRIPTOR"].keys():
                    sra_study_object.abstract = sra["STUDY"]["DESCRIPTOR"][
                        "STUDY_ABSTRACT"
                    ]
                self.session.commit()

    def get_sra_study_by_accession(self, study_accession: str) -> SraStudy:
        return (
            self.session.query(SraStudy)
            .filter(SraStudy.accession == study_accession)
            .first()
        )

    def create_new_sra_experiment(self, sra: csv.DictReader, species_name: str) -> None:
        """Create all SraExperiment objects from an SRA runinfo csv if not present in db

        Args:
            sra (csv.DictReader): A row of a sra experiments csv read by csv.DictReader
        """
        self.sra_studies = self.session.query(SraStudy).all()
        for row in sra:
            if self.sra_experiment_in_db(row["Run"]):
                print(f"Updating {row['Run']} in db")
                self.update_sra_experiment_in_db(row["Run"], row)
                continue
            linked_study_id = self.find_associated_study_id(row["SRAStudy"])
            print(f"Adding {row['Run']} in db")
            self.sra_experiments.append(
                self.create_sra_experiment_object(row, linked_study_id, species_name)
            )

    def sra_experiment_in_db(self, sra_accession: str) -> bool:
        """Return True if SRA is in DB else False
        Check against SraExperiment.run field

        Args:
            sra_accession (str): Accession number of SRA to search in db

        Returns:
            bool: True if found else False
        """
        accessions = self.session.query(SraExperiment.run).all()
        # Extract the run from the tuple they are in
        accessions = [accession[0] for accession in accessions]
        return True if sra_accession in accessions else False

    def update_sra_experiment_in_db(self, run: str, row: dict) -> None:
        """Update SraExperiment field using new data

        Args:
            run (str): Run name to get the correct SraExperiment to update
            row (dict): dict with all the SraExperiment info
        """
        exp = self.session.query(SraExperiment).filter(SraExperiment.run == run).first()
        exp.release_date = datetime.strptime(row["ReleaseDate"], "%Y-%m-%d %H:%M:%S")
        exp.load_date = datetime.strptime(row["LoadDate"], "%Y-%m-%d %H:%M:%S")
        exp.average_length = row["avgLength"]
        exp.dl_path = row["download_path"]
        exp.library_name = row["LibraryName"]
        exp.library_selection = row["LibrarySelection"]
        exp.library_layout = row["LibraryLayout"]
        exp.platform = row["Platform"]
        exp.model = row["Model"]
        exp.project = row["ProjectID"]
        exp.sample_name = row["SampleName"]
        exp.center_name = row["CenterName"]
        exp.species_name = row["ScientificName"]

        self.session.commit()

    def find_associated_study_id(self, study_accession: str) -> int:
        """Find the study with same accession and return its id.
        The study have to be inserted in the db

        Args:
            study_accession (str): Accession number of the study to find

        Returns:
            int: Id in db of the study find
        """
        for study in self.sra_studies:
            if study.accession == study_accession:
                return study.id
        raise NameError(f"No study correponding to this accession {study_accession}")

    @staticmethod
    def create_sra_experiment_object(
        row: dict, study_id: int, species_name: str
    ) -> SraExperiment:
        """Create a SraExperiment object from a row of the csv of the SRA experiment

        Args:
            row (dict): A row of a sra experiments csv read by csv.DictReader

        Returns:
            SraExperiment: An instance of SraExperiment to insert in db
        """
        sra_exp = SraExperiment(
            run=row["Run"],
            release_date=datetime.strptime(row["ReleaseDate"], "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime(row["LoadDate"], "%Y-%m-%d %H:%M:%S"),
            average_length=row["avgLength"],
            dl_path=row["download_path"],
            library_name=row["LibraryName"],
            library_selection=row["LibrarySelection"],
            library_layout=row["LibraryLayout"],
            platform=row["Platform"],
            model=row["Model"],
            project=row["ProjectID"],
            sample_name=row["SampleName"],
            center_name=row["CenterName"],
            species_name=species_name,
            study_id=study_id,
        )
        return sra_exp

    def insert_all_sra_study_in_db(self) -> None:
        print(f"Inserting {len(self.sra_studies)} SRA studies in db")
        self.session.add_all(self.sra_studies)
        self.session.commit()
        print("Done")

    def bulk_insert_sra_experiment_in_db(self) -> None:
        print(f"Inserting {len(self.sra_experiments)} SRA experiments in db")
        bulk_insert_object_in_db(self.session, self.sra_experiments)
        print("Done")
