from typing import List
from sqlalchemy.orm.session import Session

from app import UNIPROT_API
from app.db.models import Gene, DomainAnnotation, Species
from app.load_db.utils import fetch_endpoint


def fetch_uniprot(request, content_type="application/json"):
    return fetch_endpoint(UNIPROT_API, request, content_type)


def get_genes_with_uniprot_id_by_species(session: Session, species_name) -> List[Gene]:
    species = session.query(Species).filter(Species.name == species_name).first()
    genes_filter = [gene for gene in species.genes if gene.alias["uniprotkb"]]
    return genes_filter


def get_domain_annotation_by_accession(
    session: Session, domain_annotation_accession: str
) -> DomainAnnotation:
    query = (
        session.query(DomainAnnotation)
        .filter(DomainAnnotation.accession == domain_annotation_accession)
        .first()
    )
    return query
