import sys
import requests
from typing import List, Iterator, Any, Dict
from sqlalchemy.orm import Session
from app.db.models import SeqRegion, Genome, Gene, Transcript, Species
from sqlalchemy.ext.declarative.api import DeclarativeMeta


def fetch_endpoint(server, request, content_type):
    """
    Fetch an endpoint from the server, allow overriding of default content-type
    """
    retry = 10
    while True:
        try:
            r = requests.get(server + request, headers={"Accept": content_type})
            if content_type == "application/json":
                return r.json()
            else:
                return r.text
        except Exception as error:
            if retry > 0:
                print(f"Retry due to {error}")
                retry -= 1
                continue
            elif retry == 0:
                raise requests.HTTPError(error)


def post_endpoint(server, request, data, content_type):
    """
    Fetch an endpoint from the server, allow overriding of default content-type
    """
    retry = 10
    while True:
        try:
            r = requests.post(
                server + request, json=data, headers={"Accept": content_type}
            )
            if content_type == "application/json":
                return r.json()
            else:
                return r.text
        except Exception as error:

            if retry > 1:
                print(f"Retry due to {error}")
                retry -= 1
                continue
            elif retry == 0:
                r.raise_for_status()
                sys.exit()


def select_last_genome(genomes: List[Genome]) -> Genome:
    if len(genomes) == 0:
        raise ValueError("No Genomes given to select_last_genome")
    last = 0
    latest_index = 0
    for i, genome in enumerate(genomes):
        if genome.id > last:
            last = genome.id
            latest_index = i
    return genomes[latest_index]


def select_genomes_from_species(session: Session, species_name: str) -> List[Genome]:
    species = session.query(Species).filter(Species.name == species_name).first()
    return species.assemblies


def get_species_by_name(session: Session, species_name: str) -> Species:
    return session.query(Species).filter(Species.name == species_name).first()


def get_seq_regions(genome: Genome) -> List[SeqRegion]:
    top_level_region = genome.top_level_region
    seq_regions = [region.seq_region for region in top_level_region]
    return seq_regions


def get_last_assembly_id_by_name(session: Session, assembly_name: str) -> int:
    genomes = session.query(Genome).filter(Genome.assembly_name == assembly_name).all()

    if genomes:
        genome = select_last_genome(genomes)
        return genome.id
    else:
        return 0


def get_assembly_name_from_gene_name(gene_name: str) -> str:
    """Try to determine assembly name from a gene name"""
    # Phatr3 correspond to ASM15095v2 assembly
    if "Phatr3" in gene_name:
        return "ASM15095v2"
    else:
        return ""


def get_gene_id_from_name(session: Session, gene_name: str):
    """
    Return the gene_id from the gene's name. If no gene with this name
    in the db, return 0
    """
    gene = session.query(Gene).filter(Gene.name == gene_name).first()
    if gene:
        return gene.id
    else:
        return 0


def get_gene_name_from_transcript_name(session: Session, transcript_name: str) -> str:
    transcript = (
        session.query(Transcript).filter(Transcript.name == transcript_name).first()
    )
    if transcript:
        gene = transcript.gene
        return gene.name
    else:
        return ""


def get_transcript_id_from_name(session: Session, transcript_name: str) -> int:
    transcript = (
        session.query(Transcript).filter(Transcript.name == transcript_name).first()
    )
    if transcript:
        return transcript.id
    else:
        return 0


def bulk_insert_object_in_db(
    session: Session, sqlalchemy_objects: List[DeclarativeMeta]
) -> None:
    """Bulk insert given Sqlalchemy object in db

    Args:
        session (Session): SqlAlchemy Session object to access the db
        sqlalchemy_objects (List[DeclarativeMeta]): List of object inheriting
            from sqlalchemy Base
    """
    session.bulk_save_objects(sqlalchemy_objects)
    session.commit()


def divide_chunks(list_to_divide: List, chunk_size: int) -> Iterator[List[Any]]:
    for i in range(0, len(list_to_divide), chunk_size):
        yield list_to_divide[i : i + chunk_size]


def remove_duplicate_dict_in_list(list_of_dict: List[Dict]) -> List[Dict]:
    return [d for i, d in enumerate(list_of_dict) if d not in list_of_dict[i + 1 :]]
