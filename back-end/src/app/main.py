import os
import time
import random
import string
from fastapi import FastAPI, Request
from fastapi.responses import RedirectResponse
from fastapi_sqlalchemy import DBSessionMiddleware
from fastapi.middleware.cors import CORSMiddleware
from app import (
    RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
    RNA_SEQ_BIOPROJECT_DESCRIPTION,
    REPETITIVE_ELEMENTS_TABLE,
    RNA_SEQ_COMPARISON_TABLE,
    RNA_SEQ_RUNS_GROUP_TABLE,
    GENE_TO_MODULEPHAEONET,
    MODULEPHAEONET_TO_KEGG,
    API_DATA_FOLDER_PATH,
    PHAEONET_DATA_TSV,
    SERVER_DATA_URL,
    ECOTYPES_TABLE,
    LOG_FILE_PATH,
)
from app.db import DATABASE_URL
from app.api import gene, jbrowse, rna_seq, search, resources, species
from app.logger import logger, create_logger


app = FastAPI()
# Add Db middleware for simple db access in function
app.add_middleware(DBSessionMiddleware, db_url=DATABASE_URL)
# Configure CORS
origins = [
    "*",
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


log = create_logger("DiatOmicBase.api:", filename=LOG_FILE_PATH)


@app.middleware("http")
async def log_requests(request: Request, call_next):

    idem = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
    log.info(f"rid={idem} start request path={request.url.path}")
    start_time = time.time()

    response = await call_next(request)

    process_time = (time.time() - start_time) * 1000
    formatted_process_time = "{0:.2f}".format(process_time)
    log.info(
        f"rid={idem} completed_in={formatted_process_time}ms status_code={response.status_code}"
    )

    return response


app.include_router(gene.router, prefix="/gene")
app.include_router(search.router, prefix="/search")
app.include_router(species.router, prefix="/species")
app.include_router(rna_seq.router, prefix="/rna_seq")
app.include_router(jbrowse.router, prefix="/jbrowse")
app.include_router(resources.router, prefix="/resources")


@app.get("/server_data/{path}")
async def server_data_redirect(path: str):
    return RedirectResponse(f"{SERVER_DATA_URL}/{path}")


@app.on_event("startup")
async def verify_files():
    files_to_check = [
        ("RNA_SEQ_BIOPROJECT_DESCRIPTION", RNA_SEQ_BIOPROJECT_DESCRIPTION),
        ("REPETITIVE_ELEMENTS_TABLE", REPETITIVE_ELEMENTS_TABLE),
        ("RNA_SEQ_COMPARISON_TABLE", RNA_SEQ_COMPARISON_TABLE),
        ("RNA_SEQ_RUNS_GROUP_TABLE", RNA_SEQ_RUNS_GROUP_TABLE),
        ("GENE_TO_MODULEPHAEONET", GENE_TO_MODULEPHAEONET),
        ("MODULEPHAEONET_TO_KEGG", MODULEPHAEONET_TO_KEGG),
        ("PHAEONET_DATA_TSV", PHAEONET_DATA_TSV),
        ("ECOTYPES_TABLE", ECOTYPES_TABLE),
    ]
    for var_name, file in files_to_check:
        if not os.path.exists(file):
            log.warning(f"{var_name} file ('{file}') doesn't exist")
        elif is_lfs_pointer(file):
            log.warning(
                f"{var_name} ('{file}') is a git lfs pointer for now, did you setup git lfs ?"
            )

    dir_to_check = [
        ("RNA_SEQ_DIFF_EXPRESSION_OUTPUT", RNA_SEQ_DIFF_EXPRESSION_OUTPUT),
    ]
    for var_name, dir in dir_to_check:
        if not os.path.isdir(dir):
            log.warning(f"{var_name} dir ('{dir}') doesn't exist")


def is_lfs_pointer(file: str) -> bool:
    file = open(file, "r")
    return (
        True
        if file.readline().startswith("version https://git-lfs.github.com/")
        else False
    )
