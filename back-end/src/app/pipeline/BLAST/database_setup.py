import os
import shutil
import subprocess
from enum import Enum
from typing import List
from sqlalchemy.orm import Session
from app import BLAST_WORK_DIR
from app.db.models import Gene


class BLASTDatabaseSetup:
    def __init__(self, session: Session, WORK_DIR: str = BLAST_WORK_DIR):
        self.session = session
        self.WORK_DIR = WORK_DIR

        if not os.path.exists(self.WORK_DIR):
            os.makedirs(self.WORK_DIR)
            os.makedirs(os.path.join(self.WORK_DIR, "nucleotide_db"))
            os.makedirs(os.path.join(self.WORK_DIR, "protein_db"))
            os.makedirs(os.path.join(self.WORK_DIR, "blast_output"))
            os.makedirs(os.path.join(self.WORK_DIR, "blast_input"))

        for cmd in ["makeblastdb"]:
            if not self.cmd_exists(cmd):
                raise FileNotFoundError(f"{cmd} is not found in the PATH")

    def cmd_exists(self, cmd):
        return shutil.which(cmd) is not None

    def get_all_gene(self) -> List[Gene]:
        return self.session.query(Gene).all()

    def create_header_gene_fasta(self, gene: Gene) -> str:
        species_name = gene.species.ensembl_name
        return f">{gene.name} chrom={gene.seq_region.name} species={species_name}"

    def create_nucleotide_blast_db(self) -> None:
        all_gene_fasta_path = os.path.join(self.WORK_DIR, "nucleotide_db", "all_gene")
        self.create_fasta(all_gene_fasta_path, "nucl")
        self.create_blast_db(all_gene_fasta_path, "nucl")

    def create_protein_blast_db(self) -> None:
        all_prot_fasta_path = os.path.join(self.WORK_DIR, "protein_db", "all_protein")
        self.create_fasta(all_prot_fasta_path, "prot")
        self.create_blast_db(all_prot_fasta_path, "prot")

    def create_protein_diamond_db(self) -> None:
        all_prot_fasta_path = os.path.join(self.WORK_DIR, "protein_db", "all_protein")
        self.create_fasta(all_prot_fasta_path, "prot")
        self.create_diamond_db(all_prot_fasta_path)

    def create_fasta(
        self, fasta_path: str, type_seq: Enum("type_seq", ["nucl", "prot"])
    ) -> None:
        fasta = ""
        genes = self.get_all_gene()
        for gene in genes:
            header = self.create_header_gene_fasta(gene)
            if type_seq == "nucl":
                seq = gene.get_sequence(self.session)
            elif type_seq == "prot":
                if len(gene.cds) > 0:
                    seq = gene.get_protein_sequence(self.session)
                else:
                    continue
            fasta += f"{header}\n{seq}\n"

        fasta_file = open(fasta_path, "w")
        fasta_file.write(fasta)
        fasta_file.close()

    def create_blast_db(
        self, input_fasta_path: str, type_db: Enum("nucl", "prot")
    ) -> None:
        res = subprocess.run(
            f"makeblastdb -dbtype {type_db} -in {input_fasta_path} \
                -input_type fasta -parse_seqids -hash_index",
            shell=True,
            capture_output=True,
            text=True,
        )
        if res.stdout:
            print(res.stdout)
        if res.stderr:
            print(res.stderr)

    def create_diamond_db(self, input_fasta_path: str) -> None:
        diamond_db_path = os.path.join(self.WORK_DIR, "protein_db", "diamond_db")
        res = subprocess.run(
            f"diamond makedb --in {input_fasta_path} -d {diamond_db_path}",
            shell=True,
            capture_output=True,
            text=True,
        )
        if res.stdout:
            print(res.stdout)
        if res.stderr:
            print(res.stderr)

    def create_db(self):
        self.create_nucleotide_blast_db()
        self.create_protein_diamond_db()
