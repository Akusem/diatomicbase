#!/usr/bin/env Rscript

library(DESeq2)
library(readr)


merge.all <- function(x, ..., by = "row.names") {
  L <- list(...)
  for (i in seq_along(L)) {
    x <- merge(x, L[[i]], by = by, all = T)
    rownames(x) <- x$Row.names
    x$Row.names <- NULL
  }
  return(x)
}


#### Passing argument to R ###

args = commandArgs(trailingOnly=TRUE)
if (length(args)==0) {
  stop("You must supply 5 argument: 
      [1] = Result directory with :
      [2] = comparisons file (comparisons.tsv)
      [3] = sample groups (runs_group.tsv)
      [4] = species name
      [5] = output file is not mandatory [default = out.tsv]
      [6] = optional bioproject blacklist (id separated by semi-colon)"
       , call.=FALSE)
} else if (length(args)==4) {
  # default output file
  args[5] = "out.tsv"
}



### Preparing input files ###
dir <- args[1]
setwd(dir)
dir.create(file.path("detailed_results"), showWarnings=FALSE)

species_name <- args[4]
comparisons <- read.table(args[2], h=T, sep="\t")
run_groups <- read.table(args[3], h=T, sep="\t")

if (length(args) == 6) {
  blacklist <- strsplit(args[6], ";")
  blacklist <- unlist(blacklist, use.names=FALSE)
  # remove blacklisted from table
  run_groups <- subset(run_groups, !is.element(project_id, blacklist))
  comparisons <- subset(comparisons, !is.element(project_id, blacklist))
}

# keep element from the correct species
run_groups <- subset(run_groups, is.element(species, species_name))
comparisons <- subset(comparisons, is.element(species, species_name))
first <- read.table(paste(run_groups$run[1],"/results/featureCounts/merged_gene_counts.txt", sep=""), h=T, sep="\t")

####### Loop on all the comparisons ######

FC_table <- matrix("NA", nrow(first), nrow(comparisons))
colnames(FC_table) <- paste(comparisons$comp_id, "log2FC", sep="_")
padj_table <- matrix("NA", nrow(first), nrow(comparisons))
colnames(padj_table) <- paste(comparisons$comp_id, "padj", sep="_")

for (i in 1:nrow(comparisons)){
  project <- subset(run_groups, project_id == as.character(comparisons[i,"project_id"]))
  compa <- subset(project, (group_name == as.character(comparisons[i,"subset_1"]) | group_name == as.character(comparisons[i,"subset_2"])), select=c(run, group_name))
  # Ensure the type of compa[,1] is correct
  compa[,1] <- as.character(compa[,1])
  files <- file.path(dir, compa[,1], "results/featureCounts/merged_gene_counts.txt")
  list_of_files <- lapply(files, read_tsv)
  ab <- merge.all(list_of_files, all=TRUE)
  # Remove _1 after some SRR in ab colnames
  colnames(ab) <- gsub("_.$","",colnames(ab))
  rownames(ab) <- ab[,"Geneid"]
  ab <- ab[,c(compa[,1])]
  metadata <- as.data.frame(compa[,2])
  rownames(metadata) <- compa[,1]
  colnames(metadata) <- "condition"

  dds = DESeqDataSetFromMatrix(countData = ab, colData = metadata, design = ~ condition)
  dds <- DESeq(dds, betaPrior=FALSE)
  res <- results(dds,contrast=c("condition", as.character(comparisons[i,"subset_1"]), as.character(comparisons[i,"subset_2"])))
  counts_table = counts(dds, normalized=TRUE)
  detailed_table <- merge(counts_table, res, by="row.names", all.x=TRUE)
  file_name = paste("detailed_results/", comparisons[i,"comp_id"], sep="")
  write.table(detailed_table, file_name, quote=FALSE, row.names=FALSE, sep="\t")
  FC_table[,i] <- detailed_table$log2FoldChange
  padj_table[,i] <- detailed_table$padj
}
DEG_table <- cbind("GeneID"=detailed_table$Row.names, FC_table, padj_table)
write.table(DEG_table, file=args[5], quote=FALSE, row.names=FALSE, sep="\t")
