from _io import TextIOWrapper
from sqlalchemy.orm import Session
from app.db.models import Genome, Transcript
from app.load_db.utils import select_genomes_from_species, select_last_genome


def create_transcript_fasta_header(transcript: Transcript) -> str:
    """From a transcript, create a fasta header with the transcript name,
    gene name, chromosome name and strand. This type of header is used by
    prose.

    Args:
        transcript (Transcript): A Transcript instance

    Returns:
        str: A fasta header folowing this model:
            '>{name} gene={gene_name} seq={chrom_name} strand={strand}'
    """
    name = transcript.name
    gene_name = transcript.gene.name
    chrom_name = transcript.seq_region.name
    strand = "+" if transcript.strand == 1 else "-"
    return f">{name} gene={gene_name} seq={chrom_name} strand={strand}"


def write_seq(output: TextIOWrapper, header: str, seq: str) -> None:
    output.write(header + "\n" + seq + "\n")


def create_transcript_fasta(
    session: Session, species_name: str, output_path: str
) -> None:
    """Put all the transcript in a fasta file

    Args:
        session (Session): A SQLalchemy Session object to access the db
        species_name (str): The name of the species to retrieve its genome
        output_path (str): Name and path of the fasta
    """
    output = open(output_path, "w")
    genomes = select_genomes_from_species(session, species_name)
    genome = select_last_genome(genomes)
    transcripts = session.query(Transcript).filter(Genome.id == genome.id).all()
    for transcript in transcripts:
        header = create_transcript_fasta_header(transcript)
        seq = transcript.get_sequence(session)
        write_seq(output, header, seq)
