from sqlalchemy.orm import Session
from app.db.models import GenomeTopLevelRegion
from app.load_db.utils import select_genomes_from_species, select_last_genome


def create_fasta_header(
    assembly_name: str, top_level_region: GenomeTopLevelRegion
) -> str:
    length = top_level_region.length
    coord_system = top_level_region.coord_system
    name = top_level_region.seq_region.name

    return (
        f">{name} dna:{coord_system} "
        f"{coord_system}:{assembly_name}:{name}:1:{length}:1 REF"
    )


def create_genome_fasta(session: Session, species_name: str, output_path: str = ""):
    output = open(output_path, "w")
    genomes = select_genomes_from_species(session, species_name)
    genome = select_last_genome(genomes)
    for top_level_region in genome.top_level_region:
        # The supercontig use the assembly name with '_bd' at the end
        if top_level_region.seq_region.name.startswith("bd"):
            assembly_name = genome.assembly_name + "_bd"
        else:
            assembly_name = genome.assembly_name
        fasta_header = create_fasta_header(assembly_name, top_level_region)
        dna = top_level_region.seq_region.dna.sequence
        output.write(fasta_header + "\n")
        output.write(dna + "\n")
        output.flush()
    output.close()
