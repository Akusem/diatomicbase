import os
import glob
import shutil
import logging
import threading
import subprocess
from typing import List, Tuple, Dict, Optional
import pandas as pd
from sqlalchemy.orm import Session
from app import (
    PIPELINE_CPUS,
    PIPELINE_MAX_RAM,
    DIATOMICBASE_PATH,
    GTF_FILES,
    USE_FASTERQ,
    RNA_SEQ_ENV,
    FASTERQ_TMPDIR,
    RNA_SEQ_ALIGNER,
    RNA_SEQ_WORK_DIR,
    SINGULARITY_TMPDIR,
    API_DATA_FOLDER_PATH,
    RNA_SEQ_DOWNLOAD_DIR,
    RNA_SEQ_RUNS_GROUP_TABLE,
    RNA_SEQ_COMPARISON_TABLE,
    NXF_SINGULARITY_CACHEDIR,
    RNA_SEQ_BIOPROJECT_BLACKLIST,
    RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
)
from app.db.models import SraExperiment
from app.pipeline.RNA_seq.generate_genome_fasta import create_genome_fasta


class RNASeqManager:
    def __init__(
        self,
        session: Session,
        ALIGNER: str = RNA_SEQ_ALIGNER,
        WORK_DIR: str = RNA_SEQ_WORK_DIR,
        RNA_SEQ_ENV: str = RNA_SEQ_ENV,
        USE_FASTERQ: bool = USE_FASTERQ,
        FASTERQ_TMPDIR: str = FASTERQ_TMPDIR,
        DOWNLOAD_DIR: str = RNA_SEQ_DOWNLOAD_DIR,
        PIPELINE_CPUS: int = PIPELINE_CPUS,
        PIPELINE_MAX_RAM: float = PIPELINE_MAX_RAM,
        DIATOMICBASE_PATH: str = DIATOMICBASE_PATH,
        GTF_FILES: Dict[str, str] = GTF_FILES,
        SINGULARITY_TMPDIR: str = SINGULARITY_TMPDIR,
        API_DATA_FOLDER_PATH: str = API_DATA_FOLDER_PATH,
        COMPARISON_TABLE: str = RNA_SEQ_COMPARISON_TABLE,
        RUNS_GROUP_TABLE: str = RNA_SEQ_RUNS_GROUP_TABLE,
        BIOPROJECT_BLACKLIST: List[int] = RNA_SEQ_BIOPROJECT_BLACKLIST,
        NXF_SINGULARITY_CACHEDIR: str = NXF_SINGULARITY_CACHEDIR,
        DIFF_EXPRESSION_OUTPUT: str = RNA_SEQ_DIFF_EXPRESSION_OUTPUT,
    ):
        self.log = logging.getLogger("DiatOmicBase.RNASeqManager")
        self.session = session
        self.genomes_fasta = []
        self.WORK_DIR = WORK_DIR
        self.USE_FASTERQ = USE_FASTERQ
        self.FASTERQ_TMPDIR = FASTERQ_TMPDIR
        self.DOWNLOAD_DIR = DOWNLOAD_DIR
        self.ALIGNER = ALIGNER
        self.GTF_FILES = GTF_FILES
        self.RNA_SEQ_ENV = RNA_SEQ_ENV
        self.PIPELINE_CPUS = PIPELINE_CPUS
        self.PIPELINE_MAX_RAM = PIPELINE_MAX_RAM
        self.RUNS_GROUP_TABLE = RUNS_GROUP_TABLE
        self.COMPARISON_TABLE = COMPARISON_TABLE
        self.DIATOMICBASE_PATH = DIATOMICBASE_PATH
        self.SINGULARITY_TMPDIR = SINGULARITY_TMPDIR
        self.BIOPROJECT_BLACKLIST = BIOPROJECT_BLACKLIST
        self.API_DATA_FOLDER_PATH = API_DATA_FOLDER_PATH
        self.NXF_SINGULARITY_CACHEDIR = NXF_SINGULARITY_CACHEDIR
        self.DIFF_EXPRESSION_OUTPUT = DIFF_EXPRESSION_OUTPUT

        if not os.path.exists(self.WORK_DIR):
            os.makedirs(self.WORK_DIR)
        if not os.path.exists(self.DOWNLOAD_DIR):
            os.makedirs(self.DOWNLOAD_DIR)
        self.bigwig_dir = os.path.join(self.WORK_DIR, "BigWig")
        if not os.path.exists(self.bigwig_dir):
            os.makedirs(self.bigwig_dir)
        if not os.path.exists(os.path.join(self.bigwig_dir)):
            os.symlink(self.bigwig_dir, self.API_DATA_FOLDER_PATH)

    def run(self, species_name: str) -> None:
        # test_exp = [("SRR11570919", "PAIRED"), ("SRR10708548", "SINGLE")]
        first_run = True
        self.setup_env_var()
        experiments = self.get_all_sra_experiment_accession_and_layout(species_name)
        self.log_number_of_sra_to_be_processed(experiments)
        for i, exp in enumerate(experiments):
            # if exp not in test_exp:
            #     continue
            sra_accession = exp[0]
            paired = True if exp[1] == "PAIRED" else False
            if self.run_finished(sra_accession):
                self.log.info(f"{sra_accession} finished")
                continue
            if first_run:
                self.download_fastq_if_not_present(sra_accession, paired)
                first_run = False
            else:
                self.wait_fastq_download(sra_accession, paired)
            self.launch_download_next_experiment_fastq(experiments, i)
            self.log.info(f"Launching nfcore/rnaseq on {sra_accession}")
            self.launch_rna_seq_alignment(sra_accession, species_name)
            self.clean_run(sra_accession)
            if not self.bigwig_computed(sra_accession):
                self.log.info(f"Launch BigWig computation of {sra_accession}")
                self.compute_bigwig(sra_accession)
                self.log.info(f"End of BigWig computation of {sra_accession}")
            self.log.info(f"End of {sra_accession} analysis")
            self.delete_fastq(sra_accession)
        self.launch_diff_analysis(species_name)

    def setup_env_var(self):
        """Setup env variable define in init file if not already present.
        Env variable setup:
            NXF_SINGULARITY_CACHEDIR,
            SINGULARITY_TMPDIR
        """
        if (
            not "NXF_SINGULARITY_CACHEDIR" in os.environ
            and self.NXF_SINGULARITY_CACHEDIR != ""
        ):
            os.environ["NXF_SINGULARITY_CACHEDIR"] = self.NXF_SINGULARITY_CACHEDIR

        if not "SINGULARITY_TMPDIR" in os.environ and self.SINGULARITY_TMPDIR != "":
            os.environ["SINGULARITY_TMPDIR"] = self.SINGULARITY_TMPDIR

    def get_all_sra_experiment_accession_and_layout(
        self, species_name: str
    ) -> List[Tuple[str]]:
        """Get all SraExperiment accession (run) and library layout in a
        List of Tuple, exclude one's with bioproject define in
        RNA_SEQ_BIOPROJECT_BLACKLIST in init file.

        Args:
            species_name (str): Species to get SraExperiment info

        Returns:
            List[Tuple[str]]: List of all SraExperiment accession and layout
        """
        run_accessions_and_layout = (
            self.session.query(SraExperiment.run, SraExperiment.library_layout)
            .filter(
                SraExperiment.species_name.ilike(f"{species_name}%"),
                SraExperiment.project.notin_(self.BIOPROJECT_BLACKLIST),
            )
            .all()
        )
        run_accessions_and_layout = self.filter_runs_accession_not_in_manual_data(
            run_accessions_and_layout
        )
        return run_accessions_and_layout

    def filter_runs_accession_not_in_manual_data(
        self, sra_experiment_and_layout: List[Tuple[str]]
    ) -> List[Tuple[str]]:
        df = pd.read_csv(self.RUNS_GROUP_TABLE, sep="\t")
        runs = df["run"].to_list()
        filtered_exps = [exp for exp in sra_experiment_and_layout if exp[0] in runs]
        return filtered_exps

    def log_number_of_sra_to_be_processed(self, experiments: List[Tuple[str]]) -> None:
        sra_to_do = [exp[0] for exp in experiments if not self.run_finished(exp[0])]
        sra_to_do_str = "\n".join(sra_to_do)
        self.log.info(f"{len(sra_to_do)} SRA will be processed by the RnaSeq Pipeline")
        self.log.info(f"They are: {sra_to_do_str}")

    def run_finished(self, sra_accession: str) -> bool:
        """Tell if the RNA_seq analysis if finished by looking at the output file.

        Args:
            sra_accession (str): Which SRA to test if the computation have ended

        Returns:
            bool: Is the computation finished
        """
        work_dir = self.get_or_create_run_work_dir(sra_accession)
        result_dir = os.path.join(work_dir, "results")
        if not os.path.exists(result_dir):
            return False
        list_result_dir = os.listdir(result_dir)
        expected_completed_run_result_dir = [
            "dupradar",
            "fastqc",
            "featureCounts",
            "markDuplicates",
            "MultiQC",
            "pipeline_info",
            "preseq",
            "qualimap",
            "salmon",
            "rseqc",
            "stringtieFPKM",
            "trim_galore",
        ]
        if self.ALIGNER == "hisat2":
            expected_completed_run_result_dir.append("HISAT2")
        elif self.ALIGNER == "STAR":
            expected_completed_run_result_dir.append("STAR")
        if sorted(list_result_dir) == sorted(expected_completed_run_result_dir):
            return True
        else:
            return False

    def get_or_create_run_work_dir(self, sra_accession: str) -> str:
        """Return path to work dir specific to the sra_accession given.
        If the run folder doesn't exists, create it.

        Args:
            sra_accession (str): Run for which the dir is created

        Returns:
            str: Path to the Run dir
        """
        run_work_dir = os.path.join(self.WORK_DIR, sra_accession)
        if not os.path.exists(run_work_dir):
            try:
                os.mkdir(run_work_dir)
            except FileExistsError:
                pass
        return run_work_dir

    def download_fastq_if_not_present(self, sra_accession: str, paired: bool) -> None:
        """Download the Fastq files of the accession number given in self.DOWNLOAD_DIR
        if not already present

        Args:
            sra_accession (str): SRA Accession to download
            paired (bool): True if the RNA-seq paired else False
        """
        if not self.fastq_present(sra_accession, paired):
            self.log.info(f"launching download of {sra_accession}")
            self.download_fastq(sra_accession, paired, self.DOWNLOAD_DIR)
            self.log.info(f"End of download of {sra_accession}")
        else:
            self.log.info(f"{sra_accession} already downloaded")
        if not self.fastq_compressed(sra_accession):
            self.log.info(f"launch compression of {sra_accession}")
            self.compress_fastq(sra_accession, paired)
            self.log.info(f"End compression of {sra_accession}")
        else:
            self.log.info(f"{sra_accession} already compressed")

    def fastq_present(self, sra_accession: str, paired: bool) -> bool:
        """Test if the fastq are present in the download_dir.

        Args:
            sra_accession (str): SRA accession to investigate
            paired (bool): The library layout is paired or not

        Returns:
            bool: True if fastq files are present, else False
        """
        file_found = 0
        list_fastq = os.listdir(self.DOWNLOAD_DIR)
        for fastq in list_fastq:
            if sra_accession in fastq:
                file_found += 1
            if not paired and file_found == 1:
                return True
            if paired and file_found == 2:
                return True
        return False

    def download_fastq(
        self,
        sra_accession: str,
        paired: bool,
        output_path: str,
    ):
        """Download SRA by accession to the given output_path

        Args:
            sra_accession (str): SRA to download
            paired (bool): Is the SRA paired or not
            output_path (str): Path to download folder
        """
        dl_didnt_worked = True
        while dl_didnt_worked:
            split_files = "--split-files" if paired else ""
            if self.USE_FASTERQ:
                command = f"fasterq-dump {split_files} -t {self.FASTERQ_TMPDIR} -O {output_path} {sra_accession}"
            else:
                command = f"fastq-dump {split_files} -O {output_path} {sra_accession}"
            res = subprocess.run(command, shell=True, capture_output=True, text=True)
            if res.stdout:
                self.log.debug(f"stdout: {res.stdout}")
            if res.stderr:
                self.log.error(f"stderr: {res.stderr}")
                if "quit with error code 3" in res.stderr:
                    self.log.error("Restart download for crash with error code 3")
                    continue
            dl_didnt_worked = self.has_dl_crashed(sra_accession, output_path)
            if dl_didnt_worked:
                os.system(f"rm -rf {os.path.join(self.FASTERQ_TMPDIR, 'fasterq.tmp')}")

    def has_dl_crashed(self, sra: str, output: str) -> bool:
        sra_dl = glob.glob(os.path.join(output, sra) + "*")
        if sra_dl:
            return False
        else:
            self.log.error(f"{sra} not downloaded, {sra_dl}")
            self.log.debug(f"restart download {sra}")
            return True

    def fastq_compressed(self, sra_accession: str) -> bool:
        """Test if the fastq are compressed

        Args:
            sra_accession (str): SRA accession to investigate

        Returns:
            bool: True if the fastq are compresseq, else Fasle
        """
        list_fastq = os.listdir(self.DOWNLOAD_DIR)
        gz_find = False
        not_compressed_find = False
        for fastq in list_fastq:
            if sra_accession in fastq and fastq.endswith(".fastq"):
                not_compressed_find = True
            if sra_accession in fastq and fastq.endswith(".gz"):
                gz_find = True
        if not_compressed_find and not gz_find:
            return False
        # While compressing, the 2 files (compressed and not compressed) are present
        elif gz_find and not_compressed_find:
            return False
        elif gz_find and not not_compressed_find:
            return True
        else:
            return False

    def compress_fastq(self, sra_accession: str, paired: bool):
        """Compress fastq in download dir by accession

        Args:
            sra_accession (str): SRA accession which fastq have to be compressed
            paired (bool): Library layout is paired or not
        """
        if paired:
            self.gzip(f"{self.DOWNLOAD_DIR}/{sra_accession}_1.fastq")
            self.gzip(f"{self.DOWNLOAD_DIR}/{sra_accession}_2.fastq")
        else:
            self.gzip(f"{self.DOWNLOAD_DIR}/{sra_accession}.fastq")

    def gzip(self, file_path: str) -> None:
        """Compress the file given in .gz

        Args:
            file_path (str): Path to the file to compress
        """
        command = f"pigz {file_path} -p {self.PIPELINE_CPUS}"
        res = subprocess.run(command, shell=True, capture_output=True, text=True)
        if res.stdout:
            self.log.debug(f"stdout: {res.stdout}")
        if res.stderr:
            self.log.error(f"stderr: {res.stderr}")

    def wait_fastq_download(self, sra_accession: str, paired: bool) -> None:
        """Wait for the fastq to be downloaded and compressed

        Args:
            sra_accession (str): Accession number of the fastq to wait for.
            paired (bool): Is the fastq paired.
        """
        while True:
            if self.fastq_present(sra_accession, paired) and self.fastq_compressed(
                sra_accession
            ):
                break

    def launch_download_next_experiment_fastq(
        self, experiments: List[Tuple[str]], i: int
    ) -> None:
        """Determine the next experiment to be proceed and launch the
        download of its Fastq file(s) in a separated thread

        Args:
            experiments (List[Tuple[str]]): List of all SraExperiment
                accession and layout
            i (int): Index of the SRA already download/processed
        """
        next_exp = self.get_next_experiment(experiments, i)
        if not next_exp:
            return None
        next_sra_accession = next_exp[0]
        next_paired = True if next_exp[1] == "PAIRED" else False
        thread = threading.Thread(
            target=self.download_fastq_if_not_present,
            args=(next_sra_accession, next_paired),
        )
        thread.start()

    def get_next_experiment(
        self, experiments: List[Tuple[str]], i: int
    ) -> Optional[Tuple[str]]:
        """Find wihch experiment will be proceed next and return its
        accession and layout in a tuple

        Args:
            experiments (List[Tuple[str]]): List of all SraExperiment
                accession and layout
            i (int): Index of the SRA already download/processed

        Returns:
            Optional[Tuple[str]]: SRA accession and Layout of the next experiment,
                or None
        """
        experiments = experiments[i + 1 :]
        for exp in experiments:
            sra_accession = exp[0]
            if self.run_finished(sra_accession):
                continue
            return exp
        return None

    def launch_rna_seq_alignment(self, sra_accession: str, species_name: str) -> None:
        run_work_dir = self.get_or_create_run_work_dir(sra_accession)
        gtf_path = self.get_species_gtf_path(species_name)
        genome_fasta_path = self.get_or_create_genome_fasta(species_name)
        cur_dir = os.getcwd()
        if self.is_paired(sra_accession):
            fastq_reads = f"'{self.DOWNLOAD_DIR}/{sra_accession}_{{1,2}}.fastq.gz'"
            paired_arg = ""
        else:
            fastq_reads = f"'{self.DOWNLOAD_DIR}/{sra_accession}.fastq.gz'"
            paired_arg = "--singleEnd"
        command = (
            f"nextflow run nf-core/rnaseq -r 1.4.2 -profile {self.RNA_SEQ_ENV} "
            f"--reads {fastq_reads} --fasta {genome_fasta_path} "
            f"--gtf {gtf_path} --max_memory '{self.PIPELINE_MAX_RAM}GB' "
            f"--max_cpus {self.PIPELINE_CPUS} --aligner {self.ALIGNER} "
            f"{paired_arg} --pseudo_aligner salmon --saveAlignedIntermediates"
        )
        self.log.debug(f"command:  {command}")
        os.chdir(run_work_dir)
        self.log.debug(f"os.getcwd():  {os.getcwd()}")
        output = subprocess.run(command, shell=True, capture_output=True, text=True)
        os.chdir(cur_dir)
        self.log.debug(f"os.getcwd():  {os.getcwd()}")
        if output.stdout:
            self.log.debug(f"rna_seq stdout: {output.stdout}")
        if output.stderr:
            self.log.debug(f"rna_seq stderr: {output.stderr}")

    def get_species_gtf_path(self, species_name: str) -> str:
        """Using GTF_FILES define in init of diatomic base, get path
        to GTF.

        Args:
            species_name (str): Species to search GTF file path

        Raises:
            FileNotFoundError: When the GTF is define but not present
            FileNotFoundError: When the GTF is not define for the species

        Returns:
            str: Path to the gtf
        """
        if species_name in self.GTF_FILES:
            gtf_path = self.create_path_to_download_genomes(
                self.GTF_FILES[species_name]
            )
            if not os.path.isfile(gtf_path):
                raise FileNotFoundError(f"GTF file {gtf_path} is not found")
            return gtf_path

        raise FileNotFoundError(
            f"GTF file for {species_name} not define in constant GTF_FILES"
        )

    def get_or_create_genome_fasta(self, species_name: str) -> str:
        """Get the path to the genome fasta and return it.
        If it doesn't exists create it using info about the species in db.

        Args:
            species_name (str): Species to retrieve the genome fasta

        Returns:
            str: genome fasta path
        """
        fasta = self.select_genome_fasta_by_species(species_name)
        if fasta:
            genome_fasta_path = self.create_path_to_download_genomes(fasta)
            return genome_fasta_path
        else:
            species = species_name.replace(" ", "_")
            genome_fasta_path = self.create_path_to_download_genomes(
                f"{species}_dna.fa"
            )
            create_genome_fasta(self.session, species_name, genome_fasta_path)
            self.genomes_fasta.append(
                {"species": species_name, "filename": genome_fasta_path}
            )
            return genome_fasta_path

    def select_genome_fasta_by_species(self, species_name: str) -> Optional[str]:
        """Return the fasta of the species if it exists,
        else return None.

        Args:
            species_name (str): Species name to extract

        Returns:
            Optional[str]: The filename or None
        """
        genome = [
            fasta for fasta in self.genomes_fasta if fasta["species"] == species_name
        ]
        if len(genome) > 0:
            return genome[0]["filename"]
        else:
            return None

    def create_path_to_download_genomes(self, filename: str) -> str:
        """Return path to the file,
        Create download genomes folder if it does not exists.

        Args:
            filename (str): Name of the file

        Returns:
            str: The path to the file in the download dir
        """
        genomes_folder = os.path.join(self.DOWNLOAD_DIR, "genomes")
        file_path = os.path.join(genomes_folder, filename)
        if not os.path.exists(genomes_folder):
            os.mkdir(genomes_folder)
        return file_path

    def is_paired(self, sra_accession: str) -> bool:
        """Looking at the fastq files, inform if the SRA layout
        is paired or single.

        Args:
            sra_accession (str): SRA accession to investigate

        Raises:
            FileNotFoundError: If no fastq with the sra_accession in their names is present

        Returns:
            bool: True if the SRA layout is Paired, False if not
        """
        dl_file = os.listdir(self.DOWNLOAD_DIR)
        file_found = 0
        for file in dl_file:
            if file.startswith(sra_accession):
                file_found += 1
        if file_found == 2:
            return True
        elif file_found == 1:
            return False
        else:
            raise FileNotFoundError(f"No Fastq file found for {sra_accession}")

    def clean_run(self, sra_accession: str) -> None:
        """Delete non-required BAM and folder to save storage.
        Nextflow work folder is deleted, hisat2 non-sorted BAM, markDuplicates BAM too.
        Cleaning STAR folder is not implemented

        Args:
            sra_accession (str): SRA_accession corresponding to the run to clean
        """
        run_dir = self.get_or_create_run_work_dir(sra_accession)
        work_dir = os.path.join(run_dir, "work")
        shutil.rmtree(work_dir, ignore_errors=True)

        results_dir = os.path.join(run_dir, "results")
        hisat_bam_list = glob.glob(os.path.join(results_dir, "HISAT2/*.bam"))
        markDuplicates_bam = glob.glob(
            os.path.join(results_dir, f"markDuplicates/{sra_accession}*.bam*")
        )
        for bam in hisat_bam_list + markDuplicates_bam:
            os.remove(bam)

    def bigwig_computed(self, sra_accession: str) -> bool:
        """Test if the bigwig has been computed

        Args:
            sra_accession (str): Accession of SRA test

        Return:
            (bool): True if bigwig is computed else False
        """
        list_bigwig = os.listdir(self.bigwig_dir)
        for bigwig in list_bigwig:
            if bigwig.startswith(sra_accession):
                return True
        return False

    def compute_bigwig(self, sra_accession: str) -> None:
        """Create a coverage graph in BigWig format from Bam of alignements

        Args:
            sra_accession (str): Accession of SRA to convert in BigWig
        """
        run_dir = self.get_or_create_run_work_dir(sra_accession)
        result_dir = os.path.join(run_dir, "results")
        if self.ALIGNER == "hisat2":
            bam_dir = os.path.join(result_dir, "HISAT2", "aligned_sorted")
        else:
            bam_dir = os.path.join(result_dir, "STAR")
        bam_path = os.path.join(bam_dir, f"{sra_accession}_1.sorted.bam")
        # Some SRA doesn't have the '_1' at the end
        if not os.path.exists(bam_path):
            bam_path = os.path.join(bam_dir, f"{sra_accession}.sorted.bam")

        output_path = os.path.join(self.bigwig_dir, f"{sra_accession}.bw")

        command = (
            f"bamCoverage -b {bam_path} -o {output_path} -p {self.PIPELINE_CPUS} -bs 10"
        )
        res = subprocess.run(command, shell=True, capture_output=True, text=True)
        if res.stdout:
            self.log.debug(f"bamCoverage stdout: {res.stdout}")
        if res.stderr:
            self.log.debug(f"bamCoverage stderr: {res.stderr}")

    def delete_fastq(self, sra_accession: str) -> None:
        """Retrieve fastq file for the SRA accession given and delete them

        Args:
            sra_accession (str): Accession of the SRA to delete it's fastq
        """
        download_wildcard = os.path.join(self.DOWNLOAD_DIR, f"{sra_accession}*.fastq*")
        list_fastq = glob.glob(download_wildcard)
        for fastq in list_fastq:
            os.remove(fastq)

    def launch_diff_analysis(self, species_name: str) -> None:
        if not os.path.exists(self.COMPARISON_TABLE):
            raise FileNotFoundError(
                f"COMPARISON_TABLE not found at {self.COMPARISON_TABLE}"
            )
        if not os.path.exists(self.RUNS_GROUP_TABLE):
            raise FileNotFoundError(
                f"RUNS_GROUP_TABLE not found at {self.RUNS_GROUP_TABLE}"
            )

        output_file = os.path.join(
            self.DIFF_EXPRESSION_OUTPUT,
            species_name.lower().replace("-", "").replace(" ", "_")
            + "_diff_expression.tsv",
        )

        blacklist = self.format_bioproject_blacklist()
        command = (
            f"Rscript {self.DIATOMICBASE_PATH}/src/app/pipeline/RNA_seq/DESeq2_precomp.R {self.WORK_DIR} "
            f"{self.COMPARISON_TABLE} {self.RUNS_GROUP_TABLE} '{species_name}' {output_file} '{blacklist}'"
        )
        self.log.debug(f"command: {command}")
        res = subprocess.run(command, shell=True, capture_output=True, text=True)
        if res.stdout:
            self.log.debug(f"stdout: {res.stdout}")
        if res.stderr:
            self.log.error(f"stderr: {res.stderr}")

    def format_bioproject_blacklist(self):
        blacklist = ""
        for bioproject in self.BIOPROJECT_BLACKLIST:
            blacklist += f"{bioproject};"
        return blacklist
