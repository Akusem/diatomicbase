#!/usr/bin/env Rscript

library(ggplot2)
library(reshape)
library(viridis)


#### Passing argument to R ###

args = commandArgs(trailingOnly = TRUE)
if (length(args) == 0) {
  stop(
    "You must supply 6 arguments:
      [1] = Read counts file for all the experiments (diff_expr_featureCounts.tsv)
      [2] = Comparison file with at least 2 columns comp_id & keyword (comp.tab)
      [3] = Genes of interest file with at least 3 columns Group & Gene_name & GeneID  (GoI.tab)
      [4] = output file is not mandatory [default = out.pdf]
      [5] = Limit for the log2FC colour scale [number between 1.5 and 10, default = 5 ]"
    ,
    call. = FALSE
  )
} else if (length(args) == 4) {
  # default output file
  args[4] = "out.pdf"
  args[5] = 5
}

compDE.db <- read.table(args[1],
                        header = T,
                        sep = "\t",
                        dec = ".")
comp.tab.db <- read.table(args[2], header = T)
GoI.ini <- read.table(args[3], header = T, sep = "\t")

error <- setdiff(GoI.ini$GeneID, compDE.db$GeneID)
###  Si error n'est pas vide, print la liste en message d'erreur
if (length(error) != 0) {
  print(error)
  print(
    "These gene IDs are not corresponding to our database, they will be removed from the analysis"
  )
}

GoI.db <- GoI.ini[!GoI.ini$GeneID %in% error, ]

# Max number of genes to plot on a A4: 75
# Max number of comparisons: 40

pdf(args[4], height = 11.75, width = 8.25)

for (i in seq(1, nrow(GoI.db), by = 70)) {
  GoI <- GoI.db[c(i:(i + 69)), ]
  for (j in seq(1, nrow(comp.tab.db), by = 42)) {
    comp.tab <- comp.tab.db[c(j:(j + 41)), ]
    comp.tab <- comp.tab[!is.na(comp.tab[, 1]), ]
    
    project <- merge(GoI, compDE.db,  by = "GeneID")
    
    ###### Prepare the table in long format ####
    
    db_long <- melt(project[, -1])
    db_long$variable <- gsub("_log2FC", "", db_long$variable)
    padj_long <- melt(project[, -1])
    padj_long$variable <- gsub("_padj", "", padj_long$variable)
    colnames(padj_long)[c(2:4)] <- c("Genes", "Comparisons", "padj")
    
    db_select <-
      merge(
        db_long,
        comp.tab,
        by.x = "variable",
        by.y = "comp_id",
        all.y = T
      )
    colnames(db_select)[c(1:4)] <-
      c("Comparisons", "Gene_Groups", "Genes", "log2FC")
    db_padj <-
      merge(db_select,
            padj_long,
            by = c("Comparisons", "Genes"),
            all.y = F)
    db_padj$log2FC[db_padj$padj > 0.05] <- 0
    
    ###### Draw the heatmap ####
    
    lim <- scan(
      text = args[5],
      what = double(),
      sep = ",",
      quiet = TRUE
    )
    level_order <- comp.tab$comp_id
    db_padj$log2FC[db_padj$log2FC > lim] <- lim
    db_padj$log2FC[db_padj$log2FC < -lim] <- -lim
    
    plot <-
      ggplot(db_padj, aes(
        x = factor(Comparisons, level = level_order) ,
        y = Genes,
        fill = log2FC
      )) +
      geom_tile() +
      scale_y_discrete(limits = rev) +
      scale_fill_viridis(
        name = "log2FC",
        option = "E",
        discrete = FALSE,
        values = scales::rescale(c(-8,-2, 0, 2, 8)),
        breaks = c(-lim, -lim / 2, 0, lim / 2, lim),
        label = c(paste("<-", lim), -lim / 2, "0", lim /
                    2, paste(">", lim))
      ) +
      theme(
        axis.text.x = element_text(
          angle = 90,
          vjust = 0.5,
          hjust = 1,
          size = 9
        ),
        strip.text.y = element_text(angle = 0),
        strip.text.x = element_text(angle = 90)
      ) +
      facet_grid(
        rows = vars(db_padj$Gene_Groups),
        cols = vars(db_padj$keyword),
        scales = "free",
        space = "free"
      ) +
      xlab("Comparisons")
    print(plot)
  }
}

dev.off()
