import os
import re
import time
import json
import logging
import tempfile
import subprocess
import pandas as pd
from io import StringIO
from typing import List, Dict, Union
from datetime import datetime, timedelta
from multiprocessing import Pool
from sqlalchemy.orm import Session
from app import (
    LOG_FILE_PATH,
    BLAST_WORK_DIR,
    USER_R_ANALYSIS,
    RNA_SEQ_WORK_DIR,
    DIATOMICBASE_PATH,
    RNA_SEQ_RUNS_GROUP_TABLE,
)
from app.logger import create_logger
from app.db.models import ProcessQueue
from app.db import SessionLocal
from app.api.utils import get_species_diff_expr_path


def blastn_worker(res: ProcessQueue, log: logging.Logger):
    log.info(f"Launch Blastn worker for process {res.id}")
    try:
        # Create Session here because it cannot be pass in subprocess
        session = SessionLocal()
        log.debug(f"res.id: {res.id}")
        parameter = json.loads(res.parameter)
        log.debug(f"parameter: {parameter}")

        blast_db = os.path.join(BLAST_WORK_DIR, "nucleotide_db", "all_gene")
        blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
        blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
        input_fasta = f"{blast_input_dir}/{res.id}.fa"
        output_blast = f"{blast_output_dir}/{res.id}.xml"

        evalue = parameter["evalue"]
        word_size = parameter["word_size"]
        perc_identity = parameter["perc_identity"]
        ungapped = True if parameter["ungapped"] == "True" else False
        seq: str = re.sub("^ ", "", parameter["seq"])
        write_input_fasta(seq, input_fasta, res, log)

        # Launch BLASTn
        command = (
            f"blastn -query {input_fasta} -db {blast_db} -out {output_blast} -outfmt 5 "
            f"-evalue {evalue} -word_size {word_size} -perc_identity {perc_identity} {'-ungapped' if ungapped else ''}"
        )
        log.debug(f"command: {command}")
        res_sub = subprocess.run(
            command,
            shell=True,
            capture_output=True,
            text=True,
        )
        log.debug(f"after subprocess")
        # Log output
        if res_sub.stdout:
            log.debug(f"stdout: {res_sub.stdout}")
        if res_sub.stderr:
            log.debug(f"stderr: {res_sub.stderr}")
        # Delete input fasta
        os.remove(input_fasta)
        change_process_status(session, res, done=True, reloading=True)
        log.info(f"End blastn worker for id {res.id}")
    except Exception as error:
        log.error(f"Exception catched in blastn_worker: {str(error)}")
        change_process_status(
            session, res, error=True, parameter=str(error), reloading=True
        )


def write_input_fasta(seq: str, input_fasta: str, res: ProcessQueue, log):
    isFasta = False
    log.debug(f"seq: {seq}")
    if seq.startswith(">"):
        isFasta = True
    log.debug(f"isFasta: {isFasta}")
    # Add fasta header
    if not isFasta:
        seq = f">input_seq_{res.id}\n{seq}"
    log.debug(f"seq: {seq}")
    # Create fasta
    try:
        open(input_fasta, "w").write(seq)
    except Exception as err:
        log.error(err)


def blastp_worker(res: ProcessQueue, log: logging.Logger):
    log.info(f"Launch Blastp worker for process {res.id}")
    try:
        # Create Session here because it cannot be pass in subprocess
        session = SessionLocal()
        log.debug(f"res.id: {res.id}")
        parameter = json.loads(res.parameter)
        log.debug(f"parameter: {parameter}")

        blast_db = os.path.join(BLAST_WORK_DIR, "protein_db", "diamond_db")
        blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
        blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
        input_fasta = f"{blast_input_dir}/{res.id}.fa"
        output_blast = f"{blast_output_dir}/{res.id}.xml"

        seq: str = re.sub("^ ", "", parameter["seq"])
        write_input_fasta(seq, input_fasta, res, log)

        evalue = parameter["evalue"]
        min_score = int(parameter["min_score"])  # 0%
        query_cover = int(parameter["query_cover"])  # 0%
        subject_cover = int(parameter["subject_cover"])  # 0%
        perc_identity = int(parameter["perc_identity"])  # 0%

        command = (
            f"diamond blastp -q {input_fasta} -d {blast_db} -o {output_blast} -f 5 -p 1 "
            f"-e {evalue} "
            f"{f'--min-score {min_score}' if min_score > 0 else ''} "
            f"{f'--id {perc_identity}' if perc_identity > 0 else ''} "
            f"{f'--query-cover {query_cover}' if query_cover > 0 else ''} "
            f"{f'--subject-cover {subject_cover}' if subject_cover > 0 else ''} "
        )
        log.debug(f"command: {command}")

        # Launch BLASTp
        res_sub = subprocess.run(
            command,
            shell=True,
            capture_output=True,
            text=True,
        )
        log.debug(f"after subprocess")
        # Log output
        if res_sub.stdout:
            log.debug(f"stdout: {res_sub.stdout}")
        if res_sub.stderr:
            log.debug(f"stderr: {res_sub.stderr}")
        # Delete input fasta
        os.remove(input_fasta)
        change_process_status(session, res, done=True, reloading=True)
        log.info(f"End blastp worker for id {res.id}")
    except Exception as error:
        log.error(f"Exception catched in blastp_worker: {str(error)}")
        change_process_status(
            session, res, error=True, parameter=str(error), reloading=True
        )


def blastx_worker(res: ProcessQueue, log):
    log.info(f"Launch Blastx worker for process {res.id}")
    try:
        # Create Session here because it cannot be pass in subprocess
        session = SessionLocal()
        log.debug(f"res.id: {res.id}")
        parameter = json.loads(res.parameter)
        log.debug(f"parameter: {parameter}")

        blast_db = os.path.join(BLAST_WORK_DIR, "protein_db", "diamond_db")
        blast_input_dir = os.path.join(BLAST_WORK_DIR, "blast_input")
        blast_output_dir = os.path.join(BLAST_WORK_DIR, "blast_output")
        input_fasta = f"{blast_input_dir}/{res.id}.fa"
        output_blast = f"{blast_output_dir}/{res.id}.xml"

        seq: str = re.sub("^ ", "", parameter["seq"])
        write_input_fasta(seq, input_fasta, res, log)

        evalue = parameter["evalue"]
        min_score = int(parameter["min_score"])
        query_cover = int(parameter["query_cover"])
        subject_cover = int(parameter["subject_cover"])
        perc_identity = int(parameter["perc_identity"])

        command = (
            f"diamond blastx -q {input_fasta} -d {blast_db} -o {output_blast} -f 5 -p 1 "
            f"-e {evalue} "
            f"{f'--min-score {min_score}' if min_score > 0 else ''} "
            f"{f'--id {perc_identity}' if perc_identity > 0 else ''} "
            f"{f'--query-cover {query_cover}' if query_cover > 0 else ''} "
            f"{f'--subject-cover {subject_cover}' if subject_cover > 0 else ''} "
        )
        log.debug(f"command: {command}")

        # Launch BLASTx
        res_sub = subprocess.run(
            command,
            shell=True,
            capture_output=True,
            text=True,
        )
        log.debug(f"after subprocess")
        # Log output
        if res_sub.stdout:
            log.debug(f"stdout: {res_sub.stdout}")
        if res_sub.stderr:
            log.debug(f"stderr: {res_sub.stderr}")
        # Delete input fasta
        os.remove(input_fasta)
        change_process_status(session, res, done=True, reloading=True)
        log.info(f"End blastx worker for id {res.id}")
    except Exception as error:
        log.error(f"Exception catched in blastx_worker: {str(error)}")
        change_process_status(
            session, res, error=True, parameter=str(error), reloading=True
        )


def diff_expr_worker(res: ProcessQueue, log):
    log.info(f"Launch diff_expr worker for process {res.id}")
    # Create Session here because it cannot be pass in subprocess
    session = SessionLocal()
    try:
        if not os.path.exists(RNA_SEQ_RUNS_GROUP_TABLE):
            raise FileNotFoundError(
                f"RNA_SEQ_RUNS_GROUP_TABLE not found at {RNA_SEQ_RUNS_GROUP_TABLE}"
            )

        comparison = pd.read_csv(StringIO(res.parameter), sep="\t")
        runs_group = pd.read_csv(open(RNA_SEQ_RUNS_GROUP_TABLE, "r"), sep="\t")
        SRRs = comparison["run"].to_list()
        SRRs_to_sample = get_SRRs_to_sample_and_manage_duplicate(SRRs, runs_group)
        read_files = get_read_files_path(SRRs)
        experiment_design_file = get_experiment_design(comparison, SRRs_to_sample)
        idep_read_file = concat_read_files(read_files, SRRs, SRRs_to_sample)
        read_file_name = write_read_and_experiment_file(
            res.id, idep_read_file, experiment_design_file
        )
        change_process_status(
            session, res, done=True, parameter=read_file_name, reloading=True
        )
        log.info(f"End diff_expr worker for id {res.id}")
    except Exception as error:
        log.error(f"Exception catched in diff_expr_worker: {str(error)}")
        change_process_status(
            session, res, error=True, parameter=str(error), reloading=True
        )


def transcriptomics_heatmap_worker(res: ProcessQueue, log):
    log.info(f"Launch transcriptomics_heatmap worker for process {res.id}")
    # Create Session here because it cannot be pass in subprocess
    session = SessionLocal()

    try:
        # Load info
        param = json.loads(res.parameter)
        log.info(f"parameter: {param}")

        R_script_path = os.path.join(
            DIATOMICBASE_PATH,
            "back-end/src/app/pipeline/RNA_seq/transcriptomics_heatmap.R",
        )

        path_to_diff_expr = get_species_diff_expr_path(param["species"])

        # Rebuild path
        gene_subset_path = os.path.join(USER_R_ANALYSIS, param["gene_subset_file"])
        comparison_tsv_path = os.path.join(
            USER_R_ANALYSIS, param["comparison_tsv_file"]
        )
        output_path = os.path.join(USER_R_ANALYSIS, param["output_name_file"])

        command = (
            f"Rscript {R_script_path} {path_to_diff_expr} {comparison_tsv_path} "
            f" {gene_subset_path} {output_path} {param['log2FCLimit']}"
        )
        print("command:", command)
        # Launch R script
        res_sub = subprocess.run(
            command,
            shell=True,
            capture_output=True,
            text=True,
        )
        log.debug(f"after subprocess")
        # Log output
        if res_sub.stdout:
            log.debug(f"stdout: {res_sub.stdout}")
        if res_sub.stderr:
            log.debug(f"stderr: {res_sub.stderr}")

        # Clean input
        os.remove(gene_subset_path)
        os.remove(comparison_tsv_path)
        # Remove process
        change_process_status(session, res, done=True, reloading=True)
        log.info(f"End transcriptomics_heatmap_worker for id {res.id}")

    except Exception as error:
        log.error(f"Exception catched in transcriptomics_heatmap_worker: {error}")
        change_process_status(
            session, res, error=True, parameter=str(error), reloading=True
        )


def get_SRRs_to_sample_and_manage_duplicate(
    SRRs: List[str], runs_group: pd.DataFrame
) -> Dict[str, str]:
    """
    Create a dict with SRR as key, and sample_name as value.

    Automaticaly add number at the end of duplicated sample_name to avoid conflict
    """
    sample_name_to_SRR = {}
    presence_counter = {}
    for SRR in SRRs:
        sample_name = runs_group.loc[runs_group["run"] == SRR]["sample_name"].item()
        # In case of duplicate, this 'if else' will add a number at the end of their sample_name
        if not sample_name in presence_counter:
            presence_counter[sample_name] = 1
        else:
            presence_counter[sample_name] += 1
            # if this sample_name is duplicated, add '_1' to the first instance
            if presence_counter[sample_name] == 2:
                sample_name_to_SRR[sample_name + "_1"] = sample_name_to_SRR[sample_name]
                del sample_name_to_SRR[sample_name]
            # Add _{n} to have different key
            sample_name = sample_name + "_" + str(presence_counter[sample_name])
        sample_name_to_SRR[sample_name] = SRR

    # Invert Key Item to have SRR to sample
    SRRs_to_sample = {v: k for k, v in sample_name_to_SRR.items()}
    return SRRs_to_sample


def get_read_files_path(srrs: List[str]) -> List[str]:
    return [
        os.path.join(
            RNA_SEQ_WORK_DIR, srr, "results/featureCounts/merged_gene_counts.txt"
        )
        for srr in srrs
    ]


def get_experiment_design(
    comparison_table: pd.DataFrame, SRRs_to_sample: Dict[str, str]
) -> pd.DataFrame:
    experiment_file = pd.DataFrame(index=["group"])
    for run, group in zip(
        comparison_table["run"].to_list(), comparison_table["subset_group"].to_list()
    ):
        sample_name = SRRs_to_sample[run]
        experiment_file[sample_name] = [group]

    return experiment_file


def concat_read_files(
    read_files: List[str], srrs: List[str], sample: Dict[str, str]
) -> pd.DataFrame:
    read_file = ""
    for i, file_name in enumerate(read_files):
        file = pd.read_csv(file_name, sep="\t")
        run_name = get_run_name(file, srrs[i])
        sample_name = sample[srrs[i]]
        if i == 0:
            read_file = pd.DataFrame(
                {sample_name: file[run_name].to_list()}, file["Geneid"]
            )
        else:
            read_file[sample_name] = file[run_name].to_list()
    return read_file


def get_run_name(file: pd.DataFrame, name: str) -> str:
    """Add if needed _1 at the end of run_name

    Args:
        file (pd.DataFrame): run file
        name (str): SRR code

    Returns:
        str: SRR code with _1 added if needed
    """
    colnames = file.columns
    colnames = [col for col in colnames if col.startswith(name)]
    run_name = f"{name}_1" if colnames[0].endswith("_1") else name
    return run_name


def write_read_and_experiment_file(
    id, read_file: pd.DataFrame, experiment_file: pd.DataFrame
) -> None:
    if not os.path.exists(USER_R_ANALYSIS):
        os.makedirs(USER_R_ANALYSIS)
    tmp_file = tempfile.NamedTemporaryFile(
        mode="w", delete=False, prefix=str(id), dir=USER_R_ANALYSIS
    )
    read_file.to_csv(tmp_file.name, sep="\t")
    experiment_file.to_csv(tmp_file.name + "_design", sep="\t")
    return os.path.basename(tmp_file.name)


def select_worker(
    worker_type: Union[
        "blastp", "blastx", "blastn", "diff_expr", "transcriptomics_heatmap"
    ]
):
    if worker_type == "blastp":
        return blastp_worker
    elif worker_type == "blastn":
        return blastn_worker
    elif worker_type == "blastx":
        return blastx_worker
    elif worker_type == "diff_expr":
        return diff_expr_worker
    elif worker_type == "transcriptomics_heatmap":
        return transcriptomics_heatmap_worker


def queue_manager(n_process: int):
    logger = create_logger("DiatOmicBase.queue_manager", LOG_FILE_PATH)
    session: Session = SessionLocal()

    pool = Pool(n_process)
    while True:
        time.sleep(5)
        process_queue: List[ProcessQueue] = session.query(ProcessQueue).all()

        if not process_queue:
            continue
        for process in process_queue:
            session.refresh(process)
            if process.status == "queueing":
                logger.info(
                    f"Launching worker {process.worker_type} for process {process.id}"
                )
                worker = select_worker(process.worker_type)
                pool.apply_async(worker, args=(process, logger))
                change_process_status(session, process, running=True)
            if process.status == "done" or process.status == "error":
                if time_passed(process.input_date):
                    clean_process_output(session, process, logger)
            if process.status == "running":
                logger.info(f"Process {process.id} running")
                if time_passed(process.input_date):
                    clean_process_output(session, process, logger)


def submit_process(
    session: Session,
    worker_type: Union[
        "blastp", "blastx", "blastn", "diff_expr", "transcriptomics_heatmap"
    ],
    parameter: str,
):
    """Submit process to background worker and return process ID

    Args:
        session (Session): A SQLAlchemy Session object to access db
        worker_type (Union["blastp", "blastx", "blastn", "diff_expr"]): The worker type
        parameter (str): The data to pass to the worker

    Raises:
        TypeError: When the worker_type is not correct

    Returns:
        int: The process id
    """
    possible_worker_type = [
        "blastp",
        "blastx",
        "blastn",
        "diff_expr",
        "transcriptomics_heatmap",
    ]
    if worker_type not in possible_worker_type:
        raise TypeError(
            f"{worker_type} cannot be used as worker type, "
            f"use one of those type instead {','.join(possible_worker_type)}"
        )
    date = datetime.now()
    process = ProcessQueue(
        worker_type=worker_type, parameter=parameter, input_date=date, status="queueing"
    )
    session.add(process)
    session.commit()
    return process.id


def change_process_status(
    session: Session,
    process: ProcessQueue,
    parameter=None,
    running=False,
    done=False,
    error=False,
    delete=False,
    reloading=False,
) -> None:
    """Change ProcessQueue status as desired, or delete it.

    Args:
        session (Session): A SQlAlchemy Session object to access db
        process (ProcessQueue): The ProcessQueue of interest
        paramaeter (Dict, optional): Define it to change process parameter. Defaults to None.
        running (bool, optional): Set to True to change status to running. Defaults to False.
        done (bool, optional): Set to True to change status to done. Defaults to False.
        error (bool, optional): Set to True to change status to error. Defaults to False.
        delete (bool, optional): Set to True to delete process from queue. Defaults to False.
        reloading (bool, optional): Set to True to requery ProcessQueue
                            (needed when using a different Session object than
                            the one that queried the process). Defaults to False.
    """
    if reloading:
        process = (
            session.query(ProcessQueue).filter(ProcessQueue.id == process.id).first()
        )

    if parameter:
        process.parameter = parameter

    if delete:
        session.delete(process)
    elif running:
        process.status = "running"
    elif done:
        process.status = "done"
    elif error:
        process.status = "error"
    session.commit()


def clean_process_output(
    session: Session, process: ProcessQueue, log: logging.Logger
) -> None:
    """Remove output file of a process after a certain time, and remove the
    process itself of the queue.

    Args:
        session (Session): A SQLAlchemy Session object to access db
        process (ProcessQueue): The process to clean
        log (logging.Logger): The Logger
    """
    log.info(f"Deleting {process.id} output file and process in queue")
    blast_process = ["blastn", "blastx", "blastp"]
    if process.worker_type in blast_process:
        blast_output_path = os.path.join(
            BLAST_WORK_DIR, "blast_output", f"{process.id}.xml"
        )
        delete_file(blast_output_path)
    elif process.worker_type == "diff_expr":
        diff_expr_output_path = process.parameter
        delete_file(diff_expr_output_path)
    elif process.worker_type == "transcriptomics_heatmap":
        param = json.loads(process.parameter)
        output_path = os.path.join(USER_R_ANALYSIS, param["output_name_file"])
        delete_file(output_path)
    else:
        raise TypeError("Worker type not present")

    change_process_status(session, process, delete=True)


def time_passed(date, days=2):
    """Tell if the number of days define have passed sinced the date given

    Args:
        date (datetime): start date
        days (int, optional): Number of days until time is passed. Defaults to 2.

    Returns:
        bool: True if time as passed else False
    """
    now = datetime.now()
    delta = timedelta(days=days)
    passed = date + delta
    if now > passed:
        return True
    else:
        return False


def delete_file(filename: str):
    try:
        os.remove(filename)
    except FileNotFoundError:
        pass
