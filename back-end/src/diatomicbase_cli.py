import os
import sys
from typing import Optional, Union
from csv import DictReader
from typing import List, Dict
from pathlib import Path
from app import LOG_FILE_PATH, ENSEMBL_REST, API_DATA_FOLDER_PATH
from app.logger import create_logger
from app.db import SessionLocal
from app.db.models import (
    Phatr2Proteogenomics,
    DomainAnnotation,
    GeneNameAlias,
    GeneOntology,
    ProcessQueue,
    Species,
    Genome,
    Gene,
    Kegg,
)
from app.db.diagram import generate_db_diagram
from app.load_db.loader import loader
from app.load_db.manual_loader import manual_loader, assembly_sequence_loader
from app.load_db.utils import fetch_endpoint
from app.pipeline.RNA_seq.main import RNASeqManager
from app.pipeline.BLAST.database_setup import BLASTDatabaseSetup
from app.pipeline.background_worker import clean_process_output, change_process_status
from InquirerPy import inquirer
from sqlalchemy.orm import Session
from InquirerPy.validator import Validator, ValidationError, PathValidator
from InquirerPy.base.control import Choice

session: Session = SessionLocal()

log = create_logger("DiatOmicBase.cli", LOG_FILE_PATH)


def main():
    answer = inquirer.select(
        message="Choose one of the options:",
        choices=[
            "Add New Species (Manual & Automatic)",
            "Launch automatic loader",
            "Add data manually",
            "Clean Process Queue",
            "Launch RNA-seq proccessing",
            "Launch only RNA-seq differential expression computing",
            "Generate Database diagram",
            "Create BLAST Database",
            "Exit",
        ],
    ).execute()

    if answer == "Exit":
        sys.exit(0)
    elif answer == "Add New Species (Manual & Automatic)":
        add_new_species()
    elif answer == "Launch automatic loader":
        loader_cli()
    elif answer == "Add data manually":
        manual_data_selector()
    elif answer == "Clean Process Queue":
        clean_process_queue()
    elif answer == "Launch RNA-seq proccessing":
        species = choose_species(
            "Select on which species the pipeline should be launched:"
        )
        RNASeqManager(session).run(species)
    elif answer == "Launch only RNA-seq differential expression computing":
        species = choose_species(
            "Select on which species the pipeline should be launched:"
        )
        RNASeqManager(session).launch_diff_analysis(species)
    elif answer == "Generate Database diagram":
        generate_sql_graph()
    elif answer == "Create BLAST Database":
        BLASTDatabaseSetup(session).create_db()


def manual_data_selector():
    answer = inquirer.select(
        message="Choose one of the options:",
        choices=[
            "Add New Species",
            "Add New Assembly",
            "Add Chromosomes Sequences through fasta",
            "Add Gene/Transcript/Exon/CDS through GFF",
            "Add/Update Phatr2Proteogenomic",
            "Add/Update gene annotation (KEGG, GO, Domain)",
            "Add/Update Alias gene name",
            "Exit",
        ],
    ).execute()

    if answer == "Exit":
        sys.exit(0)
    elif answer == "Add New Species":
        add_new_species_manually()
    elif answer == "Add New Assembly":
        add_new_assembly()
    elif answer == "Add Chromosomes Sequences through fasta":
        add_seq_region_sequence()
    elif answer == "Add Gene/Transcript/Exon/CDS through GFF":
        gff_loader()
    elif answer == "Add/Update Phatr2Proteogenomic":
        phatr2_proteogenomic()
    elif answer == "Add/Update gene annotation (KEGG, GO, Domain)":
        add_annotation()
    elif answer == "Add/Update Alias gene name":
        load_alias_gene_name()


def choose_species(
    message: str = "Select a species:", as_object=False
) -> Union[Species, str]:
    species = session.query(Species).all()
    species_name = [s.name for s in species]
    selected = inquirer.select(message, species_name).execute()
    if as_object:
        return session.query(Species).filter_by(name=selected).first()
    return selected


def choose_assembly(species_name: str, message: str = "Select an assembly:") -> Genome:
    species = session.query(Species).filter(Species.name == species_name).first()
    assemblies = [assembly.assembly_name for assembly in species.assemblies]
    # No need to select assembly if only one is present
    if len(assemblies) == 1:
        return species.assemblies[0]
    selected = inquirer.select(message, assemblies).execute()
    return session.query(Genome).filter(Genome.assembly_name == selected).first()


def add_new_species():
    # Select Manual or use Ensembl auto completion
    manual = inquirer.select(
        "Do you want to use Ensembl to create the Species ?", ["Use Ensembl", "Manual"]
    ).execute()
    manual = True if manual == "Manual" else False

    if not manual:
        # Fetch species available in Ensembl
        print("Getting list of species available in EnsemblProtists")
        result = fetch_endpoint(
            ENSEMBL_REST, "info/species?division=EnsemblProtists", "application/json"
        )
        species_list = [res["display_name"] for res in result["species"]]
        # Choose species
        species_chose = inquirer.fuzzy(
            "Which species do you want to add:", species_list
        ).execute()
        species_data = [
            res for res in result["species"] if res["display_name"] == species_chose
        ][0]
        species = add_species_in_db(
            name=species_chose,
            ensembl_name=species_data["name"],
            accession=species_data["accession"],
        )
        # Ask to launch automatic loader
        launch_loader = inquirer.confirm(
            "Do you want to launch the automatic loader ?", default=True
        ).execute()
        if launch_loader:
            loader_cli(species.name)
    else:
        species = add_new_species_manually()
        # Launch manual loader
        launch_manual_loader = inquirer.confirm(
            "Do you want to launch the manual loader ?", default=True
        ).execute()
        if launch_manual_loader:
            print("Adding new Assembly in db")
            assembly = add_new_assembly(species)
            fasta_loader(assembly)
            gff_loader(species, assembly)


def add_new_species_manually() -> Species:
    # Get species field manually
    species_name: str = inquirer.text("Enter the species name: ").execute().strip()
    species_ensembl_name = (
        inquirer.text(
            "Enter the species ensembl name (typically lowercase and connected by underscore): ",
            default=species_name.lower().replace(" ", "_").replace("-", ""),
        )
        .execute()
        .strip()
    )
    species_accession = inquirer.text("Enter the accession number: ").execute().strip()
    species = add_species_in_db(species_name, species_ensembl_name, species_accession)
    return species


def add_species_in_db(name: str, ensembl_name: str, accession: str) -> Species:
    species_object = Species(
        name=name,
        ensembl_name=ensembl_name,
        accession=accession,
    )
    # Confirm species info before inserting in db
    print("Species name:", species_object.name)
    print("Species ensembl_name:", species_object.ensembl_name)
    print("Species accession:", species_object.accession)
    insert = inquirer.confirm("Insert species in database ?", default=False).execute()
    if insert:
        # Adding species
        session.add(species_object)
        session.commit()
        print(f"Species {name} added in database")
        return species_object
    else:
        print("Abort species insertion. Exiting...")
        sys.exit(0)


def add_new_assembly(species: Species = None) -> Genome:
    # If use directly (not imported) select species
    if not species:
        species = choose_species(as_object=True)
    # Get required fields
    name = inquirer.text(
        "Enter assembly name:", instruction="E.g. ASM15095v2"
    ).execute()
    accession = inquirer.text(
        "Enter assembly accession:", instruction="E.g. GCA_000150955.2"
    ).execute()
    date = inquirer.text(
        "Enter assembly date (In format 2xxx-0x) :", instruction="E.g. 2014-05"
    ).execute()
    assembly = Genome(
        assembly_name=name,
        assembly_accession=accession,
        assembly_date=date,
        species_id=species.id,
    )
    # Get optional fields
    base_pair = inquirer.text(
        "Enter Base pair: ", mandatory=False, instruction="Ctrl+Z to skip"
    ).execute()
    if base_pair:
        assembly.base_pair = base_pair
    genebuild_method = inquirer.text(
        "Enter Genebuild method: ", mandatory=False, instruction="Ctrl+Z to skip"
    ).execute()
    if genebuild_method:
        assembly.genebuild_method = genebuild_method
    genebuild_initial_release_date = inquirer.text(
        "Enter Genebuild initial release date: ",
        mandatory=False,
        instruction="Ctrl+Z to skip",
    ).execute()
    if genebuild_initial_release_date:
        assembly.genebuild_initial_release_date = genebuild_initial_release_date
    genebuild_start_date = inquirer.text(
        "Enter Genebuild start date: ", mandatory=False, instruction="Ctrl+Z to skip"
    ).execute()
    if genebuild_start_date:
        assembly.genebuild_start_date = genebuild_start_date
    genebuild_last_geneset_update = inquirer.text(
        "Enter Genebuild last geneset update: ",
        mandatory=False,
        instruction="Ctrl+Z to skip",
    ).execute()
    if genebuild_last_geneset_update:
        assembly.genebuild_last_geneset_update = genebuild_last_geneset_update

    session.add(assembly)
    session.commit()
    session.refresh(assembly)

    return assembly


def add_seq_region_sequence():
    species = choose_species(as_object=True)
    assembly = choose_assembly(species.name)
    fasta_loader(assembly)


def fasta_loader(assembly: Genome):
    fasta_path = inquirer.filepath(
        "Enter path to Assembly Fasta:",
        validate=PathValidator(is_file=True, message="Input is not a file"),
    ).execute()
    assembly_sequence_loader(session, assembly, fasta_path)


def loader_cli(species_name: str = None):
    if not species_name:
        species_name = choose_species("On which Species should launch loader")

    loader_choice = [
        Choice("Assembly (Ensembl)", enabled=True),
        Choice("Genome Sequence (Ensembl)", enabled=True),
        Choice("Genes (Ensembl)", enabled=True),
        Choice("Transcripts (Ensembl)", enabled=True),
        Choice("Exons (Ensembl)", enabled=True),
        Choice("CDS (Ensembl)", enabled=True),
        Choice("Xrefs (Ensembl & NCBI)", enabled=True),
        Choice("GO & Domains & KOG (Uniprot)", enabled=True),
        Choice("KOG details (eggNOG)", enabled=True),
        Choice("Domain position (UniProt)", enabled=True),
        Choice("KEGG (gProfiler)", enabled=True),
        Choice("RNA-seq MetaData (NCBI)", enabled=True),
    ]

    loader_options = inquirer.checkbox(
        message="Which steps the loader should execute",
        choices=loader_choice,
        cycle=True,
        instruction="(Ctrl+R to toggle all)",
        validate=lambda result: len(result) >= 1,
    ).execute()

    loader(loader_options, species_name)


def gff_loader(species: Species = None, assembly: Genome = None):
    if not species:
        species: Species = choose_species(as_object=True)
    if not assembly:
        assembly = choose_assembly(species.name)
    gff = inquirer.filepath(
        "Path to the GFF containing Gene features to add:",
        validate=PathValidator(is_file=True, message="Input is not a file"),
    ).execute()
    source = inquirer.text(
        "Which Source should be used if not present ?", default="GFF"
    ).execute()
    manual_loader(
        gff,
        species,
        assembly,
        source,
    )


def phatr2_proteogenomic():
    filename = get_tsv_input_file(
        column_name=[
            "gene_id",
            "phatr3_vs_phatr2_category",
            "phatr2_protein_id",
            "phatr2_gene_id",
            "detection_in_yang_2018",
            "potential_non_coding_gene",
            "post_translational_modification",
        ],
    )
    file = open_tsv(filename)
    for row in file:
        proteogenomic_already_exists = True
        if not row["phatr2_gene_id"]:
            continue
        gene = get_gene_from_name(row["gene_id"])
        if not gene:
            print(f"{row['gene_id']} doesn't exists in the database")
            continue
        proteogenomics_object = get_proteogenomics_if_exist(row["phatr2_gene_id"])
        if not proteogenomics_object:
            proteogenomic_already_exists = False
            proteogenomics_object = Phatr2Proteogenomics(name=row["phatr2_gene_id"])
        proteogenomics_object.gene_id = gene.id
        proteogenomics_object.protein_id = row["phatr2_protein_id"]
        proteogenomics_object.detection_in_yang_2018 = (
            True if row["detection_in_yang_2018"] == "Detected set" else False
        )
        proteogenomics_object.potential_non_coding_gene = (
            row["potential_non_coding_gene"],
        )
        proteogenomics_object.post_translational_modification = (
            row["post_translational_modification"],
        )
        proteogenomics_object.phatr3_vs_phatr2_category = row[
            "phatr3_vs_phatr2_category"
        ]
        if not proteogenomic_already_exists:
            session.add(proteogenomics_object)
        session.commit()


def get_gene_from_name(gene_id: str) -> Gene:
    return session.query(Gene).filter(Gene.name == gene_id).first()


def get_proteogenomics_if_exist(phatr2_id) -> Union[Phatr2Proteogenomics, None]:
    protegenomics = (
        session.query(Phatr2Proteogenomics)
        .filter(Phatr2Proteogenomics.name == phatr2_id)
        .first()
    )
    return protegenomics


def get_tsv_input_file(column_name: List[str]) -> str:
    formatted_column_name = ", ".join(column_name)
    tsv_message = (
        "Input file need to be in TSV format with thoses columns:\n"
        f"{formatted_column_name}\n"
    )
    tsv_path = inquirer.filepath(
        message=tsv_message,
        validate=TsvValidator(column_name=column_name),
    ).execute()
    return tsv_path


class TsvValidator(Validator):
    """Validator Class for get_tsv_input_file,
    Ensure input file exist and contain the good columns.
    """

    def __init__(
        self, message: str = "Input is not a valid path", column_name: List[str] = None
    ) -> None:
        self._message = message
        self._colnames = column_name

    def validate(self, document) -> None:
        path = Path(document.text).expanduser()
        if not path.exists() or not path.is_file():
            raise ValidationError(
                message=self._message,
                cursor_position=document.cursor_position,
            )

        # Verify TSV header is correct
        file = open(document.text, "r")
        first_line = file.readline().strip()
        file.close()
        list_headers_file = first_line.split("\t")
        for header in self._colnames:
            if header not in list_headers_file:
                raise ValidationError(
                    message=f"TSV column {header} not present",
                    cursor_position=document.cursor_position,
                )


def open_tsv(filename: str) -> List[Dict]:
    try:
        content = DictReader(open(filename, "r"), delimiter="\t")
        return content
    except FileNotFoundError as e:
        raise FileNotFoundError(f"{e}\n input not found")


def add_annotation():
    select = inquirer.select(
        "Which type of annotation do you want to add ?", ["KEGG", "GO", "Domains"]
    ).execute()
    if select == "KEGG":
        kegg()
    elif select == "GO":
        go()
    elif select == "Domains":
        domains()


def kegg():
    filename = get_tsv_input_file(
        column_name=["gene_id", "kegg_id", "kegg_description"]
    )
    file = open_tsv(filename)
    for row in file:
        gene = get_gene_from_name(row["gene_id"])
        if not gene:
            print(f"No gene found in db for {row['gene_id']}")
            continue
        if (
            row["kegg_id"] == "No KEGG"
            or row["kegg_id"] == "na"
            or row["kegg_id"] == "NA"
            or row["kegg_id"] == "N/A"
            or row["kegg_id"] == None
        ):
            continue
        kegg_object = get_kegg_by_accession(row["kegg_id"])
        # Create Kegg if not already present in db
        if not kegg_object:
            print(f"{row['kegg_id']} not present in db, Creating entry")
            kegg_object = Kegg(
                accession=row["kegg_id"], description=row["kegg_description"]
            )
            session.add(kegg_object)
        # Update description if it was changed
        if kegg_object.description != row["kegg_description"]:
            kegg_object.description = row["kegg_description"]
            print(f"Updating {row['kegg_id']} description")
        # Make link
        gene.kegg.append(kegg_object)
        session.commit()
        print(f'{row["kegg_id"]} added to {row["gene_id"]}')


def get_kegg_by_accession(kegg_accession: str) -> Optional[Kegg]:
    return session.query(Kegg).filter(Kegg.accession == kegg_accession).first()


def go():
    go_type_valid = ["F", "C", "P"]
    filename = get_tsv_input_file(
        column_name=["gene_id", "go_type", "go_term", "go_description"]
    )
    file = open_tsv(filename)
    for row in file:
        gene = get_gene_from_name(row["gene_id"])
        if not gene:
            print(f"No gene found in db for {row['gene_id']}")
            continue
        if (
            row["go_term"] == "No GO"
            or row["go_term"] == "na"
            or row["go_term"] == "NA"
            or row["go_term"] == "N/A"
            or row["go_term"] == None
        ):
            continue
        go_object = get_go_by_term(row["go_term"])
        # Create GO if not already present in db
        if not go_object:
            print(f"{row['go_term']} not present in db, Creating entry")
            if row["go_type"] not in go_type_valid:
                raise TypeError(
                    f"go type of {row['go_term']} is not valid, it should be one of the folowing {', '.join(go_type_valid)}"
                )
            go_object = GeneOntology(
                type=row["go_type"],
                term=row["go_term"],
                description=row["go_description"],
            )
            session.add(go_object)
        # Update description if it was changed
        if go_object.description != row["go_description"]:
            go_object.description = row["go_description"]
            print(f"Updating {row['go_term']} description")
        # Update type if it was changed
        if go_object.type != row["go_type"]:
            go_object.type = row["go_type"]
            print(f"Updating {row['go_term']} type")
        # Make link
        gene.go.append(go_object)
        session.commit()
        print(f'{row["go_term"]} added to {row["gene_id"]}')


def get_go_by_term(term: str) -> GeneOntology:
    return session.query(GeneOntology).filter_by(term=term).first()


def domains():
    filename = get_tsv_input_file(
        column_name=["gene_id", "database", "accession", "description"]
    )
    file = open_tsv(filename)
    for row in file:
        gene = get_gene_from_name(row["gene_id"])
        if not gene:
            print(f"No gene found in db for {row['gene_id']}")
            continue
        if (
            row["accession"] == "No Domain"
            or row["accession"] == "na"
            or row["accession"] == "NA"
            or row["accession"] == "N/A"
            or row["accession"] == None
        ):
            continue
        domain_object = get_domain_by_accession(row["accession"])
        # Create Domain if not already present in db
        if not domain_object:
            print(f"{row['accession']} not present in db, Creating entry")
            domain_object = DomainAnnotation(
                db_name=row["database"],
                accession=row["accession"],
                description=row["description"],
            )
            session.add(domain_object)
        # Update description if it was changed
        if domain_object.description != row["description"]:
            domain_object.description = row["description"]
            print(f"Updating {row['accession']} description")
        # Update database if it was changed
        if domain_object.db_name != row["database"]:
            domain_object.db_name = row["database"]
            print(f"Updating {row['accession']} database")
        # Make link
        gene.domains_annotation.append(domain_object)
        session.commit()
        print(f'{row["accession"]} added to {row["gene_id"]}')


def get_domain_by_accession(accession: str) -> DomainAnnotation:
    return session.query(DomainAnnotation).filter_by(accession=accession).first()


def load_alias_gene_name():
    manual = inquirer.select(
        "Do you want to manually Add/Update/Remove alias ? Or use a TSV file:",
        ["Manually update", "Through TSV file"],
    ).execute()
    manual = True if manual == "Manually update" else False
    if manual:
        load_alias_gene_name_manually()
    else:
        load_alias_gene_name_from_tsv()


def load_alias_gene_name_manually():
    genes_name = session.query(Gene.name).all()
    # Remove tuple to form a list
    genes_name = [gene_name[0] for gene_name in genes_name]
    gene_name = inquirer.fuzzy("Select the gene to update:", genes_name).execute()
    choice = inquirer.select(
        "Do you want to Add, Update, Delete a gene name alias ?",
        ["Add", "Update", "Delete"],
    ).execute()
    gene = session.query(Gene).filter_by(name=gene_name).first()
    if choice == "Add":
        existing_origin = session.query(GeneNameAlias.origin).distinct().all()
        # Extract string from tuple
        existing_origin = [origin[0] for origin in existing_origin]
        origin = inquirer.select(
            "Do you want to add an alias from one of the following Database",
            existing_origin + ["other"],
        ).execute()
        if origin == "other":
            origin = inquirer.text(
                "What is the origin of the Alias:",
                instruction="(in lowercase)",
                validate=lambda alias: alias == alias.lower(),
            ).execute()
        alias_name = inquirer.text("Enter the Alias:").execute()
        print("New alias origin: " + origin)
        print("New alias name: " + alias_name)
        confirm = inquirer.confirm(
            f"Do you really want to add alias {alias_name} from {origin} to {gene_name}"
        ).execute()
        if confirm:
            gene.name_alias.append(GeneNameAlias(origin=origin, name=alias_name))
            session.commit()
            print(f"Alias {alias_name} from {origin} have been added for {gene_name}")
        else:
            print("Abort, exiting...")
            sys.exit(0)
    elif choice == "Update":
        raise NotImplementedError("Need implementation after fix of #379")
    else:
        alias_list = [
            Choice(
                value=(alias.id, alias.origin, alias.name),
                name=f"{alias.name} from {alias.origin}",
            )
            for alias in gene.name_alias
        ]
        alias_to_delete = inquirer.select(
            "Which Alias do you want to delete", alias_list
        ).execute()
        delete = inquirer.confirm(
            f"Do you really want to delete the alias {alias_to_delete[2]} from {alias_to_delete[1]} linked with {gene_name}"
        ).execute()
        if delete:
            session.query(GeneNameAlias).filter_by(id=alias_to_delete[0]).delete()
            session.commit()
            print(f"{alias_to_delete[2]} alias is deleted")


def load_alias_gene_name_from_tsv():
    filename = get_tsv_input_file(["gene_name", "id_type", "id"])
    file = open_tsv(filename)

    for line in file:
        name = line["gene_name"]
        id_type = line["id_type"]
        alias = line["id"]
        gene = get_gene_from_name(name)
        if id_type in gene.alias:
            print(f"Updating {gene.name} {id_type} by {alias}")
        else:
            print(f"Adding {id_type} id {alias} to {gene.name}")
        gene.alias[id_type] = alias

    session.commit()


def clean_process_queue():
    running: List[ProcessQueue] = (
        session.query(ProcessQueue)
        .filter(ProcessQueue.status.in_(["running", "done", "error"]))
        .all()
    )
    for run in running:
        run.status = "done"
        print(f"Ending process {run.id}")
        clean_process_output(session, run, log)
        print(f"Clean process files {run.id}")
        change_process_status(session, run, delete=True)
        print(f"Delete process {run.id}")
    session.commit()


def generate_sql_graph():
    select = inquirer.select(
        "Which library should be used for diagram generation :",
        ["Pydot", "ERalchemy", "UML from schemadisplay", "All"],
    ).execute()
    default_path = os.path.join(API_DATA_FOLDER_PATH, "database_diagram.png")

    if select == "All":
        pydot_path = default_path.replace(".png", "_pydot.png")
        generate_db_diagram(pydot_path)
        print(f"Database diagram created with Pydot at {pydot_path}")

        er_path = default_path.replace(".png", "_er.png")
        generate_db_diagram(er_path, er=True)
        print(f"Database diagram created with ERalchemy at {er_path}")

        uml_path = default_path.replace(".png", "_uml.png")
        generate_db_diagram(uml_path, uml=True)
        print(f"Database diagram created with Pydot at {uml_path}")
        sys.exit(0)

    path = inquirer.filepath(
        "Where do you want to create these diagram :", default=default_path
    ).execute()
    if select == "Pydot":
        generate_db_diagram(path)
    elif select == "ERalchemy":
        generate_db_diagram(path, er=True)
    elif select == "UML from schemadisplay":
        generate_db_diagram(path, uml=True)


if __name__ == "__main__":
    main()
