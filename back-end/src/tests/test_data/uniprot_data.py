gff_list_line_example_1 = [
    "B7GER0",
    "UniProtKB",
    "Domain",
    "65",
    "92",
    ".",
    ".",
    ".",
    "Note=C3H1-type;Ontology_term=ECO:0000259;evidence=ECO:0000259|PROSITE:PS50103",
    "",
]
gff_list_line_example_2 = [
    "B7GER0",
    "UniProtKB",
    "Region",
    "212",
    "232",
    ".",
    ".",
    ".",
    "Note=Disordered;Ontology_term=ECO:0000256;evidence=ECO:0000256|SAM:MobiDB-lite",
    "",
]


uniprot_sheet_example = """
ID   B7FXA2_PHATC            Unreviewed;       443 AA.
AC   B7FXA2;
DT   10-FEB-2009, integrated into UniProtKB/TrEMBL.
DT   10-FEB-2009, sequence version 1.
DT   22-APR-2020, entry version 52.
DE   RecName: Full=Succinate--CoA ligase [ADP-forming] subunit beta, mitochondrial {ECO:0000256|HAMAP-Rule:MF_03219};
DE            EC=6.2.1.5 {ECO:0000256|HAMAP-Rule:MF_03219};
DE   AltName: Full=Succinyl-CoA synthetase beta chain {ECO:0000256|HAMAP-Rule:MF_03219};
DE            Short=SCS-beta {ECO:0000256|HAMAP-Rule:MF_03219};
GN   ORFNames=PHATRDRAFT_26921 {ECO:0000313|EMBL:EEC49126.1};
OS   Phaeodactylum tricornutum (strain CCAP 1055/1).
OC   Eukaryota; Sar; Stramenopiles; Ochrophyta; Bacillariophyta;
OC   Bacillariophyceae; Bacillariophycidae; Naviculales; Phaeodactylaceae;
OC   Phaeodactylum.
OX   NCBI_TaxID=556484 {ECO:0000313|Proteomes:UP000000759};
RN   [1] {ECO:0000313|EMBL:EEC49126.1, ECO:0000313|Proteomes:UP000000759}
RP   NUCLEOTIDE SEQUENCE [LARGE SCALE GENOMIC DNA].
RC   STRAIN=CCAP 1055/1 {ECO:0000313|EMBL:EEC49126.1,
RC   ECO:0000313|Proteomes:UP000000759};
RX   PubMed=18923393; DOI=10.1038/nature07410;
RA   Bowler C., Allen A.E., Badger J.H., Grimwood J., Jabbari K., Kuo A.,
RA   Maheswari U., Martens C., Maumus F., Otillar R.P., Rayko E., Salamov A.,
RA   Vandepoele K., Beszteri B., Gruber A., Heijde M., Katinka M., Mock T.,
RA   Valentin K., Verret F., Berges J.A., Brownlee C., Cadoret J.P.,
RA   Chiovitti A., Choi C.J., Coesel S., De Martino A., Detter J.C., Durkin C.,
RA   Falciatore A., Fournet J., Haruta M., Huysman M.J., Jenkins B.D.,
RA   Jiroutova K., Jorgensen R.E., Joubert Y., Kaplan A., Kroger N., Kroth P.G.,
RA   La Roche J., Lindquist E., Lommer M., Martin-Jezequel V., Lopez P.J.,
RA   Lucas S., Mangogna M., McGinnis K., Medlin L.K., Montsant A.,
RA   Oudot-Le Secq M.P., Napoli C., Obornik M., Parker M.S., Petit J.L.,
RA   Porcel B.M., Poulsen N., Robison M., Rychlewski L., Rynearson T.A.,
RA   Schmutz J., Shapiro H., Siaut M., Stanley M., Sussman M.R., Taylor A.R.,
RA   Vardi A., von Dassow P., Vyverman W., Willis A., Wyrwicz L.S.,
RA   Rokhsar D.S., Weissenbach J., Armbrust E.V., Green B.R., Van de Peer Y.,
RA   Grigoriev I.V.;
RT   "The Phaeodactylum genome reveals the evolutionary history of diatom
RT   genomes.";
RL   Nature 456:239-244(2008).
RN   [2] {ECO:0000313|EMBL:EEC49126.1, ECO:0000313|Proteomes:UP000000759}
RP   GENOME REANNOTATION.
RC   STRAIN=CCAP 1055/1 {ECO:0000313|EMBL:EEC49126.1,
RC   ECO:0000313|Proteomes:UP000000759};
RG   Diatom Consortium;
RA   Grigoriev I., Grimwood J., Kuo A., Otillar R.P., Salamov A., Detter J.C.,
RA   Lindquist E., Shapiro H., Lucas S., Glavina del Rio T., Pitluck S.,
RA   Rokhsar D., Bowler C.;
RL   Submitted (AUG-2008) to the EMBL/GenBank/DDBJ databases.
CC   -!- FUNCTION: Succinyl-CoA synthetase functions in the citric acid cycle
CC       (TCA), coupling the hydrolysis of succinyl-CoA to the synthesis of ATP
CC       and thus represents the only step of substrate-level phosphorylation in
CC       the TCA. The beta subunit provides nucleotide specificity of the enzyme
CC       and binds the substrate succinate, while the binding sites for coenzyme
CC       A and phosphate are found in the alpha subunit. {ECO:0000256|HAMAP-
CC       Rule:MF_03219}.
CC   -!- CATALYTIC ACTIVITY:
CC       Reaction=ATP + CoA + succinate = ADP + phosphate + succinyl-CoA;
CC         Xref=Rhea:RHEA:17661, ChEBI:CHEBI:30031, ChEBI:CHEBI:30616,
CC         ChEBI:CHEBI:43474, ChEBI:CHEBI:57287, ChEBI:CHEBI:57292,
CC         ChEBI:CHEBI:456216; EC=6.2.1.5; Evidence={ECO:0000256|HAMAP-
CC         Rule:MF_03219};
CC   -!- COFACTOR:
CC       Name=Mg(2+); Xref=ChEBI:CHEBI:18420;
CC         Evidence={ECO:0000256|HAMAP-Rule:MF_03219};
CC       Note=Binds 1 Mg(2+) ion per subunit. {ECO:0000256|HAMAP-Rule:MF_03219};
CC   -!- PATHWAY: Carbohydrate metabolism; tricarboxylic acid cycle; succinate
CC       from succinyl-CoA (ligase route): step 1/1. {ECO:0000256|HAMAP-
CC       Rule:MF_03219}.
CC   -!- SUBUNIT: Heterodimer of an alpha and a beta subunit.
CC       {ECO:0000256|HAMAP-Rule:MF_03219, ECO:0000256|RuleBase:RU361258}.
CC   -!- SUBCELLULAR LOCATION: Mitochondrion {ECO:0000256|HAMAP-Rule:MF_03219}.
CC   -!- SIMILARITY: Belongs to the succinate/malate CoA ligase beta subunit
CC       family. {ECO:0000256|HAMAP-Rule:MF_03219,
CC       ECO:0000256|RuleBase:RU361258}.
CC   ---------------------------------------------------------------------------
CC   Copyrighted by the UniProt Consortium, see https://www.uniprot.org/terms
CC   Distributed under the Creative Commons Attribution (CC BY 4.0) License
CC   ---------------------------------------------------------------------------
DR   EMBL; CM000609; EEC49126.1; -; Genomic_DNA.
DR   RefSeq; XP_002179303.1; XM_002179267.1.
DR   STRING; 2850.Phatr26921; -.
DR   PRIDE; B7FXA2; -.
DR   EnsemblProtists; Phatr3_J26921.t1; Phatr3_J26921.p1; Phatr3_J26921.
DR   GeneID; 7199965; -.
DR   KEGG; pti:PHATRDRAFT_26921; -.
DR   eggNOG; KOG0404; Eukaryota.
DR   HOGENOM; CLU_037430_0_0_1; -.
DR   InParanoid; B7FXA2; -.
DR   KO; K01900; -.
DR   OMA; ITACDEV; -.
DR   OrthoDB; 973871at2759; -.
DR   UniPathway; UPA00223; UER00999.
DR   Proteomes; UP000000759; Chromosome 6.
DR   GO; GO:0005739; C:mitochondrion; IEA:UniProtKB-SubCell.
DR   GO; GO:0005524; F:ATP binding; IEA:UniProtKB-UniRule.
DR   GO; GO:0000287; F:magnesium ion binding; IEA:UniProtKB-UniRule.
DR   GO; GO:0004775; F:succinate-CoA ligase (ADP-forming) activity; IEA:UniProtKB-UniRule.
DR   GO; GO:0006099; P:tricarboxylic acid cycle; IEA:UniProtKB-UniRule.
DR   Gene3D; 3.30.1490.20; -; 1.
DR   Gene3D; 3.40.50.261; -; 1.
DR   HAMAP; MF_00558; Succ_CoA_beta; 1.
DR   InterPro; IPR013650; ATP-grasp_succ-CoA_synth-type.
DR   InterPro; IPR013815; ATP_grasp_subdomain_1.
DR   InterPro; IPR005811; CoA_ligase.
DR   InterPro; IPR017866; Succ-CoA_synthase_bsu_CS.
DR   InterPro; IPR005809; Succ_CoA_synthase_bsu.
DR   InterPro; IPR016102; Succinyl-CoA_synth-like.
DR   PANTHER; PTHR11815; PTHR11815; 1.
DR   Pfam; PF08442; ATP-grasp_2; 1.
DR   Pfam; PF00549; Ligase_CoA; 1.
DR   PIRSF; PIRSF001554; SucCS_beta; 1.
DR   SUPFAM; SSF52210; SSF52210; 1.
DR   TIGRFAMs; TIGR01016; sucCoAbeta; 1.
DR   PROSITE; PS01217; SUCCINYL_COA_LIG_3; 1.
PE   3: Inferred from homology;
KW   ATP-binding {ECO:0000256|HAMAP-Rule:MF_03219};
KW   Ligase {ECO:0000256|HAMAP-Rule:MF_03219, ECO:0000256|RuleBase:RU361258,
KW   ECO:0000313|EMBL:EEC49126.1}; Magnesium {ECO:0000256|HAMAP-Rule:MF_03219};
KW   Metal-binding {ECO:0000256|HAMAP-Rule:MF_03219};
KW   Mitochondrion {ECO:0000256|HAMAP-Rule:MF_03219};
KW   Nucleotide-binding {ECO:0000256|HAMAP-Rule:MF_03219};
KW   Reference proteome {ECO:0000313|Proteomes:UP000000759};
KW   Tricarboxylic acid cycle {ECO:0000256|HAMAP-Rule:MF_03219}.
FT   DOMAIN          26..240
FT                   /note="ATP-grasp_2"
FT                   /evidence="ECO:0000259|Pfam:PF08442"
FT   DOMAIN          300..420
FT                   /note="Ligase_CoA"
FT                   /evidence="ECO:0000259|Pfam:PF00549"
FT   NP_BIND         83..85
FT                   /note="ATP"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   REGION          359..361
FT                   /note="Substrate binding; shared with subunit alpha"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   METAL           237
FT                   /note="Magnesium"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   METAL           251
FT                   /note="Magnesium"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   BINDING         76
FT                   /note="ATP"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   BINDING         143
FT                   /note="ATP"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
FT   BINDING         302
FT                   /note="Substrate; shared with subunit alpha"
FT                   /evidence="ECO:0000256|HAMAP-Rule:MF_03219"
SQ   SEQUENCE   443 AA;  47622 MW;  E34AAF6A7037E903 CRC64;
     MLSSTVARYA LRRTANPAVG AVRNLNVHEY ISMEIMKNHG IQTPECYVAS NPEEAENIFL
     NSLNRPGAPK KDIVIKAQVL SGGRGLGTFK NGFKGGVHMV TKPGQAREFA AAMLGNELVT
     KQAPNGIVCN KVLLMERMYM RREMYLSILM DRGSQGPLLV ASPRGGTSIE DVAATNPEVI
     FTQPIDITEG LTPEGAHRMA SNLGLEEGST GHDKAVILMQ NLYSMFIACD CTQVEVNPLA
     ETPEGDIVVC DAKVNFDDNA EFRQSSIFAR RDTSQEDVRE VDASKYDLNY IGLEGTIGCM
     VNGAGLAMST MDIIQLKGGS PANFLDVGGG ANETQVQKAF EILNSDSQVK AILVNIFGGI
     MRCDVIATGI INAAKEIGIN KPIVIRLQGT NVNEAKTLIE GCGFRMILAE DLEDAAEKAV
     GVAEIAAQAE KIQVGVKFEG FSL
//
"""

gff_example = """
##gff-version 3
##sequence-region B7FXA2 1 443
B7FXA2	UniProtKB	Domain	26	240	.	.	.	Note=ATP-grasp_2;Ontology_term=ECO:0000259;evidence=ECO:0000259|Pfam:PF08442	
B7FXA2	UniProtKB	Domain	300	420	.	.	.	Note=Ligase_CoA;Ontology_term=ECO:0000259;evidence=ECO:0000259|Pfam:PF00549	
B7FXA2	UniProtKB	Nucleotide binding	83	85	.	.	.	Note=ATP;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Region	359	361	.	.	.	Note=Substrate binding%3B shared with subunit alpha;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Metal binding	237	237	.	.	.	Note=Magnesium;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Metal binding	251	251	.	.	.	Note=Magnesium;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Binding site	76	76	.	.	.	Note=ATP;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Binding site	143	143	.	.	.	Note=ATP;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
B7FXA2	UniProtKB	Binding site	302	302	.	.	.	Note=Substrate%3B shared with subunit alpha;Ontology_term=ECO:0000256;evidence=ECO:0000256|HAMAP-Rule:MF_03219	
"""

gff_example_2 = """
##gff-version 3
##sequence-region B7GCG7 1 133
B7GCG7	UniProtKB	Chain	1	133	.	.	.	ID=PRO_0000446441;Note=Cytochrome b5	
B7GCG7	UniProtKB	Transmembrane	108	128	.	.	.	Note=Helical;Ontology_term=ECO:0000255;evidence=ECO:0000255	
B7GCG7	UniProtKB	Domain	4	86	.	.	.	Note=Cytochrome b5 heme-binding;Ontology_term=ECO:0000255;evidence=ECO:0000255|PROSITE-ProRule:PRU00279	
B7GCG7	UniProtKB	Metal binding	45	45	.	.	.	Note=Iron (heme axial ligand);Ontology_term=ECO:0000255;evidence=ECO:0000255|PROSITE-ProRule:PRU00279	
B7GCG7	UniProtKB	Metal binding	69	69	.	.	.	Note=Iron (heme axial ligand);Ontology_term=ECO:0000255;evidence=ECO:0000255|PROSITE-ProRule:PRU00279	
"""
