import pytest
import pandas as pd
from copy import deepcopy
from app.db import models
from app.load_db.gprofiler.kegg import RetrieveKegg
from app.load_db.ensembl.gene import insert_genes_in_db
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from tests.test_data.ensembl_data import genome_example, gene_example
from tests.test_data.gprofiler_data import kegg_info_example

SPECIES = "Phaeodactylum tricornutum"


def test_get_gprofiler_species_name():
    rk = RetrieveKegg("session")
    assert "ptricornutum" == rk.get_gprofiler_species_name(SPECIES)

    with pytest.raises(KeyError):
        rk.get_gprofiler_species_name("A species")


def test_get_unique_kegg_not_in_db(session):
    rk = RetrieveKegg(session)
    # Input for get_kegg_not_in_db
    kegg_relevant_info = [
        {"accession": "ko00650", "description": "a description", "gene": "Phatr3_766"},
        {
            "accession": "ko03050",
            "description": "another description",
            "gene": "Phatr3_432",
        },
        {
            "accession": "ko03050",
            "description": "another description",
            "gene": "Phatr3_432",
        },
    ]
    # Insert one of the kegg
    kegg_object_to_insert = models.Kegg(
        accession="ko00650", description="a description"
    )
    session.add(kegg_object_to_insert)
    expected_unique_kegg_not_in_db = [
        {
            "accession": "ko03050",
            "description": "another description",
            "gene": "Phatr3_432",
        },
    ]
    # Test function
    kegg_not_in_db = rk.get_unique_kegg_not_in_db(kegg_relevant_info)
    assert expected_unique_kegg_not_in_db == kegg_not_in_db


def test_create_and_insert_kegg_object_in_db(session):
    rk = RetrieveKegg(session)
    # Input for create_and_insert_kegg_object_in_db
    kegg_relevant_info = [
        {"accession": "ko00650", "description": "a description", "gene": "Phatr3_766"},
        {
            "accession": "ko03050",
            "description": "another description",
            "gene": "Phatr3_432",
        },
    ]
    # Test function
    rk.create_and_insert_kegg_object_in_db(kegg_relevant_info)
    # Retrieve inserted Kegg
    kegg_in_db = session.query(models.Kegg).all()
    assert len(kegg_in_db) == 2
    assert kegg_in_db[0].accession == kegg_relevant_info[0]["accession"]
    assert kegg_in_db[1].accession == kegg_relevant_info[1]["accession"]
    assert kegg_in_db[0].description == kegg_relevant_info[0]["description"]
    assert kegg_in_db[1].description == kegg_relevant_info[1]["description"]


def test_extract_kegg_relevant_info():
    rk = RetrieveKegg("session")
    # Extract_kegg_relevant_info take an Dataframe in input
    kegg_info = pd.DataFrame(kegg_info_example)
    # Test Function
    kegg_relevant_info = rk.extract_kegg_relevant_info(kegg_info)
    expected_kegg_relevant_info = [
        {
            "accession": "ko03450",
            "description": "Non-homologous end-joining",
            "gene_name": "Phatr3_J50218",
        },
        {
            "accession": "ko00500",
            "description": "Starch and sucrose metabolism",
            "gene_name": "Phatr3_EG02210",
        },
        {
            "accession": "ko00650",
            "description": "Butanoate metabolism",
            "gene_name": "Phatr3_J55192",
        },
    ]
    assert kegg_relevant_info == expected_kegg_relevant_info


def test_get_all_genes_name(session):
    rk = RetrieveKegg(session)
    # Setup gene and genome
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )
    # Get gene_name inserted
    expected_gene_name = [gene_example["gene_id"]]
    # Test function
    gene_name = rk.get_all_genes_name(SPECIES)
    assert gene_name == expected_gene_name


def test_get_gene_from_name(session):
    rk = RetrieveKegg(session)
    # Setup a gene in db
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )

    expected_gene = session.query(models.Gene).first()
    # Test function
    gene = rk.get_gene_from_name("Phatr3_J50624")
    assert gene == expected_gene, "Should have return the Gene Phatr3_J50624"

    gene = rk.get_gene_from_name("OTHER_GENE")
    assert gene == None, "Shouldn't have find a Gene"


def test_get_kegg_from_accession(session):
    rk = RetrieveKegg(session)
    # Add a kegg in db
    Kegg = models.Kegg(accession="ko00650", description="a description")
    session.add(Kegg)
    # Retrieve it to have the Kegg.id set
    expected_kegg = session.query(models.Kegg).first()
    # Test find correctly
    kegg = rk.get_kegg_from_accession("ko00650")
    assert kegg == expected_kegg, "Should have return the Kegg ko00650"
    # Test doesn't return other kegg than wanted
    kegg = rk.get_kegg_from_accession("ko00051")
    assert kegg == None, "Shouldn't have find a Kegg"


def test_create_kegg_link_with_gene(session):
    rk = RetrieveKegg(session)
    # Add a gene in db
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )
    # Add a Kegg in db
    Kegg_object = models.Kegg(accession="ko00650", description="a description")
    session.add(Kegg_object)
    # Create input for create_kegg_link_with_gene
    kegg_relevant_info = [
        {
            "accession": "ko00650",
            "description": "a description",
            "gene_name": "Phatr3_J50624",
        }
    ]
    # Test function
    rk.create_kegg_link_with_gene(kegg_relevant_info)
    # Retrieve the Gene and kegg inserted
    gene = session.query(models.Gene).first()
    kegg = session.query(models.Kegg).first()
    assert gene.kegg[0] == kegg, "Should have a link with kegg"


def test_retrieve_kegg_run(session, monkeypatch):
    rk = RetrieveKegg(session)
    # Mock API call
    def mock_retrieve_kegg_info(self, genes_name, species):
        return pd.DataFrame(kegg_info_example)

    monkeypatch.setattr(RetrieveKegg, "retrieve_kegg_info", mock_retrieve_kegg_info)
    # Setup genome
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    # Setup 3 gene to correspond to the 3 kegg in kegg_info_example
    gene_example_1 = deepcopy(gene_example)
    gene_example_2 = deepcopy(gene_example)
    gene_example_3 = deepcopy(gene_example)
    gene_example_1["id"] = "Phatr3_J50218"
    gene_example_2["id"] = "Phatr3_EG02210"
    gene_example_3["id"] = "Phatr3_J55192"
    insert_genes_in_db(
        session,
        [gene_example_1, gene_example_2, gene_example_3],
        [
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1},
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1},
            {"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1},
        ],
    )

    # Test function
    rk.run(SPECIES)
    # Test that the kegg have been inserted and linked correctly
    genes = session.query(models.Gene).all()
    keggs = session.query(models.Kegg).all()
    assert genes[0].kegg[0] == keggs[0], "Kegg should have been inserted and linked"
    assert genes[1].kegg[0] == keggs[1], "Kegg should have been inserted and linked"
    assert genes[2].kegg[0] == keggs[2], "Kegg should have been inserted and linked"
