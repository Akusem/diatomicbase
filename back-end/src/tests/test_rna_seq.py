import os
import pytest
from copy import deepcopy
from datetime import datetime
from sqlalchemy.orm import Session
from tests.test_data.rna_seq_data import expected_fasta
from tests.test_data.ensembl_data import (
    genome_example,
    gene_example,
    transcript_example,
)
from app.db.models import GenomeTopLevelRegion, Transcript, SraExperiment
from app.load_db.ensembl.gene import insert_genes_in_db
from app.load_db.ensembl.transcript import insert_transcripts_in_db
from app.load_db.ensembl.dna import EnsemblRetrieveGenomeSequence
from app.load_db.ensembl.genome_info import EnsemblRetrieveGenomeInfo
from app.pipeline.RNA_seq import generate_genome_fasta
from app.pipeline.RNA_seq.generate_genome_fasta import (
    create_fasta_header,
    create_genome_fasta,
)
from app.pipeline.RNA_seq.fasta import (
    create_transcript_fasta,
    create_transcript_fasta_header,
    write_seq,
)
from app.pipeline.RNA_seq import main
from app.pipeline.RNA_seq.main import RNASeqManager

SPECIES = "Phaeodactylum tricornutum"


def test_create_fasta_header(session: Session, monkeypatch):
    # Mock ensembl API call
    def mock_fetch_sequence(self, species, region):
        return "ATCTTAGTCGATCGATCAGT"

    monkeypatch.setattr(
        EnsemblRetrieveGenomeSequence,
        "fetch_sequence_from_region",
        mock_fetch_sequence,
    )

    # Setup Genome, GenomeTopLevelRegion, SeqRegion and Dna in db
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    EnsemblRetrieveGenomeSequence(session).run(SPECIES)
    # Get list top_level_region. These are use by create_fasta_header as input
    top_level_regions = session.query(GenomeTopLevelRegion).all()
    assembly_name = "ASM15095v2"
    # Select chrom 33
    chrom_33 = [chrom for chrom in top_level_regions if chrom.seq_region.name == "33"][
        0
    ]
    # Expected header from fasta dl on ensembl ftp website
    expected_header_for_chrom_33 = (
        ">33 dna:chromosome chromosome:ASM15095v2:33:1:87967:1 REF"
    )
    # Test function
    header_chrom_33 = create_fasta_header(assembly_name, chrom_33)
    assert header_chrom_33 == expected_header_for_chrom_33

    # Setup test for chloroplast header
    chloroplat = [
        chrom for chrom in top_level_regions if chrom.seq_region.name == "chloroplast"
    ][0]
    expected_header_for_chloroplast = (
        ">chloroplast dna:chromosome chromosome:ASM15095v2:chloroplast:1:117369:1 REF"
    )
    header_chloroplast = create_fasta_header(assembly_name, chloroplat)
    assert header_chloroplast == expected_header_for_chloroplast

    # Same test with a supercontig
    supercontig_assembly_name = "ASM15095v2_bd"
    expected_header_for_supercontig = (
        ">bd_32x35 dna:supercontig supercontig:ASM15095v2_bd:bd_32x35:1:293345:1 REF"
    )
    supercontig = [
        chrom for chrom in top_level_regions if chrom.seq_region.name == "bd_32x35"
    ][0]
    header_supercontig = create_fasta_header(supercontig_assembly_name, supercontig)
    assert header_supercontig == expected_header_for_supercontig


def test_create_genome_fasta(session, tmp_path, monkeypatch):
    # Mock ensembl API call
    def mock_fetch_sequence(self, species, region):
        return "ATCTTAGTCGATCGATCAGT"

    monkeypatch.setattr(
        EnsemblRetrieveGenomeSequence,
        "fetch_sequence_from_region",
        mock_fetch_sequence,
    )

    # Setup Genome, GenomeTopLevelRegion, SeqRegion and Dna in db
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    EnsemblRetrieveGenomeSequence(session).run(SPECIES)
    output_path = tmp_path / "tmp.test"
    create_genome_fasta(session, SPECIES, str(output_path.resolve()))

    genome_fasta = "\n" + output_path.open("r").read()
    assert genome_fasta == expected_fasta


def test_create_transcript_fasta_header(session):
    # Setup info needed in db (assembly, a gene, and a transcript)
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    gene_example["id"] = "Phatr3_J50640"
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )
    transcript_example_2 = deepcopy(transcript_example)
    transcript_example_2["strand"] = -1
    insert_transcripts_in_db(
        session,
        [transcript_example, transcript_example_2],
        [
            {"gene_id": 1, "assembly_id": 1, "region_id": 3},
            {"gene_id": 1, "assembly_id": 1, "region_id": 3},
        ],
    )
    # Retrieve the transcript
    transcript = session.query(Transcript).all()
    # Test function
    header_1 = create_transcript_fasta_header(transcript[0])
    header_2 = create_transcript_fasta_header(transcript[1])

    expected_header_1 = ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=+"
    expected_header_2 = ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=-"

    assert header_1 == expected_header_1
    assert header_2 == expected_header_2


def test_write_seq(tmp_path):
    output_path = tmp_path / "tmp.txt"
    # Setup argument for write seq
    output = output_path.open("w")
    header_1 = ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=+"
    header_2 = ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=-"
    seq_1 = "ATCGCTAGACTGCATCACN"
    seq_2 = "TCGTCGATCGAATATCGTCA"
    # Test function
    write_seq(output, header_1, seq_1)
    write_seq(output, header_2, seq_2)
    output.close()
    # Retrieve write seq output
    output_content = output_path.open("r").readlines()
    # Test it
    assert output_content[0] == header_1 + "\n"
    assert output_content[1] == seq_1 + "\n"
    assert output_content[2] == header_2 + "\n"
    assert output_content[3] == seq_2 + "\n"


def test_generate_transcript_fasta(session, monkeypatch, tmp_path):
    # mock sequence used
    def mock_get_sequence(self, session):
        return "ATCGCTAGACTGCATCACN"

    monkeypatch.setattr(Transcript, "get_sequence", mock_get_sequence)

    # Setup info needed in db (assembly, a gene, and a transcript)
    EnsemblRetrieveGenomeInfo(session).load_genome_info_in_db(genome_example, SPECIES)
    gene_example["id"] = "Phatr3_J50640"
    insert_genes_in_db(
        session,
        [gene_example],
        [{"assembly_id": 1, "eco": "", "embl": "", "uniprotkb": "", "region_id": 1}],
    )
    transcript_example_2 = deepcopy(transcript_example)
    transcript_example_2["strand"] = -1
    insert_transcripts_in_db(
        session,
        [transcript_example, transcript_example_2],
        [
            {"gene_id": 1, "assembly_id": 1, "region_id": 3},
            {"gene_id": 1, "assembly_id": 1, "region_id": 3},
        ],
    )
    output_path = tmp_path / "tmp.txt"
    # Test function
    create_transcript_fasta(session, SPECIES, output_path.resolve())

    expected_file_content = (
        ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=+\n"
        "ATCGCTAGACTGCATCACN\n"
        ">Phatr3_J50640.t1 gene=Phatr3_J50640 seq=32 strand=-\n"
        "ATCGCTAGACTGCATCACN\n"
    )
    # Retrieve output for test
    file_content = output_path.open("r").read()
    assert file_content == expected_file_content


def test_create_and_get_run_work_dir(session, tmp_path):
    rna = RNASeqManager(session, WORK_DIR=str(tmp_path.resolve()))
    # Test function
    work_dir = rna.get_or_create_run_work_dir("SRR10708548")
    # Test creation
    expected_work_dir = tmp_path / "SRR10708548"
    folder_created = expected_work_dir.exists()

    assert work_dir == str(expected_work_dir.resolve()), "The Path doesn't correspond"
    assert folder_created is True, "Folder hasn't be created"


def test_get_all_sra_experiment_accession_and_layout(session):
    sra_experiments = [
        SraExperiment(
            run="SRR11570918",
            release_date=datetime.strptime("2020-04-20 08:09:02", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:05:31", "%Y-%m-%d %H:%M:%S"),
            average_length="302",
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra0/SRR/011299/SRR11570918",
            library_name="D4-RNA_1",
            library_selection="PCR",
            library_layout="PAIRED",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project="625589",
            sample_name="D4-RNA_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
        SraExperiment(
            run="SRR11570919",
            release_date=datetime.strptime("2020-04-20 08:05:44", "%Y-%m-%d %H:%M:%S"),
            load_date=datetime.strptime("2020-04-20 08:02:37", "%Y-%m-%d %H:%M:%S"),
            average_length="302",
            dl_path="https://sra-download.ncbi.nlm.nih.gov/"
            "traces/sra80/SRR/011299/SRR11570919",
            library_name="D4_1",
            library_selection="PCR",
            library_layout="SINGLE",
            platform="ILLUMINA",
            model="HiSeq X Ten",
            project="625589",
            sample_name="D4_1",
            center_name="KIST GANGNEUNG INSTITUTE OF NATURAL PRODUCTS",
            species_name="Phaeodactylum tricornutum",
            study_id=1,
        ),
    ]
    session.add_all(sra_experiments)

    sra_acc_layout = RNASeqManager(session).get_all_sra_experiment_accession_and_layout(
        "Phaeodactylum tricornutum"
    )
    assert len(sra_acc_layout) == 2
    assert sra_acc_layout[0][0] == "SRR11570918"
    assert sra_acc_layout[1][0] == "SRR11570919"
    assert sra_acc_layout[0][1] == "PAIRED"
    assert sra_acc_layout[1][1] == "SINGLE"


def test_get_species_gtf_path(session, tmp_path):
    # Setup empty gtf_files
    rna = RNASeqManager(session, DOWNLOAD_DIR=tmp_path.resolve(), GTF_FILES=[])
    # Test error raise for no configuration
    with pytest.raises(
        FileNotFoundError, match="GTF file for .* not define in constant GTF_FILES"
    ):
        rna.get_species_gtf_path(SPECIES)
    # Test error raises when the specified file dos not exist
    test_gtf_path = tmp_path / "genomes" / "test_get_species_gtf_path.tmp"
    rna.GTF_FILES = [{"species": SPECIES, "filename": "test_get_species_gtf_path.tmp"}]
    with pytest.raises(FileNotFoundError, match="GTF file .* is not found"):
        rna.get_species_gtf_path(SPECIES)
    # Create file
    file = open(test_gtf_path.resolve(), "w")
    file.write("test")
    file.close()
    # test_gtf_path.flush()
    # Test normal function
    print(rna.GTF_FILES)
    gtf_path = rna.get_species_gtf_path(SPECIES)
    print("gtf_path: ", gtf_path)

    assert gtf_path == str(test_gtf_path.resolve())


def test_get_or_create_genome_fasta(session, tmp_path, monkeypatch):
    def mock_create_genome_fasta(session, species_name, path):
        open(path, "w").write("test")
        return None

    monkeypatch.setattr(main, "create_genome_fasta", mock_create_genome_fasta)

    rna = RNASeqManager(session, DOWNLOAD_DIR=str(tmp_path.resolve()))
    # Get expected path
    fasta_name = SPECIES.replace(" ", "_") + "_dna.fa"
    expected_path = tmp_path / "genomes" / fasta_name
    # Test when the species fasta was not present
    fasta_path = rna.get_or_create_genome_fasta(SPECIES)
    fasta_was_created = os.path.exists(fasta_path)
    assert fasta_path == str(expected_path.resolve())
    assert fasta_was_created is True
    # Test with the fasta already present
    fasta_path = rna.get_or_create_genome_fasta(SPECIES)
    assert fasta_path == str(expected_path.resolve())


def test_select_genome_fasta_by_species(session):
    rna = RNASeqManager(session)
    rna.genomes_fasta = [{"species": SPECIES, "filename": "test"}]
    # Test when no genome fasta for this species
    should_be_none = rna.select_genome_fasta_by_species("homo sapiens")
    assert should_be_none is None
    # Test when genome fasta for this species is present
    genome_fasta = rna.select_genome_fasta_by_species(SPECIES)
    assert genome_fasta == "test"


def test_create_path_to_download_genomes(session, tmp_path):
    # Setup class and tmp dir for test
    rna = RNASeqManager(session, DOWNLOAD_DIR=str(tmp_path.resolve()))

    file_path = rna.create_path_to_download_genomes("test1")
    genomes_folder = tmp_path / "genomes"
    # Test it created the genomes folder
    assert genomes_folder.exists() == True
    expected_file_path = genomes_folder / "test1"
    assert file_path == str(expected_file_path.resolve())


def test_is_paired(session, tmp_path):
    rna = RNASeqManager(session, DOWNLOAD_DIR=str(tmp_path.resolve()))
    # Create file
    fastq_1 = tmp_path / "SRR11570919_1.fastq"
    fastq_2 = tmp_path / "SRR11570919_2.fastq"
    fastq_single = tmp_path / "ERR18343598.fastq"
    fastq_1.open("w").write("t")
    fastq_2.open("w").write("t")
    fastq_single.open("w").write("t")

    # Test if find paired and single. And if raise FileNotFound when needed
    paired = rna.is_paired("SRR11570919")
    single = rna.is_paired("ERR18343598")
    assert paired == True
    assert single == False
    with pytest.raises(FileNotFoundError):
        rna.is_paired("SPR826842193")


def test_fastq_present(session, tmp_path):
    # Setup, create only one file of paired fastq
    rna = RNASeqManager(session, DOWNLOAD_DIR=str(tmp_path.resolve()))
    fastq_1 = tmp_path / "SRR11570919_1.fastq"
    fastq_1.open("w").write("t")

    one_file_missing = rna.fastq_present("SRR11570919", paired=True)
    assert one_file_missing == False

    # Add missing fastq
    fastq_2 = tmp_path / "SRR11570919_2.fastq"
    fastq_2.open("w").write("t")
    file_present = rna.fastq_present("SRR11570919", paired=True)
    assert file_present == True

    # Test single fastq
    fastq_single = tmp_path / "ERR18343598.fastq"
    fastq_single.open("w").write("t")
    file_present = rna.fastq_present("ERR18343598", paired=False)
    assert file_present == True

    # Test for file not present for SINGLE and PAIRED
    file_not_present = rna.fastq_present("SOR18334324", paired=False)
    assert file_not_present == False
    file_not_present = rna.fastq_present("SOR18334324", paired=True)
    assert file_not_present == False


def test_fastq_compressed(session, tmp_path):
    rna = RNASeqManager(session, DOWNLOAD_DIR=str(tmp_path.resolve()))
    fastq_gz = tmp_path / "SRR11570919_1.fastq.gz"
    fastq_not_gz = tmp_path / "ERR18343598.fastq"
    fastq_gz.open("w").write("t")
    fastq_not_gz.open("w").write("t")

    compressed = rna.fastq_compressed("SRR11570919")
    not_compressed = rna.fastq_compressed("ERR18343598")
    assert compressed == True
    assert not_compressed == False


def test_delete_fastq(session, tmp_path):
    rna = RNASeqManager(session, DOWNLOAD_DIR=tmp_path.resolve())
    # Create files to deletes
    fastq_paired1_gz = tmp_path / "SRR11570919_1.fastq.gz"
    fastq_paired2_gz = tmp_path / "SRR11570919_2.fastq.gz"
    fastq_single_not_gz = tmp_path / "ERR18343598.fastq"
    fastq_paired1_gz.open("w").write("t")
    fastq_paired2_gz.open("w").write("t")
    fastq_single_not_gz.open("w").write("t")
    # Test delete SRR11570919
    rna.delete_fastq("SRR11570919")
    # Test it deleted only wanted fastq
    list_dir = os.listdir(tmp_path.resolve())
    is_fastq_paired1_deleted = "SRR11570919_1.fastq.gz" not in list_dir
    is_fastq_paired2_deleted = "SRR11570919_2.fastq.gz" not in list_dir
    is_fastq_single_deleted = "ERR18343598.fastq" not in list_dir
    assert is_fastq_paired1_deleted == True
    assert is_fastq_paired2_deleted == True
    assert is_fastq_single_deleted == False, "shouldn't have deleted fastq_single"

    rna.delete_fastq("ERR18343598")
    list_dir = os.listdir(tmp_path.resolve())
    assert len(list_dir) == 0, "Should have deleted last fastq"


def test_run_finished(session, tmp_path):
    rna = RNASeqManager(session, ALIGNER="hisat2", WORK_DIR=tmp_path.resolve())
    test_accession = "SRR11570919"
    # Test without run done
    is_finished = rna.run_finished(test_accession)
    assert is_finished == False
    # Test with a run finished
    run_path = tmp_path / test_accession / "results"
    # Create all folders and sub_folder
    if not run_path.exists():
        run_path.mkdir()
    list_sub_dir = [
        "dupradar",
        "fastqc",
        "featureCounts",
        "markDuplicates",
        "MultiQC",
        "pipeline_info",
        "preseq",
        "qualimap",
        "salmon",
        "rseqc",
        "stringtieFPKM",
        "trim_galore",
        "HISAT2",
    ]
    for sub_dir in list_sub_dir:
        sub_dir_path = run_path / sub_dir
        sub_dir_path.mkdir()
    # Test function
    is_run_finished = rna.run_finished(test_accession)
    assert is_run_finished == True
    # Test with STAR aligner
    rna = RNASeqManager(session, ALIGNER="STAR", WORK_DIR=tmp_path.resolve())
    test_accession = "SRR11570915"
    # Create all folders and sub_folder
    run_path = tmp_path / test_accession
    if not run_path.exists():
        run_path.mkdir()
    results_path = tmp_path / test_accession / "results"
    if not results_path.exists():
        results_path.mkdir()
    list_sub_dir = [
        "dupradar",
        "fastqc",
        "featureCounts",
        "markDuplicates",
        "MultiQC",
        "pipeline_info",
        "preseq",
        "qualimap",
        "salmon",
        "rseqc",
        "stringtieFPKM",
        "trim_galore",
        "STAR",
    ]
    for sub_dir in list_sub_dir:
        sub_dir_path = results_path / sub_dir
        sub_dir_path.mkdir()
    # Test function
    is_run_STAR_finished = rna.run_finished(test_accession)
    assert is_run_STAR_finished == True


def test_get_next_experiment(session, tmp_path):
    rna = RNASeqManager(session, WORK_DIR=tmp_path.resolve())
    experiments = [("SRR11570919", "PAIRED"), ("SRR11570918", "PAIRED")]
    exp = rna.get_next_experiment(experiments, 0)
    assert exp == ("SRR11570918", "PAIRED")


# def test_launch_download_next_experiment_fastq(session, tmp_path, monkeypatch):
#     rna = RNASeqManager(
#         session, WORK_DIR=tmp_path.resolve(), DOWNLOAD_DIR=tmp_path.resolve()
#     )
#     # Mock the download of file from SRA
#     def mock_download_fastq(self, sra_accession, paired, output_path):
#         if paired:
#             filename1 = os.path.join(output_path, sra_accession + "_1.fastq")
#             filename2 = os.path.join(output_path, sra_accession + "_2.fastq")
#             open(filename1, "w").write("t")
#             open(filename2, "w").write("t")
#         else:
#             filename = os.path.join(output_path, sra_accession + ".fastq")
#             open(filename, "w").write("t")

#     monkeypatch.setattr(RNASeqManager, "download_fastq", mock_download_fastq)
#     experiments = [("SRR11570919", "PAIRED"), ("SRR11570918", "PAIRED")]
#     rna.launch_download_next_experiment_fastq(experiments, 0)
#     list_dir = os.listdir(str(tmp_path.resolve()))
#     assert (
#         "SRR11570918_1.fastq.gz" in list_dir
#     ), "should have dl next experiment, SRR11570918"
#     assert (
#         "SRR11570918_2.fastq.gz" in list_dir
#     ), "should have dl next experiment, SRR11570918"


def test_download_fastq_if_not_present(session, tmp_path, monkeypatch):
    rna = RNASeqManager(session, DOWNLOAD_DIR=tmp_path.resolve())
    # Mock the download of file from SRA
    def mock_download_fastq(self, sra_accession, paired, output_path):
        if paired:
            filename1 = os.path.join(output_path, sra_accession + "_1.fastq")
            filename2 = os.path.join(output_path, sra_accession + "_2.fastq")
            open(filename1, "w").write("t")
            open(filename2, "w").write("t")
        else:
            filename = os.path.join(output_path, sra_accession + ".fastq")
            open(filename, "w").write("t")

    monkeypatch.setattr(RNASeqManager, "download_fastq", mock_download_fastq)

    # Test with paired fastq not dl
    not_downloaded_paired_fastq = {"sra_accession": "SRR11570919", "paired": True}
    rna.download_fastq_if_not_present(**not_downloaded_paired_fastq)
    list_dir = os.listdir(tmp_path.resolve())
    assert (
        "SRR11570919_1.fastq.gz" in list_dir
    ), "Should have dl the paired Fastq and compress it"
    assert (
        "SRR11570919_2.fastq.gz" in list_dir
    ), "Should have dl the paired Fastq and compress it"

    # Test with single fastq not dl
    not_downloaded_single_fastq = {"sra_accession": "SRR11570918", "paired": False}
    rna.download_fastq_if_not_present(**not_downloaded_single_fastq)
    list_dir = os.listdir(tmp_path.resolve())
    assert (
        "SRR11570918.fastq.gz" in list_dir
    ), "Should have dl the single Fastq and compress it"

    # Test with paired fastq present but not compressed
    open(str(tmp_path.resolve()) + "/SRR11570917_1.fastq", "w").write("t")
    open(str(tmp_path.resolve()) + "/SRR11570917_2.fastq", "w").write("t")
    downloaded_paired_fastq = {"sra_accession": "SRR11570917", "paired": True}
    rna.download_fastq_if_not_present(**downloaded_paired_fastq)
    list_dir = os.listdir(tmp_path.resolve())
    assert (
        "SRR11570917_1.fastq.gz" in list_dir
    ), "Should have compressed the paired Fastq"
    assert (
        "SRR11570917_2.fastq.gz" in list_dir
    ), "Should have compressed the paired Fastq"

    # Test with single fastq present but not compressed
    open(str(tmp_path.resolve()) + "/SRR11570916.fastq", "w").write("t")
    downloaded_single_fastq = {"sra_accession": "SRR11570916", "paired": False}
    rna.download_fastq_if_not_present(**downloaded_single_fastq)
    list_dir = os.listdir(tmp_path.resolve())
    assert "SRR11570916.fastq.gz" in list_dir, "Should have compressed the single Fastq"

    # Test it doesn't do anything if already dl and compressed on paired SRA
    open(str(tmp_path.resolve()) + "/SRR11570915_1.fastq.gz", "w").write("t")
    open(str(tmp_path.resolve()) + "/SRR11570915_2.fastq.gz", "w").write("t")
    downloaded_paired_fastq = {"sra_accession": "SRR11570915", "paired": True}
    rna.download_fastq_if_not_present(**downloaded_paired_fastq)
    list_dir = os.listdir(tmp_path.resolve())
    assert (
        "SRR11570915_1.fastq.gz" in list_dir
    ), "Should haven't touch to the paired Fastq already present and compressed"
    assert (
        "SRR11570915_2.fastq.gz" in list_dir
    ), "Should haven't touch to the paired Fastq already present and compressed"


def test_clean_run(session, tmp_path):
    rna = RNASeqManager(session, ALIGNER="hisat2", WORK_DIR=tmp_path.resolve())
    srr1 = "SRR8647971"
    run_path = tmp_path / srr1
    work_path = run_path / "work"
    results_path = run_path / "results"
    hisat_path = results_path / "HISAT2"
    markDulicates_path = results_path / "markDuplicates"
    hisat_bam_path = hisat_path / f"{srr1}.bam"
    markDulicates_bam_path = markDulicates_path / f"{srr1}.sorted.markDups.bam"
    markDulicates_bai_path = markDulicates_path / f"{srr1}.sorted.markDups.bam.bai"
    markDulicates_bam_1_path = markDulicates_path / f"{srr1}_1.sorted.markDups.bam"
    markDulicates_bai_1_path = markDulicates_path / f"{srr1}_1.sorted.markDups.bam.bai"
    markDulicates_random_file_path = markDulicates_path / "random.txt"
    run_path.mkdir()
    work_path.mkdir()
    results_path.mkdir()
    hisat_path.mkdir()
    markDulicates_path.mkdir()
    hisat_bam_path.open("w").write("t")
    markDulicates_bam_path.open("w").write("t")
    markDulicates_bai_path.open("w").write("t")
    markDulicates_bam_1_path.open("w").write("t")
    markDulicates_bai_1_path.open("w").write("t")
    markDulicates_random_file_path.open("w").write("t")

    # Test function
    rna.clean_run(srr1)

    # Assert work folder with all nextflow temp file is deleted
    run_list_dir = os.listdir(run_path)
    assert run_list_dir == ["results"], "Should have deleted work dir to save storage"

    # Assert non sorted bam in hisat folder is deleted
    hisat_list_dir = os.listdir(hisat_path)
    assert hisat_list_dir == [], "Should have deleted hisat bam"

    # Assert markDuplicates bam files are deleted
    markDuplicates_list_dir = os.listdir(markDulicates_path)
    assert markDuplicates_list_dir == [
        "random.txt"
    ], "Should have deleted markDuplicates bam"


def test_log_number_of_sra_to_be_processed(session, tmp_path, caplog):
    sras_not_done = ["SRR477791", "SRR9567471", "SRR6339567"]
    sras_finished = ["ERR8697153"]
    sra_experiments = [
        ("SRR477791", True),
        ("SRR9567471", True),
        ("SRR6339567", False),
        ("ERR8697153", True),
    ]
    # Setup RnaSeqManager
    rna = RNASeqManager(session, ALIGNER="hisat2", WORK_DIR=tmp_path.resolve())
    # Create a run
    make_sra_run_finished(tmp_path, sras_finished)
    # Test function
    rna.log_number_of_sra_to_be_processed(sra_experiments)
    # Assert good number of SRA to processed is display
    assert (
        "3 SRA will be processed by the RnaSeq Pipeline" in caplog.text
    ), "should display 3"
    # Assert correct display of each individual sra to processed
    assert "\n".join(sras_not_done) in caplog.text


def make_sra_run_finished(tmp_path, list_sra):
    for sra in list_sra:
        run_path = tmp_path / sra
        results_path = run_path / "results"
        # Create all folders and sub_folder
        if not run_path.exists():
            run_path.mkdir()
            results_path.mkdir()
        list_sub_dir = [
            "dupradar",
            "fastqc",
            "featureCounts",
            "markDuplicates",
            "MultiQC",
            "pipeline_info",
            "preseq",
            "qualimap",
            "salmon",
            "rseqc",
            "stringtieFPKM",
            "trim_galore",
            "HISAT2",
        ]
        for sub_dir in list_sub_dir:
            sub_dir_path = results_path / sub_dir
            sub_dir_path.mkdir()
