import argparse

parser = argparse.ArgumentParser(
    description="Convert Gene annotation GFF to add gene page hyperlink"
)
parser.add_argument("-i", "--input", required=True)
parser.add_argument("-o", "--output", required=True)
args = parser.parse_args()

gff = open(args.input, "r")
output = open(args.output, "w")
for line in gff:
    # Pass comment
    if line.startswith("#"):
        output.write(line)
        continue
    fields = line.split("\t")
    # Make modification only on genes
    if fields[2] != "gene":
        output.write(line)
        continue
    attrs = fields[8].split(";")
    # Look only for gene_id attribute
    for attr in attrs:
        if not attr.startswith("gene_id"):
            continue
        # Get gene_id and create a version with href to gene page
        gene_id = attr.replace("gene_id=", "").replace("\n", "")
        href_gene_id = f'<a href%3D"/genes/{gene_id}">{gene_id}</a>'
        # In line replace old gene_id with the one with href
        line = line.replace(attr, f"gene_id={href_gene_id}")
        print(f"Adding href for {gene_id}")
        output.write(line)

