import os
import argparse
import gffutils

parser = argparse.ArgumentParser(
    description="Extract Gene symbol from an Ensembl annotation GFF and create a tsv to add theses symbol in DOB"
)
parser.add_argument("-i", "--input", required=True)
parser.add_argument("-o", "--output", required=True)
parser.add_argument(
    "-d",
    "--database",
    help="Path where the GFFUtils db will be created (but it's deleted at the end)",
    default="gff_read.db",
)
args = parser.parse_args()

# Create gffutils db, needed to use gffutils
gffutils.create_db(
    args.input.strip(),
    dbfn=args.database,
    force=True,
    keep_order=True,
    merge_strategy="merge",
    sort_attribute_values=True,
)

db = gffutils.FeatureDB(args.database, keep_order=True)
gffutils.constants.always_return_list = False
# Initialize output
output = open(args.output, "w")
output.write("gene_name\tid_type\tid\n")

genes = db.all_features(featuretype=["gene"])
for gene in genes:
    attr_key = gene.attributes.keys()
    # In Ensembl GFF symbol are present as 'Name' attribute
    if "Name" in attr_key:
        # In Ensembl GFF, ID field is noted "gene:THAPS_9999", so we remove "gene:"
        gene_name = gene.id.replace("gene:", "")
        symbol = gene["Name"]
        # Write tsv output to add alternative gene id in DOB
        output.write(f"{gene_name}\tsymbol\t{symbol}\n")

os.remove(args.database)
