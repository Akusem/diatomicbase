import os
import sys
import shutil
import argparse
from glob import glob
from csv import DictReader

sys.path.insert(1, "../")
from app import RNA_SEQ_WORK_DIR, RNA_SEQ_RUNS_GROUP_TABLE


def main(args):
    print(f"Copying runs featureCounts to {args.output_dir}:")

    reader = DictReader(open(RNA_SEQ_RUNS_GROUP_TABLE, "r"), delimiter="\t")
    runs = [row["run"] for row in reader]
    runs_not_found = []
    for i, run in enumerate(runs):
        print(f"Copy {run} featureCounts to new folder {i+1}/{len(runs)}")
        not_found = copy_featureCounts(args.output_dir, run)
        if not_found:
            runs_not_found.append(run)
        print(f"Copy {run} BigWig new folder {i+1}/{len(runs)}")
        copy_bigwig(args.output_dir, run)
        print(f"Copy {run} Fastqc report new folder {i+1}/{len(runs)}")
        copy_fastqc_report(args.output_dir, run)
    print(f"Runs not found: {runs_not_found}")


def copy_featureCounts(output_dir, run):
    run_origin_path = os.path.join(
        RNA_SEQ_WORK_DIR, run, "results/featureCounts/merged_gene_counts.txt"
    )
    run_output_path = os.path.join(output_dir, run, "results/featureCounts/")

    os.makedirs(run_output_path, exist_ok=True)
    try:
        shutil.copyfile(
            run_origin_path, os.path.join(run_output_path, "merged_gene_counts.txt")
        )
    except FileNotFoundError:
        print(f"{run} not found, passing")
        return True


def copy_bigwig(output_dir, run):
    bigwig_origin_path = os.path.join(RNA_SEQ_WORK_DIR, "BigWig", f"{run}.bw")
    bigwig_output_path = os.path.join(output_dir, "BigWig")
    os.makedirs(bigwig_output_path, exist_ok=True)
    try:
        shutil.copyfile(
            bigwig_origin_path, os.path.join(bigwig_output_path, f"{run}.bw")
        )
    except FileNotFoundError:
        print(f"{run} not found, passing")
        return True


def copy_fastqc_report(output_dir, run):
    run_origin_path = os.path.join(
        RNA_SEQ_WORK_DIR, run, f"results/fastqc/*_fastqc.html"
    )
    run_output_path = os.path.join(output_dir, run, "results/fastqc/")
    os.makedirs(run_output_path, exist_ok=True)
    try:
        for file in glob(run_origin_path):
            filename = os.path.basename(file)
            file_out = os.path.join(run_output_path, filename)
            shutil.copyfile(file, file_out)
    except FileNotFoundError:
        print(f"Fastqc of {run} not found, passing")
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Copy only the needed file from the nf-core pipeline to the shared folder between BioClust, test&prod DOB&iDEP VM"
    )
    parser.add_argument("-o", "--output-dir", required=True)
    args = parser.parse_args()
    main(args)
