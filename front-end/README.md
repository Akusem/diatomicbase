This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.


## Configuration

First, Global env variable are needed for correct configuration.

Those are defined in a `.env.local` file.
The variable needed are :

```
NEXT_PUBLIC_API_URL='url/to/api/without/slash/at/the/end'
```

Optionnal variable are:

 - `NEXT_PUBLIC_COMMENTO_URL` URL to commento to enable the comments.
      Get it in the installation guide of the commento instance.
 - `NEXT_CPUS` to force the node server to use multiples core.
 - `NEXT_LOG_FILE` path to front-end log file.
 - `NEXT_PUBLIC_UMAMI_URL` & `NEXT_PUBLIC_UMAMI_ID` URL and ID to activate UMAMI analytics.
      Get theses variable in Umami settings. URL correspond to src and ID to data-website-id.
