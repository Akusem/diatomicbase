/// <reference types="Cypress" />

describe("genomeBrowser Page", () => {
  before(() => {
    cy.visit("/genomeBrowser");
  });

  it("Can select a species locally", () => {
    cy.get("#Phaeodactylum_tricornutum_local_selector").click();
  });

  it("Load JBrowse 2", () => {
    cy.get(".MuiScopedCssBaseline-root").should("be.visible");
  });

  it("Pass initial loading", () => {
    cy.get('button[title="Open track selector"]').should("be.visible");
  });

  it("Have the modified track selector button", () => {
    cy.get('button[title="Open track selector"]').should(
      "have.class",
      "selectTracksButton"
    );
    cy.get('button[title="Open track selector"]').contains("Tracks Selector");
  });

  it("Display Phatr3 by default", () => {
    cy.contains("Phatr3 gene models (Ensembl)");
    cy.wait(300);
    cy.contains("Error: HTTP 404").should("not.exist");
  });

  it("Have phatr2", () => {
    cy.get('button[title="Open track selector"]').click();
    cy.wait(300);
    cy.contains("Phatr2 gene models (JGI)").click();
    cy.contains("Error: HTTP 404").should("not.exist");
  });
});
