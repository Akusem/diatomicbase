/// <reference types="Cypress" />

describe("Transcriptomics Page", () => {
  before(() => {
    cy.visit("/transcriptomics");
  });

  it("Contain a Help box", () => {
    cy.get("#transcriptomics-help").should("be.visible");
  });

  it("Contain a search field", () => {
    cy.get("#search-bioproject").should("be.visible");
  });
  it("Contain a BioProject Table", () => {
    cy.get(".ReactTable.-highlight").should("be.visible");
  });
  it("Contain a BioSample Box", () => {
    cy.get("#biosample").should("be.visible");
  });
  it("Contain the Subset1 Box", () => {
    cy.get("#subset1").should("be.visible");
  });
  it("Contain the Subset2 Box", () => {
    cy.get("#subset2").should("be.visible");
  });
});

describe("Transcriptomics Search Function", () => {
  before(() => {
    cy.visit("/transcriptomics");
  });

  beforeEach(() => {
    cy.get("#search-bioproject").clear();
  });

  it("Search by BioProject ID", () => {
    cy.get("#search-bioproject").type("PRJEB26173");
    cy.contains("PRJEB26173");
  });

  it("Search by Description", () => {
    cy.get("#search-bioproject").type("Transcriptomes alternative oxidase");
    cy.contains("Transcriptomes of alternative oxidase (AOX) knock-down lines");
  });
  it("Search by SRA Experiments", () => {
    cy.get("#search-bioproject").type("16mg NAs");
    cy.get("#sra-experiment").contains("CT / 4mg NAs / 16mg NAs");
  });
  it("Search by Publication", () => {
    cy.get("#search-bioproject").type("Murik 2019");
    cy.get("#publication").contains("Murik et al., 2019");
  });
});

describe("Transcriptomics calculation", () => {
  before(() => {
    cy.visit("/transcriptomics");
  });

  it("Can select a BioProject", () => {
    cy.get("input#PRJNA278661").check();
    cy.get("#biosample").contains("CT_4d_rep1");
    cy.get("#biosample").contains("CT_4d_rep2");
    cy.get("#biosample").contains("Pi_depleted_4d_rep1");
    cy.get("#biosample").contains("Pi_depleted_4d_rep2");
  });

  it("Can drag&drop in Subset 1", () => {
    // Drag and drop deplete
    cy.get("#CT_4d_rep1").drag("#subset1");
    cy.get("#CT_4d_rep2").drag("#subset1", { position: "bottom" });
    // Verify they are present in subset1
    cy.get("#subset1").contains("CT_4d_rep1");
    cy.get("#subset1").contains("CT_4d_rep2");
    // Verify they not present anymore in biosample
    cy.get("#biosample").contains("CT_4d_rep1").should("not.exist");
    cy.get("#biosample").contains("CT_4d_rep2").should("not.exist");
  });

  it("Can drag&drop in Subset 2", () => {
    // Drag and drop replete
    cy.get("#Pi_depleted_4d_rep1").drag("#subset2");
    cy.get("#Pi_depleted_4d_rep2").drag("#subset2", { position: "bottom" });
    // Verify they are present in subset2
    cy.get("#subset2").contains("Pi_depleted_4d_rep1");
    cy.get("#subset2").contains("Pi_depleted_4d_rep2");
    // Verify they not present anymore in biosample
    cy.get("#biosample").contains("Pi_depleted_4d_rep1").should("not.exist");
    cy.get("#biosample").contains("Pi_depleted_4d_rep2").should("not.exist");
  });

  it("Doesn't show loading message", () => {
    cy.get("#loading-message").should("not.exist");
  });

  it("Can launch a run", () => {
    cy.get("#submit-transcriptomics").click();
    cy.get("#loading-message").should("be.visible");
  });

  it("Is running", () => {
    cy.get("#loading-message").contains("running");
  });

  it("Is redirecting", () => {
    cy.get("#loading-message").contains(
      "Redirecting to DiatOmicBase iDEP instance...",
      { timeout: 15000 }
    );
  });

  it("Go to iDEP instance", () => {
    let idepUrl = Cypress.env("idepUrl");
    cy.url().should("contain", `${idepUrl}?usePreComp=true&fileName`, {
      timeout: 15000,
    });
    cy.get("form.well").contains("Upload expression data (CSV or text)");
  });

  it("Load automaticaly the analysis", () => {
    cy.get("form.well").contains("You can select the analysis steps", {
      timeout: 30000,
    });
    cy.get(
      "table.shiny-table.table-striped.table-hover.table-bordered.spacing-s",
      { timeout: 30000 }
    ).should("exist");
  });
});
