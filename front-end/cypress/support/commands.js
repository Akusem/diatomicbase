// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import "@4tw/cypress-drag-drop";

Cypress.Commands.add("pagePositionIs", (pagePosition) => {
  cy.get("div.-pageJump>input").invoke("val").should("eq", pagePosition);
});

Cypress.Commands.add("getNumberOfPage", () => {
  cy.waitUntil(() => cy.get(".-loading").should("not.be.visible"), {
    timeout: 10000,
  });
  cy.get("span.-totalPages").invoke("text");
});

Cypress.Commands.add("numberOfPageIs", (pageNumber) => {
  cy.get("span.-totalPages").contains(pageNumber);
});

Cypress.Commands.add("goToPage", (pageNumber) => {
  if (typeof pageNumber === "string") pageNumber = parseInt(pageNumber, 10) - 1;
  for (let i = 0; i < pageNumber; i++) {
    cy.get("div.-next>button").contains("Next").click();
  }
});

Cypress.Commands.add("getSizeOfPage", () => {
  cy.waitUntil(() => cy.get(".-loading").should("not.be.visible"), {
    timeout: 10000,
  });
  cy.get(".select-wrap.-pageSizeOptions>select").invoke("val");
});

Cypress.Commands.add("numberOfRowsIs", (expectedNumberOfRows) => {
  cy.getNumberOfPage().then((numPage) => {
    console.log("numPage:", numPage);
    cy.getSizeOfPage().then((sizePage) => {
      var numRow = (numPage - 1) * sizePage;
      console.log("numRow:", numRow);
      cy.goToPage(numPage - 1);
      cy;
    });
  });
});
