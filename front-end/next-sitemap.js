module.exports = {
  siteUrl: process.env.SITE_URL || "https://www.diatomicsbase.bio.ens.psl.eu/",
  generateRobotsTxt: true,
  exclude: ["/api", "/server-sitemap.xml"],
  robotsTxtOptions: {
    additionalSitemaps: [
      (process.env.SITE_URL || "https://www.diatomicsbase.bio.ens.psl.eu/") +
        "server-sitemap.xml",
    ],
  },
};
