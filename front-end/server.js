// Load .env file
require("dotenv").config({ path: "./.env.local" });
// Load fs to write log
const fs = require("fs");
// Get arguments
const argv = require("minimist")(process.argv.slice(2));
// Import to launch a Next.js server
const { createServer } = require("http");
const { parse } = require("url");
const next = require("next");

// Multiprocess import
const cluster = require("cluster");
// Get number of core from CLI then from env or .env file then default value
const nextCPUS = argv.cpu || argv.c || argv.cpus || process.env.NEXT_CPUS || 1;

// Const for Next.js server
const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();
const port = argv.port || argv.p || argv.P || process.env.NEXT_PORT || 3000;

app.prepare().then(() => {
  // Launch multiprocess in the if & launch server itself in the else
  if (cluster.isMaster) {
    // Creates the Forks
    process.title = "nextjs-master";
    for (let i = 0; i < nextCPUS; i++) {
      cluster.fork();
    }
    // and here you can fork again when one of the forks dies
    cluster.on("exit", (worker, code, signal) => {
      console.error(
        `worker ${worker.process.pid} died (${
          signal || code
        }). restarting it in a sec`
      );
      setTimeout(() => cluster.fork(), 1000);
    });
    // Ensure that the process and its child died correctly when systemd send a SIGTERM signal
    process.on("SIGTERM", () => {
      process.exit();
    });
  } else {
    // run server
    createServer((req, res) => {
      // Be sure to pass `true` as the second argument to `url.parse`.
      // This tells it to parse the query portion of the URL.
      const parsedUrl = parse(req.url, true);
      // Logging logic
      const { pathname } = parsedUrl;
      const logFile = process.env.NEXT_LOG_FILE;
      if (logFile && pathname !== "/_next/webpack-hmr") {
        const datetime = new Date().toISOString().slice(0, 19);

        const ip =
          req.headers["x-forwarded-for"] ||
          req.headers["x-real-ip"] ||
          req.connection.remoteAddress ||
          req.socket.remoteAddress ||
          req.connection.socket.remoteAddress;

        fs.appendFile(logFile, `${pathname}\t${ip}\t${datetime}\n`, (err) =>
          console.error(err)
        );
      }
      // end log
      handle(req, res, parsedUrl);
    }).listen(port, (err) => {
      if (err) throw err;
      console.log(`> Ready on http://localhost:${port}`);
    });
  }
});
