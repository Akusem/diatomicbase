import * as React from "react";
import styles from "components/Alert.module.scss";

interface alertProps extends React.HTMLAttributes<HTMLElement> {
  type: "danger" | "warning";
  children: any;
  style?: React.CSSProperties;
}

export default function Alert(props: alertProps) {
  const { type, className, children, ...otherProps } = props;
  return (
    <div
      className={`${styles.container} ${className} ${styles[type]}`}
      {...otherProps}
    >
      {children}
    </div>
  );
}
