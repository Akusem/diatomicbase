import React from "react";
import styles from "./Button.module.scss";

interface buttonProps extends React.HTMLAttributes<HTMLButtonElement> {
  disabled?: boolean;
  disabledMessage?: string;
}

export default function Button(props: buttonProps) {
  const {
    className,
    children,
    disabled = false,
    disabledMessage = "Disabled",
    ...otherProps
  } = props;
  return (
    <button
      className={`${className} ${styles.button} ${disabled && styles.disabled}`}
      {...otherProps}
      disabled={disabled}
    >
      {disabled ? disabledMessage : children}
    </button>
  );
}
