import React, { useState, useEffect } from "react";
import styles from "./CoExpressionNetworks.module.scss";
import apiUrl from "utils/getApiUrl";
import { geneInfoProps } from "pages/genes/[gene]";
import SpeciesSpecific from "components/SpeciesSpecific";

interface coExpression {
  color: string;
  kegg: Array<string>;
}

export default function CoExpressionNetworks({
  geneInfo,
}: {
  geneInfo: geneInfoProps;
}) {
  const [coExpression, setCoExpression] = useState<coExpression>();

  useEffect(() => {
    async function fetchCoExpressionNetwork(geneName: string) {
      const response = await fetch(apiUrl + "/gene/coexpression/" + geneName);
      const json = await response.json();
      setCoExpression(json);
    }

    fetchCoExpressionNetwork(geneInfo.name);
  }, [geneInfo]);

  const geneNameIsConvertible = (geneInfo: geneInfoProps) => {
    if (geneInfo.species.ensembl_name === "phaeodactylum_tricornutum") {
      return geneInfo.name.startsWith("Phatr3_J") ? true : false;
    } else {
      return true;
    }
  };

  const convertGeneNameToPhatr2 = (geneName: string) => {
    return geneName.replace("Phatr3_J", "PHATRDRAFT_");
  };

  const convertThapsGeneName = (geneName: string) => {
    // DiatomPortal use the number at the end of the gene id for Thaps
    let re = /_([0-9]*)/g;
    let res = re.exec(geneName.replace("bd", ""));
    return res[1];
  };

  const getDiatomPortalUrl = (geneInfo: geneInfoProps) => {
    switch (geneInfo.species.ensembl_name) {
      case "phaeodactylum_tricornutum":
        return `http://networks.systemsbiology.net/diatom-portal/genes/Phatr/${convertGeneNameToPhatr2(
          geneInfo.name
        )}`;
      case "thalassiosira_pseudonana":
        return `http://networks.systemsbiology.net/diatom-portal/genes/Thaps/${convertThapsGeneName(
          geneInfo.name
        )}`;
    }
  };

  return (
    <>
      <div className={styles.diatomPortal}>
        <h3>Micro-Arrays</h3>
        {geneNameIsConvertible(geneInfo) ? (
          <>
            {" "}
            data are available on{" "}
            <a
              href={getDiatomPortalUrl(geneInfo)}
              target="_blank"
              rel="noreferrer"
            >
              DiatomPortal
            </a>
            <br />
            <br />
          </>
        ) : (
          <>
            {" "}
            data aren{"'"}t available for this gene <br />
            <br />
          </>
        )}
      </div>
      <SpeciesSpecific
        species="Phaeodactylum tricornutum"
        currentSpecies={geneInfo.species.name}
      >
        <div className={styles.phaeonet}>
          {coExpression &&
            (coExpression.color !== "#N/D" ? (
              <>
                <h3>Phaeonet Card:</h3>
                <div
                  className={`${styles.card} ${getColor(coExpression.color)}`}
                >
                  <h3
                    id="phaeonet-card"
                    className={`${styles.cardTitle} ${getColor(
                      coExpression.color
                    )}`}
                  >
                    {coExpression.color}
                  </h3>
                  <div className={styles.cardContent}>
                    {coExpression.kegg.length !== 0 ? (
                      <p className={styles.description}>Enriched in:</p>
                    ) : (
                      <p className={styles.description}>
                        No enrichissement for DarkSlateBlue
                      </p>
                    )}
                    <br />
                    {coExpression.kegg.map((kegg_text, i) => (
                      <div key={i}>
                        {formatKeggTextWithHyperLink(kegg_text)}
                      </div>
                    ))}
                  </div>
                </div>
              </>
            ) : (
              <>
                <h3 className={styles.inline}>Phaeonet Card</h3> not available
                for this gene
              </>
            ))}
        </div>
      </SpeciesSpecific>
    </>
  );
}

function formatKeggTextWithHyperLink(kegg_text: string) {
  const regex = /(ko\d*)]$/;
  const kegg_id = regex.exec(kegg_text)[1];
  const kegg_text_without_id = kegg_text.replace(regex, "");
  return (
    <p>
      {kegg_text_without_id}
      <a
        href={`https://www.genome.jp/dbget-bin/www_bget?pathwkay+${kegg_id}`}
        target="_blank"
        rel="noreferrer"
      >
        {kegg_id}
      </a>
      ]
    </p>
  );
}

function getColor(color: string): string {
  switch (color) {
    case "orange":
      return styles.orange;
    case "cyan":
      return styles.cyan;
    case "salmon":
      return styles.salmon;
    case "green":
      return styles.green;
    case "floralwhite":
      return styles.floralwhite;
    case "darkgrey":
      return styles.darkgrey;
    case "magenta":
      return styles.magenta;
    case "greenyellow":
      return styles.greenyellow;
    case "skyblue":
      return styles.skyblue;
    case "blue":
      return styles.blue;
    case "violet":
      return styles.violet;
    case "brown":
      return styles.brown;
    case "grey":
      return styles.grey;
    case "darkmagenta":
      return styles.darkmagenta;
    case "steelblue":
      return styles.steelblue;
    case "tan":
      return styles.tan;
    case "paleturquoise":
      return styles.paleturquoise;
    case "ivory":
      return styles.ivory;
    case "darkgreen":
      return styles.darkgreen;
    case "red":
      return styles.red;
    case "bisque4":
      return styles.bisque;
    case "black":
      return styles.black;
    case "brown4":
      return styles.darkbrown;
    case "lightcyan1":
      return styles.lightcyan;
    case "lightsteelblue1":
      return styles.lightsteelblue;
    case "mediumpurple3":
      return styles.mediumpurple;
    case "orangered4":
      return styles.orangered;
    case "darkslateblue":
      return styles.darkslateblue;
    default:
      return "";
  }
}
