import React, { useEffect, useRef } from "react";
import Link from "next/link";

declare global {
  interface Window {
    commento: any;
  }
}

export default function Comments(props: {
  pageId?: string;
  template?: "gene";
}) {
  let { pageId, template } = props;

  // Use a ref to be able to pass pageId to and from useEffect
  const pageIdRef = useRef<string>();
  if (pageId !== undefined && pageId !== pageIdRef.current) {
    pageIdRef.current = pageId;
  }

  // Verify that Commento server have been setup, put warning if not the case
  const commentoURL = process.env.NEXT_PUBLIC_COMMENTO_URL;
  if (!commentoURL) console.warn("No Commento URL, comments deactivated");

  useEffect(() => {
    function setTemplateInCommentBox(template: string) {
      const textarea = document.getElementById(
        "commento-textarea-root"
      ) as HTMLTextAreaElement;
      if (!textarea) return false;
      textarea.textContent = template;
      return true;
    }

    // If Commento is not configured, don't load it
    if (!commentoURL) return;
    // If server side, don't do anything
    if (typeof window === undefined) return;
    // Use Path as pageId if not defined
    if (pageIdRef.current === undefined)
      pageIdRef.current = window.location.pathname;
    // Init commento script
    if (!window.commento) {
      // init empty object so commento.js script extends this with global functions
      window.commento = {};
      const script = document.createElement("script");
      // Replace this with the url to your commento instance's commento.js script
      script.src = `${commentoURL}/js/commento.js`;
      script.defer = true;
      // Set default attributes for first load
      script.setAttribute("data-auto-init", "false");
      script.setAttribute("data-page-id", pageIdRef.current);
      script.setAttribute("data-id-root", "commento-box");
      script.setAttribute("data-no-websockets", "true");
      script.setAttribute("data-no-livereload", "true");
      script.onload = () => {
        // Tell commento.js to load the widget
        window.commento.main();
      };
      document.getElementsByTagName("head")[0].appendChild(script);
    } else {
      // In-case the commento.js script has already been loaded reInit the widget with a new pageId
      window.commento.reInit({
        pageId: pageIdRef.current,
      });
    }

    // Set template when comment box is ready
    if (template) {
      var observer = new MutationObserver((mutations, me) => {
        // `mutations` is an array of mutations that occurred
        // `me` is the MutationObserver instance
        var textarea = document.getElementById("commento-textarea-root");
        if (textarea) {
          me.disconnect(); // stop observing
          // Set template for gene
          if (template === "gene") {
            setTemplateInCommentBox(
              "Comment:\n\nName:\n\nLab:\n\nPublication:\n\n"
            );
          }
          return;
        }
      });

      // Launch observer
      observer.observe(document, {
        childList: true,
        subtree: true,
      });

      return () => {
        // Disconnect observer if component is unmounted before the callback have found the textarea
        if (observer) observer.disconnect();
      };
    }
  }, [template, commentoURL]);

  if (!commentoURL) {
    return <div id="no-commento"></div>;
  }

  return (
    <>
      <h3>Comments</h3>
      <hr />
      <br />
      <div style={{ margin: "0em 1em 2em" }}>
        <div>
          Here you can post additional information about this gene: mutant,
          expression studies, gene model prediction... (reviewed data only).{" "}
          <br /> To leave a comment, please specify: your name, your lab and the
          publication you refer to.
        </div>
        <br />
        <div style={{ color: "grey", fontStyle: "italic" }}>
          Before posting, please make sure your comment follows{" "}
          <Link href="/legalNotice#comments-policy">the comments policy</Link>
        </div>
      </div>
      <div id="commento-box" />
    </>
  );
}
