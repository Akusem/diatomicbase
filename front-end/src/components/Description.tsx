import React from "react";
import styles from "./Description.module.scss";

export default function Description(props: React.HTMLProps<HTMLElement>) {
  const { children, className, ...otherProps } = props;
  return (
    //@ts-ignore
    <div className={`${styles.description} ${className}`} {...otherProps}>
      {children}
    </div>
  );
}
