import React, { useState } from "react";
import styles from "./Dropdown.module.scss";
import useOuterClick from "hooks/useOuterClick";

interface dropdownProps extends React.HTMLAttributes<HTMLElement> {
  name: string;
  contents: Array<React.ReactNode>;
}

export default function Dropdown(props: dropdownProps) {
  const { name, contents, ...otherProps } = props;
  // Logic to close dropdown on click outside
  const [checked, setChecked] = useState(false);
  const innerRef = useOuterClick(() => setChecked(false));
  return (
    <label ref={innerRef} className={styles.dropdown} {...otherProps}>
      <div className={styles.ddButton} onClick={() => setChecked(!checked)}>
        {name}
      </div>
      <input
        type="checkbox"
        className={styles.ddInput}
        checked={checked}
        readOnly
      />
      <ul className={styles.ddMenu} onClick={() => setChecked(false)}>
        {contents.map((content, idx) => {
          return <li key={idx}>{content}</li>;
        })}
      </ul>
    </label>
  );
}
