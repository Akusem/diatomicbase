import React, { useEffect, useState } from "react";
import { fetchAssemblyAndTracks } from "pages/genes/[gene]";
import {
  assemblyInterface,
  tracksInterface,
  textSearchInterface,
} from "pages/genes/[gene]";
import "requestidlecallback-polyfill";
import {
  createViewState,
  JBrowseLinearGenomeView,
} from "@jbrowse/react-linear-genome-view";

interface defaultSessionInterface {
  name: string;
  view: {
    id: string;
    type: string;
    displayRegions?: Array<{
      id: string;
      refName: string;
      assemblyNames: Array<string>;
    }>;
    tracks: Array<{
      type: string;
      configuration: string;
      displays: Array<{
        type: string;
        height?: number;
      }>;
    }>;
  };
}

interface GenomeLinearViewProps {
  species?: string;
  location: string;
  selectedGene?: string[];
  defaultSession?: defaultSessionInterface;
  assembly?: assemblyInterface;
  tracks?: tracksInterface[];
  textSearchIndex?: textSearchInterface[];
}

export default function GenomeLinearView(props: GenomeLinearViewProps) {
  const [state, setState] = useState<{
    location: string;
    defaultSession?: defaultSessionInterface;
    assembly?: assemblyInterface;
    tracks?: tracksInterface[];
    textSearchIndex: textSearchInterface[];
  }>();

  useEffect(() => {
    const setupGenomeBrowser = async (props: GenomeLinearViewProps) => {
      let { location, defaultSession, assembly, tracks, textSearchIndex } =
        props;
      // If not define through props, fetch assembly, tracks, searchIndex and defaultSession
      if (
        tracks === undefined ||
        assembly === undefined ||
        defaultSession === undefined
      ) {
        const {
          assembly: fetchedAssembly,
          tracks: fetchedTracks,
          textSearchIndex: fetchedTextSearchIndex,
          defaultSession: fetchedDefaultSession,
        } = await fetchAssemblyAndTracks(props.species);
        assembly = fetchedAssembly;
        tracks = fetchedTracks;
        textSearchIndex = fetchedTextSearchIndex;
        if (!defaultSession) {
          defaultSession = fetchedDefaultSession;
        }
      }

      setState({
        assembly: assembly,
        tracks: tracks,
        location: location,
        defaultSession: defaultSession,
        textSearchIndex: textSearchIndex,
      });
    };
    setupGenomeBrowser(props);

    // Modify genomeBrowser look with js script

    const highlightTrackButton = () => {
      const target = document.getElementsByClassName(
        "MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textSecondary MuiButton-textSizeSmall MuiButton-sizeSmall"
      );
      if (target.length !== 0) {
        const button = target[0];
        if (!button.className.endsWith("selectTracksButton")) {
          button.className = `${button.className} selectTracksButton`;
          button.innerHTML = "<strong>Tracks Selector</strong>";
        }
      }
      return false;
    };

    // Use the gene name display below the representation (define by 'name' field in gff) to find selected gene.
    // Never return true because user can move in the genome browser far away enough that the gene is re-render
    // when he get back to the selected gene.
    const highlightSelectedGenes = (selectedGenes: string[]): boolean => {
      selectedGenes = selectedGenes
        .filter((gene) => gene != null)
        .map((gene) => gene.toLowerCase());
      const targets = document.getElementsByTagName("text");
      if (targets.length !== 0) {
        //@ts-ignore
        for (const target of targets) {
          if (isInList(selectedGenes, target.textContent)) {
            changeColorSibling(target);
          }
        }
      }
      return false;
    };

    const isInList = (selectedGenes: string[], textContent: string) => {
      textContent = textContent.toLowerCase();
      for (const gene of selectedGenes) {
        if (textContent.includes(gene)) return true;
      }
      return false;
    };

    // Recursive function to change gene representation color
    const changeColorSibling = (target: HTMLElement) => {
      const sibling = target.previousSibling as HTMLElement;
      if (!sibling) return;
      if (sibling.hasAttribute("fill")) {
        if (colorAlreadyChanged(sibling)) return;
        const fillColor = sibling.getAttribute("fill");
        if (fillColor === "goldenrod") {
          sibling.setAttribute("fill", "#353b3c");
        } else if (fillColor === "#357089") {
          sibling.setAttribute("fill", "#f2542d");
        }
      }
      changeColorSibling(sibling);
    };

    const colorAlreadyChanged = (target) => {
      const fillColor = target.getAttribute("fill");
      if (fillColor === "#353b3c" || fillColor === "#f2542d") return true;
      else return false;
    };

    // Higlight tracks button and change color of selected gene
    const intervalTrackButtonId = setInterval(highlightTrackButton, 450);
    let intervalSelectedGeneId;
    if (props.selectedGene) {
      intervalSelectedGeneId = setInterval(
        () => highlightSelectedGenes(props.selectedGene),
        450
      );
    }

    return function cleanupInterval() {
      clearInterval(intervalTrackButtonId);
      if (props.selectedGene) {
        clearInterval(intervalSelectedGeneId);
      }
    };
  }, [props]);

  const viewState =
    state &&
    createViewState({
      assembly: state.assembly[0],
      tracks: state.tracks,
      location: state.location,
      defaultSession: state.defaultSession[0],
      // aggregateTextSearchAdapters: state.textSearchIndex,
      configuration: {
        theme: {
          palette: {
            secondary: {
              main: "#333",
            },
          },
        },
      },
    });

  return (
    <div>
      {state && (
        <div>
          {/* For a reason I don't understand using a module.css doesn't work so I use this ugly ass inline css rules */}
          <style>
            {`.selectTracksButton {
                  border: 1px solid #c4c4c4;
                  background: rgba(255, 255, 255, 0.8);
                  font-weight: bolder;
                  color: #135572;
                }
                .MuiPaper-root.MuiPaper-elevation1.MuiPaper-rounded {
                  font-family: "Roboto", "Helvetica", "Arial", sans-serif;
                }
              `}
          </style>
          <JBrowseLinearGenomeView viewState={viewState} />
        </div>
      )}
    </div>
  );
}
