import React from "react";

interface LinkArticleProps {
  publication: string;
  doi: string;
}

/**
 * Return Article as href with 'et al' in italic. I
 * Support multiple article if separated by '&'.
 */
export default function LinkArticle(props: LinkArticleProps) {
  const { publication, doi } = props;
  const listPublications = extractPublication(publication, doi);
  return (
    <>
      {listPublications.map((value, i) => (
        <div style={{ marginBottom: "0.5em" }} key={i}>
          {value.doi === "NA" ? (
            <div>{value.Publication}</div>
          ) : (
            <a href={value.doi} target="_blank" rel="noopener noreferrer">
              {formatEtAl(value.Publication)}
            </a>
          )}
        </div>
      ))}
    </>
  );
}

function extractPublication(publication: string, doi: string) {
  const publications = publication.split("&");
  const dois = doi.split("&");
  let toReturn = [];
  for (let i = 0; i < publications.length; i++) {
    toReturn.push({ Publication: publications[i], doi: dois[i] });
  }
  return toReturn;
}

function formatEtAl(publi: string) {
  let pub = publi.split("et al.");

  if (pub.length === 1) {
    // verify it not a question of point
    pub = publi.split("et al,");
    // If really not 'et al.' return
    if (pub.length === 1) return publi;
  }
  return (
    <div>
      {pub[0]}
      <i>et al.</i>
      {pub[1]}
    </div>
  );
}
