import React from "react";
import Link from "next/link";
import SearchBar from "components/SearchBar";
import styles from "./Navbar.module.scss";
import SpeciesDropSelector from "components/SpeciesDropSelector";

export default function NavBar() {
  return (
    <>
      <div className={styles.banner}>
        <div>
          <Link href="/">
            <a className={styles.bannerTitle}>DiatOmicBase</a>
          </Link>
          <SpeciesDropSelector
            id="globalSpeciesSelector"
            className={styles.globalSpeciesSelector}
          />
        </div>
        <SearchBar />
      </div>
      <div className={styles.topnav}>
        <Link href="/">
          <a>Home</a>
        </Link>
        <Link href="/searchPage">
          <a>Gene Page</a>
        </Link>
        <Link href="/genomeBrowser">
          <a>Genome Browser</a>
        </Link>
        <Link href="/resources">
          <a>Resources</a>
        </Link>
        <Link href="/transcriptomicsChoice">
          <a>Transcriptomics</a>
        </Link>
        <Link href="/help">
          <a>Help</a>
        </Link>
      </div>
    </>
  );
}
