import React, { useState, useEffect, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckSquare, faSquare } from "@fortawesome/free-regular-svg-icons";
import styles from "./OtherInformation.module.scss";
import apiUrl from "utils/getApiUrl";
import InfoToggle from "components/InfoToggle";
import { ProteogenomicsProps, TableInfoToggle } from "pages/genes/[gene]";

interface OtherInformationProps {
  geneName: string;
  proteogenomicsData: ProteogenomicsProps[];
}

interface otherInformationData {
  polycomb: boolean;
  intron_retention: boolean;
  exon_skipping: boolean;
  asafind: string;
  hectar: string;
  mitofates: string;
  wsort_animal: string;
  wsort_plant: string;
  wsort_fungi: string;
  wsort_consensus: string;
  OP_lineage: string;
  OP_subgroup: string;
  OP_nbtophit: string;
  OP_localdiversity: string;
  SC_lineage: string;
  SC_subgroup: string;
  SC_nbtophit: string;
  SC_localdiversity: string;
  epigenomic_marks: string;
}

export default function OtherInformation({
  geneName,
  proteogenomicsData,
}: OtherInformationProps) {
  const [data, setData] = useState<otherInformationData>();
  const [notPresent, setNotPresent] = useState(false);

  useEffect(() => {
    const fetchPhaeoData = async (geneName: string) => {
      const res = await fetch(`${apiUrl}/gene/phaeonet/${geneName}`);
      if (res.status === 404) {
        setNotPresent(true);
        return;
      }
      const json = await res.json();
      setData(json);
    };

    fetchPhaeoData(geneName);
  }, [geneName]);

  if (notPresent) {
    return <div>Gene not present in phaeonet</div>;
  }

  return (
    <>
      {data && (
        <div className={styles.container}>
          <div className={styles.row}>
            <AssociatedEpigenomicMarks data={data} />
            <Proteogenomics data={proteogenomicsData} />
          </div>
          <div className={styles.row}>
            <HistoneMethylation data={data} />
            <AlternativeSplicing data={data} />
          </div>

          <div className={styles.row}>
            <TargetingPrediction data={data} />
            <WolfPSort data={data} />
          </div>

          <EvolutionaryOriginTable data={data} />
        </div>
      )}
    </>
  );
}

function Proteogenomics(props: { data: ProteogenomicsProps[] }) {
  const { data, ...otherProps } = props;
  return (
    <div style={{ marginLeft: "2em", marginBottom: "1em" }}>
      <h4 className={styles.underline}>Proteogenomics:</h4>
      <table className={styles.proteogenomicsTable} {...otherProps}>
        <thead>
          <tr>
            <th>Phatr2 gene ID</th>
            <th>
              Detection in Yang 2018
              <TableInfoToggle heightMultiplier={2}>
                Unambiguous identification of the protein with evidence from at
                least two unique peptides or from at least one uniquely
                identifying peptide combined with manual validation <br />
                From{" "}
                <a href="https://doi.org/10.1016/j.molp.2018.08.005">
                  Yang <i>et al.</i> 2018
                </a>
                : Genome Annotation of a Model Diatom Phaeodactylum tricornutum
                Using an Integrated Proteogenomic Pipeline.
              </TableInfoToggle>
            </th>
            <th>
              Potential Non-Coding gene
              <TableInfoToggle heightMultiplier={2}>
                Based on the analysis of atypical coding features (e.g. No
                Protein Features, Non Functional Protein, Poor Conservation).{" "}
                <br />
                From{" "}
                <a href="https://doi.org/10.1016/j.molp.2018.08.005">
                  Yang <i>et al.</i> 2018
                </a>
                : Genome Annotation of a Model Diatom Phaeodactylum tricornutum
                Using an Integrated Proteogenomic Pipeline
              </TableInfoToggle>
            </th>
            <th>
              Post translational modification
              <TableInfoToggle heightMultiplier={2}>
                PTM prediction among 24 modifications common in eukaryotes using
                MaxQuant. <br />
                From{" "}
                <a href="https://doi.org/10.1016/j.molp.2018.08.005">
                  Yang <i>et al.</i> 2018
                </a>
                : Genome Annotation of a Model Diatom Phaeodactylum tricornutum
                Using an Integrated Proteogenomic Pipeline.
              </TableInfoToggle>
            </th>
          </tr>
        </thead>
        <tbody>
          {data.map((row, i) => {
            return (
              <tr key={i} id={`proteogenomics-row-${i}`}>
                <td>{row.name}</td>
                <td>{row.detection_in_yang_2018 === true ? "Yes" : "No"}</td>
                <td>{row.potential_non_coding_gene}</td>
                <td>{row.post_translational_modification}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

function AssociatedEpigenomicMarks({ data }: { data: otherInformationData }) {
  const { epigenomic_marks } = data;
  const listEpiMarks = epigenomic_marks.split(",");
  return (
    <div id="associated-epigenomic-mark">
      <h4>
        <InfoDisplay
          line={
            <span className={styles.underline} style={{ marginRight: "4px" }}>
              Associated Epigenomic Marks:
            </span>
          }
          description={
            <div className={styles.normalFontWeight}>
              List of associated epigenomics marks as described by{" "}
              <a
                href="https://doi.org/10.1186/s13059-015-0671-8"
                target="_blank"
                rel="noopener noreferrer"
              >
                Veluchamy{" "}
                <i>
                  <i>et al.</i>
                </i>{" "}
                2015
              </a>
            </div>
          }
          widthMultiplier={1.5}
        />
      </h4>
      <h4 className={styles.underline}></h4>
      {epigenomic_marks ? (
        <ul>
          {listEpiMarks.map((epi, i) => (
            <li key={i}>{epi}</li>
          ))}
        </ul>
      ) : (
        <div>None</div>
      )}
    </div>
  );
}

function HistoneMethylation({ data }: { data: otherInformationData }) {
  return (
    <div className={styles.histoneContainer}>
      <h4 className={styles.underline}>Histone methylation:</h4>
      <span
        id={
          data.polycomb
            ? "histone-methylation-checked"
            : "histone-methylation-unchecked"
        }
      >
        <CheckedIcon style={{ margin: "3px 5px" }} checked={data.polycomb} />{" "}
        <InfoDisplay
          line={"Polycomb marked gene"}
          description={
            <>
              Whether the gene is marked by Polycomb group histone methylation (
              <a
                href="https://doi.org/10.3389/fmars.2020.00189"
                target="_blank"
                rel="noopener noreferrer"
              >
                Zhao{" "}
                <i>
                  <i>et al.</i>
                </i>
                , 2020
              </a>
              ).
            </>
          }
          addition={30}
        />
      </span>
    </div>
  );
}

interface checkedIconProps extends React.HTMLAttributes<HTMLElement> {
  checked: boolean;
}

function CheckedIcon(props: checkedIconProps) {
  const { checked, className, style } = props;
  return (
    <FontAwesomeIcon
      style={style}
      className={className}
      icon={checked ? faCheckSquare : faSquare}
    />
  );
}

interface InfoDisplayProps {
  line: React.ReactNode;
  description: React.ReactNode;
  width?: number;
  widthMultiplier?: number;
  maxWidthPercentage?: number;
  addition?: number;
  className?: string;
}

function InfoDisplay(props: InfoDisplayProps) {
  const {
    line,
    width,
    className,
    description,
    widthMultiplier,
    maxWidthPercentage,
    addition = 0,
  } = props;
  // Display bioproject tooltip when checked
  const [checked, setChecked] = useState(false);
  // Set offset of hidden div with relative information
  const [lineWidth, setLineWidth] = useState(0);
  const ref = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    setLineWidth(ref.current.offsetWidth);
  }, []);

  return (
    <div className={`${styles.rowInfo} ${className}`}>
      <div ref={ref} style={{ display: "flex" }}>
        {line}
        <InfoToggle checked={checked} setChecked={setChecked} />
      </div>
      <div
        className={
          checked ? styles.experienceInfo : styles.experienceInfoHidden
        }
        style={
          maxWidthPercentage !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                maxWidth: `${maxWidthPercentage}%`,
              }
            : width !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                width: `${width}px`,
              }
            : widthMultiplier !== undefined
            ? {
                left: `${lineWidth + addition}px`,
                width: `${(lineWidth + addition) * widthMultiplier}px`,
              }
            : {
                left: `${lineWidth + addition}px`,
                width: `${(lineWidth + addition) * 2}px`,
              }
        }
      >
        {description}
      </div>
    </div>
  );
}

function AlternativeSplicing({ data }: { data: otherInformationData }) {
  return (
    <div className={styles.alterSplicing}>
      <h4>
        <span className={styles.underline} style={{ marginRight: "4px" }}>
          Alternative splicing
        </span>{" "}
        under N stress
        <TableInfoToggle>
          <div className={styles.normalFontWeight}>
            Whether transcripts of the gene are identified to undergo
            alternative splicing under different N stress conditions (
            <a
              href="https://doi.org/10.1038/s41598-018-23106-x"
              target="_blank"
              rel="noopener noreferrer"
            >
              Rastogi{" "}
              <i>
                <i>et al.</i>
              </i>{" "}
              2018
            </a>
            ,{" "}
            <a
              href="https://doi.org/10.1105/tpc.16.00910"
              target="_blank"
              rel="noopener noreferrer"
            >
              McCarthy <i>et al.</i> 2017
            </a>
            ).
          </div>
        </TableInfoToggle>
      </h4>
      <div className={styles.row}>
        <span
          id={
            data.intron_retention
              ? "intron-retention-checked"
              : "intron-retention-unchecked"
          }
          className={styles.alterSplicingElement}
        >
          <CheckedIcon checked={data.intron_retention} /> Intron retention
        </span>
        <span
          id={
            data.exon_skipping
              ? "exon-skipping-checked"
              : "exon-skipping-unchecked"
          }
          className={styles.alterSplicingElement}
        >
          <CheckedIcon checked={data.exon_skipping} /> Exon skipping
        </span>
      </div>
    </div>
  );
}

function TargetingPrediction({ data }: { data: otherInformationData }) {
  return (
    <div className={styles.targetPredictions}>
      <h4 className={styles.underline}>Targeting predictions:</h4>
      <div className={styles.row}>
        <InfoDisplay
          line={"ASAfind"}
          description={
            <>
              Targeting predictions identified for each protein using ASAFind
              and SignalP v 3.0 (
              <a
                href="https://doi.org/10.1016/j.jmb.2004.05.028"
                target="_blank"
                rel="noopener noreferrer"
              >
                Bendtsen <i>et al.</i>, 2004
              </a>
              ;{" "}
              <a
                href="https://doi.org/10.1111/tpj.12734"
                target="_blank"
                rel="noopener noreferrer"
              >
                Gruber <i>et al.</i>,2015
              </a>
              )
            </>
          }
          widthMultiplier={5}
          addition={20}
        />{" "}
        <span id="asafind">{data.asafind}</span>
      </div>
      <div className={styles.row}>
        <InfoDisplay
          line={"HECTAR"}
          description={
            <>
              Targeting predictions identified for each protein using HECTAR (
              <a
                href="https://doi.org/10.1186/1471-2105-9-393"
                target="_blank"
                rel="noopener noreferrer"
              >
                Gschloessl <i>et al.</i>, 2008
              </a>
              )
            </>
          }
          widthMultiplier={5}
          addition={20}
        />{" "}
        <span id="hectar">{data.hectar}</span>
      </div>
      <div className={styles.row}>
        <InfoDisplay
          line={"MitoFates"}
          description={
            <>
              Targeting predictions identified for each protein using a modified
              version of MitoFates with threshold value 0.35 (
              <a
                href="https://doi.org/10.1074/mcp.M114.043083"
                target="_blank"
                rel="noopener noreferrer"
              >
                Fukasawa <i>et al.</i>, 2015
              </a>
              )
            </>
          }
          widthMultiplier={5}
          addition={20}
        />{" "}
        <span id="mitofates">{data.mitofates}</span>
      </div>
    </div>
  );
}

function WolfPSort({ data }: { data: otherInformationData }) {
  return (
    <div className={styles.wolfPSort}>
      <h4 className={styles.underline}>
        <span className={styles.underline}>WolfPSort</span>
        <TableInfoToggle>
          <div className={styles.normalFontWeight}>
            Targeting predictions identified for each protein using WolfPSort
            with animal reference libraries (
            <a
              href="https://doi.org/10.1016/j.scitotenv.2017.01.190"
              target="_blank"
              rel="noopener noreferrer"
            >
              Horton <i>et al.</i>, 2017
            </a>
            )
          </div>
        </TableInfoToggle>
      </h4>
      <div id="wolfpsort-animal" className={styles.row}>
        Animal: {data.wsort_animal}
      </div>
      <div id="wolfpsort-plant" className={styles.row}>
        Plant: {data.wsort_plant}
      </div>
      <div id="wolfpsort-fungi" className={styles.row}>
        Fungi: {data.wsort_fungi}
      </div>
      <div id="wolfpsort-consensus" className={styles.row}>
        CONSENSUS: {data.wsort_consensus}
      </div>
    </div>
  );
}

function EvolutionaryOriginTable({ data }: { data: otherInformationData }) {
  return (
    <div className={styles.gridContainer}>
      <InfoDisplay
        className={`${styles.evHeader} ${styles.gridCol1} ${styles.gridRow1}`}
        line={<span>Evolutionary origins</span>}
        description={
          <>
            Probable evolutionary origin of each protein using BLAST top hit
            analysis of a combined prokaryotic and eukaryotic tree of life,
            excluding all SAR and CCTH clade members ({" "}
            <a
              href="https://doi.org/10.1038/s41598-018-23106-x"
              target="_blank"
              rel="noopener noreferrer"
            >
              Rastogi <i>et al.</i> 2018
            </a>
            ). The statistics provided are: the lineage, and specific taxonomic
            sub-group in which the best BLAST hit was identified; and two
            confidence scores (with thresholds '3' and 'yes' for identifying
            hits of non-contaminant origin).
          </>
        }
        addition={60}
      />
      <div className={`${styles.gridCol1} ${styles.gridRow2}`}>Lineage</div>
      <InfoDisplay
        className={`${styles.gridCol1} ${styles.gridRow3}`}
        line={"Sub-group"}
        description={
          "Specific taxonomic sub-group in which the best BLAST hit was identified"
        }
        addition={60}
      />
      <InfoDisplay
        className={`${styles.gridCol1} ${styles.gridRow4}`}
        line={"Number of consecutive top hits"}
        description={
          "confidence score (with thresholds '3' for " +
          "identifying hits of non-contaminant origin)"
        }
        addition={60}
        maxWidthPercentage={40}
      />
      <InfoDisplay
        className={`${styles.gridCol1} ${styles.gridRow5}`}
        line={"Local diversity in top hits"}
        description={
          "confidence scores (with 'yes' for " +
          "identifying hits of non-contaminant origin)"
        }
        addition={60}
      />
      <div className={`${styles.gridCol2} ${styles.gridRow1}`}>
        Excluding ochrophytes and secondary plastids
      </div>
      <div
        id="evolutionary-op-lineage"
        className={`${styles.gridCol2} ${styles.gridRow2}`}
      >
        {data.OP_lineage}
      </div>
      <div
        id="evolutionary-op-subgroup"
        className={`${styles.gridCol2} ${styles.gridRow3}`}
      >
        {data.OP_subgroup}
      </div>
      <div
        id="evolutionary-op-nbtophit"
        className={`${styles.gridCol2} ${styles.gridRow4}`}
      >
        {data.OP_nbtophit}
      </div>
      <div
        id="evolutionary-op-local-diversity"
        className={`${styles.gridCol2} ${styles.gridRow5}`}
      >
        {data.OP_localdiversity}
      </div>
      <div className={`${styles.gridCol3} ${styles.gridRow1}`}>
        Excluding SAR and CCTH
      </div>
      <div
        id="evolutionary-sc-lineage"
        className={`${styles.gridCol3} ${styles.gridRow2}`}
      >
        {data.SC_lineage}
      </div>
      <div
        id="evolutionary-sc-subgroup"
        className={`${styles.gridCol3} ${styles.gridRow3}`}
      >
        {data.SC_subgroup}
      </div>
      <div
        id="evolutionary-sc-nbtophit"
        className={`${styles.gridCol3} ${styles.gridRow4}`}
      >
        {data.SC_nbtophit}
      </div>
      <div
        id="evolutionary-sc-local-diversity"
        className={`${styles.gridCol3} ${styles.gridRow5}`}
      >
        {data.SC_localdiversity}
      </div>
    </div>
  );
}
