import React, { useState, useEffect, useRef } from "react";

export default function useElementSize(): [
  { width: number; height: number },
  React.MutableRefObject<HTMLElement>
] {
  const [size, setSize] = useState({ width: 0, height: 0 });
  const elRef = useRef<HTMLElement>(null);

  useEffect(() => {
    if (elRef.current) {
      setSize({
        width: elRef.current.offsetWidth,
        height: elRef.current.offsetHeight,
      });
    }
  }, [elRef]);

  return [size, elRef];
}
