import React, { useState, useEffect, useRef } from "react";
import pathJoin from "utils/pathJoin";

type statusType = "queueing" | "running" | "done" | "error";

interface responseType {
  detail?: string;
  parameter?: any;
  status: statusType;
  [k: string]: any;
}

/**
 * Hide query logic when waiting for a long process to finish on DOB,
 * will query at regular interval the server to get the status of the query
 * /!\ useLoadingStatus will append the runID to the URL like that:
 * ${apiUrl}/example/path/${loadingId}
 *
 * Ensure The API handle it correcly
 *
 * This will return a responseType which contain the detail of the request, the parameter
 * and the status ("queueing" | "running" | "done" | "error"). the status is directly return
 * by the hook as first return to allow simple conditional rendering in the code
 * @param url loadingStatus is going to query (will append runID at the end)
 * @param intervalTime interval between query to the DOB (default 2s)
 * @returns An object with a status (like done or error), a response, and setRunId function
 */
export default function useLoadingStatus(url: string, intervalTime = 2000) {
  const [loadingId, setLoadingId] = useState(-1);
  const [status, setStatus] = useState<statusType>();
  const [response, setResponse] = useState<responseType>();
  // Stock intervalId outside of useEffect without re-render
  const interval = useRef(null);

  // SetInterval to get status of run launched by user
  useEffect(() => {
    // If no run return
    if (loadingId === -1) return;

    async function getStatus() {
      const res = await fetch(pathJoin(url, loadingId.toString()));
      const json = await res.json();
      // In case of an error, add status = error to be used correctly by the rest of the app
      if (!res.ok) {
        json.status = "error";
      }
      setResponse(json);
      setStatus(json.status);
    }

    // Get status immediatly and setInterval
    getStatus();
    interval.current = setInterval(() => getStatus(), intervalTime);
    // Remove interval when leaving page if not already done
    return function clean() {
      clearInterval(interval.current);
    };
  }, [loadingId, url, intervalTime]);

  // Remove interval of getStatus when run is finished or in case of an error
  useEffect(() => {
    if (status === "done" || status === "error")
      clearInterval(interval.current);
  }, [status]);

  return [status, response, setLoadingId] as [
    statusType,
    responseType,
    React.Dispatch<React.SetStateAction<number>>
  ];
}
