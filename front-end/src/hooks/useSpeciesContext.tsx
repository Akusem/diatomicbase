import {
  useState,
  useEffect,
  useContext,
  createContext,
  SetStateAction,
} from "react";
import apiUrl from "utils/getApiUrl";
import useQuery from "hooks/useQuery";

export interface SpeciesDataType {
  selected: null | string;
  species: Array<{
    name: string;
    ensembl_name: string;
    assemblies: Array<string>;
  }>;
}

//@ts-ignore
const SpeciesContext = createContext<{
  speciesData: SpeciesDataType;
  setSelectedSpecies: React.Dispatch<SetStateAction<string>>;
}>();

export function SpeciesContextProvider({ children }) {
  const speciesInStorage = getSpeciesInLocalStorage();
  const [selectedSpecies, setSelectedSpecies] = useState(speciesInStorage);
  const [speciesList, setSpeciesList] = useState();

  useEffect(() => {
    async function fetchSpecies() {
      const res = await fetch(`${apiUrl}/species/`);
      if (!res.ok) {
        console.error("Can't Fetch species");
      }
      const species = await res.json();
      setSpeciesList(species);
    }

    fetchSpecies();
  }, []);

  // Update global species in storage on change, so the users doesn't have to reselect it between session.
  useEffect(() => {
    setSpeciesInLocalStorage(selectedSpecies);
  }, [selectedSpecies]);

  const speciesData: SpeciesDataType = {
    selected: selectedSpecies,
    species: speciesList,
  };

  return (
    <SpeciesContext.Provider value={{ speciesData, setSelectedSpecies }}>
      {children}
    </SpeciesContext.Provider>
  );
}

function getSpeciesInLocalStorage() {
  // Return null on server-side pre-render
  if (typeof window === "undefined") return null;
  // Return what is stored or not in localStorage
  const storedSpecies = localStorage.getItem("species");
  // On first access to DOB, species is set to string 'null' in storage.
  // I don't know the cause, so I'm patching it here by manually
  // cleaning the storage if 'null' is set and ensure it return a real null
  if (storedSpecies === "null") {
    localStorage.removeItem("species");
    return null;
  } else return storedSpecies;
}

function setSpeciesInLocalStorage(species: string) {
  // If we put directly null, it will be stored as string which we make the null test not working,
  // so we remove species from localStorage so 'localStorage.getItem()' return null
  if (species === null) localStorage.removeItem("species");
  localStorage.setItem("species", species);
}

export default function useSpeciesContext() {
  return useContext(SpeciesContext);
}

/**
 * Determine if a species is selected globally, or locally (via the query parameter).
 * Return selectedSpecies if present, else null
 * @returns species selected or null
 */
export function useSpeciesSelected(): string {
  const query = useQuery();
  const { speciesData } = useSpeciesContext();
  if (!query) return null; // Return null while waiting for query loading
  // Use global selector first, then use local selector through query parameter in the URL
  if (speciesData.selected) {
    return speciesData.selected;
  } else {
    return query.species as string;
  }
}

/**
 * useSpeciesSelected doesn't inform you if it's a global selection or local,
 * So I implemented useIsGlobalSpecies for that
 * @returns true if speciesSelected is global else False
 */
export function useIsGlobalSpecies() {
  const { speciesData } = useSpeciesContext();
  if (speciesData.selected) return true;
  else return false;
}
