import * as React from "react";
import Head from "next/head";
import Link from "next/link";

export default function Error404() {
  return (
    <>
      <Head>
        <title>404-Error</title>
      </Head>
      <div
        style={{
          display: "block",
          textAlign: "center",
          fontSize: "70px",
          fontWeight: "bold",
          margin: "2em",
        }}
      >
        Error 404
        <br />
        <p style={{ fontSize: "26px" }}>
          Return <Link href="/">Home</Link>
        </p>
      </div>
    </>
  );
}
