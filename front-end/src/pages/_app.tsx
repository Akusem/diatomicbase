import "../../styles/globals.css";
import "../../styles/react-table.css";
import "@fontsource/roboto";
import "@fontsource/roboto/700.css";
import "@fontsource/roboto/900.css";

import { AppProps } from "next/app";
import Head from "next/head";
import Layout from "components/Layout";
import { umamiIsConfigured } from "utils/umami";
import { SpeciesContextProvider } from "hooks/useSpeciesContext";

function MyApp({ Component, pageProps }: AppProps) {
  if (!umamiIsConfigured) {
    console.warn("No Umami URL and ID, analytics are deactivated");
  }

  return (
    <>
      <Head>
        {umamiIsConfigured && (
          <script
            async
            defer
            data-do-not-track="true"
            data-website-id={process.env.NEXT_PUBLIC_UMAMI_ID}
            src={process.env.NEXT_PUBLIC_UMAMI_URL}
          ></script>
        )}
        {/* To identify myself as owner on Google Search Console */}
        <meta
          name="google-site-verification"
          content="cytKO9y17P-uvH-bXdvtC7IiDwTFZVSC3CKeejfMBV8"
        />
      </Head>
      <SpeciesContextProvider>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SpeciesContextProvider>
    </>
  );
}

export default MyApp;
