import * as React from "react";
import Head from "next/head";
import styles from "./help.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faQuestionCircle } from "@fortawesome/free-regular-svg-icons";

export default function Help() {
  return (
    <>
      <Head>
        <title>Help DiatOmicBase</title>
      </Head>
      <div className={styles.container}>
        <h3>Access to gene pages</h3>
        You can search genes using any ID or keyword in the general search bar
        located in the website banner. If needed, the search page offers more
        guidelines about expected query format (you can also query the database
        by blast).
        <h3>Gene Pages</h3>
        <p>
          Details about methods and references are available by clicking on{" "}
          <FontAwesomeIcon
            icon={faQuestionCircle}
            style={{ margin: "0 5px", color: "#3cb0ca", fontSize: "1.3em" }}
          />{" "}
          Gene Expression (RNA Seq) On the Differential Expression graph,
          details about comparisons and link to the experiment are available by
          clicking on the corresponding log2FC bar. Unroll “Non significant” and
          “Non expressed” to see all the comparisons performed.
        </p>
        <h3>Genome Browser</h3>
        <div className={styles.genomeBrowser}>
          <strong>Scrolling:</strong>
          <br />
          <p>
            Mouse wheel can scroll side to side, as well as click and drag. The
            pan buttons also exist in the header of the linear genome view
          </p>
          <br />
          <strong>Zooming:</strong>
          <br />{" "}
          <p>
            The zoom buttons exist in the header of the linear genome view, and
            there is also a slider bar to zoom in and out. Note: You can also
            hold the "Ctrl" key and use your mousewheel or trackpad to scroll
            and this will zoom in and out.
          </p>
          <br />
          <strong>Re-ordering tracks</strong>
          <br />
          <p>
            There is a drag handle on the track labels indicating by the six
            dots, clicking and dragging on this part of the track label can
            reorder tracks
          </p>
        </div>
      </div>
    </>
  );
}
