import * as React from "react";
import Head from "next/head";
import styles from "./legalNotice.module.scss";

export default function LegalNotice() {
  return (
    <>
      <Head>
        <title>Legal Notice</title>
      </Head>
      <div className={styles.container}>
        <h2>Legal Notices</h2>
        Published on 19 January 2021 The website diatomicbase.fr is owned
        exclusively by the département de biologie de l’ENS, and respects the
        French and international legislation on copyright and intellectual
        property.
        <h3>Publisher</h3>
        Département de Biologie de l’École normale supérieure 46 rue d’Ulm
        F-75230 Paris cedex 05
        <h3>Editor</h3>
        Pierre Paoletti, Director or the Département de Biologie de l’École
        Normale Supérieure
        <h3>Technical Informations</h3>
        Hosting and management of the website: Service Informatique du
        département de biologie de l’ENS. Accessibility according to Law
        n°2005-102 for equal opportunity, participation and citizenship for
        disabled people.
        <h3>Copyright</h3>
        Content, graphics, and design are property of the département de
        biologie de l’ENS and/or their use has been acquired by a third person.
        A copy, partial or total, is allowed only by explicit agreement of the
        département de biologie de l’ENS. The département de biologie de l’ENS
        agrees to the use of links directing to its website, as long as the
        pages do not open inside another website, but on a separate window. The
        département de biologie de l’ENS updates regularly the website but
        cannot guarantee the accuracy and/or topicality of the information
        published. You can notify the webmaster of any error, out-of-date
        information or broken link: diatomicbase@bio.ens.psl.eu
        <h3>Right of Accessing, Modifying, Correcting and Suppressing Data</h3>
        (Law nº78-17 of January 6 1978, about Informatic, Files and Liberty, or
        "Loi Informatique et Libertés") Every person whose name or picture
        (allowing his/her identification) appears on this website can, at any
        time, ask for the suppression or modification of information relating to
        him/her, by sending an email to the webmaster:
        diatomicbase@bio.ens.psl.eu
        <h3>Rights and Duties of Users</h3>
        The use of this website is subject to French legal dispositions about
        nominative treatments. Users are responsible for the queries they make
        as well as for the interpretation and use they make of results. They
        have to make a use respecting the current reglementations and the
        recommandations of the Commission Nationale de l’Informatique et des
        Libertés (CNIL) when data have a nominative character. Capturing
        nominative information in order to create or enrich another nominative
        treatement (for instance, for a commercial or advertising use) is
        strictly forbidden for all countries.
        <h3>Information on the Personal data protection</h3>
        When browsing the website we collect information about IP address,
        date/time of visit, page visited, browser type, data transferred and
        success of the request. This information is collected to better
        understand how visitors use the DiatOmicBase website, to create summary
        reports, to monitor, protect the security and stability of our website
        resources.
        <h3 id="comments-policy">Comments Policy</h3>
        Comments are moderated: every comment is read before being published and
        the publication of comments is neither instantaneous nor automatic. The
        moderator cannot be held responsible for the comments of users. The
        moderator reserves the right to delete any comment that does not respect
        the rules of this Comments Policy.
        <br />
        In their comments, users undertake to respect the laws and regulations
        in application as well as the rights of individuals. This policy
        considers as illicit (non exhaustive list) :
        <ul>
          <li>
            promoting racial discrimination, revisionism or Holocaust denial;
          </li>
          <li>
            insults (name-calling, rude, aggressive, irreverent remarks...);
          </li>
          <li>
            defamation (imputation of a fact that is prejudicial to the honor or
            consideration of the natural or legal person or body to which the
            fact is imputed);
          </li>
          <li>discriminatory remarks, in all their forms;</li>
          <li>
            copies of private messages or correspondence (violation of the
            secrecy of correspondence);
          </li>
          <li>pedophilia;</li>
          <li>contributions in the nature of advertisements;</li>
        </ul>
        The contributor undertakes to respect these rules of good conduct and
        refrains from :
        <ul>
          <li>
            writing entirely in abbreviated language or in capital letters;
          </li>
          <li>implicate specific persons;</li>
          <li>publish classified ads;</li>
          <li>
            publish off-topic comments (the link with the study of diatoms must
            be obvious);
          </li>
          <li>contribute with a hyperlink to a commercial site;</li>
          <li>
            use the site in an abusive or dishonest manner, without this being
            restrictive, by multiplying messages with the aim of obstructing or
            disrupting the functioning of this space (spamming phenomena)
          </li>
        </ul>
        As a user of this site, you acknowledge having read this moderation
        policy and you agree to respect its rules of good conduct.
        <br />
        <h2>French</h2>
        <h3>Charte des commentaires</h3>
        Les commentaires sont modérés : tout commentaire est lu avant d’être
        publié et la publication des commentaires n’est ni instantanée ni
        automatique. Le modérateur ne saurait être tenu responsable des propos
        des utilisateurs. Le modérateur se garde le droit de supprimer tout
        commentaire qui ne respecterait pas les règles de la présente charte.
        <br />
        Dans leurs commentaires, les utilisateurs s’engagent à respecter les
        lois et règlements en vigueur ainsi que le droit des personnes. Cette
        charte considère comme illicite (liste non exhaustive) :
        <ul>
          <li>
            l’incitation à la haine raciale, au révisionnisme ou au
            négationnisme;
          </li>
          <li>
            les insultes (injures, propos grossiers, agressifs,
            irrévérencieux…);
          </li>
          <li>
            la diffamation (imputation d’un fait portant atteinte à l’honneur ou
            à la considération de la personne physique ou morale, ou du corps
            auquel le fait est imputé);
          </li>
          <li>les propos discriminatoires, sous toutes leurs formes;</li>
          <li>
            les copies de messages privés ou de correspondance (violation du
            secret de la correspondance);
          </li>
          <li>la pédophilie;</li>
          <li>les contributions ayant la nature de publicités;</li>
        </ul>
        Le contributeur s’engage à respecter ces règles de bonnes conduite et
        s’interdit de :
        <ul>
          <li>écrire entièrement en langage abrégé ou en majuscules;</li>
          <li>mettre en cause des personnes particulières;</li>
          <li>publier des petites annonces;</li>
          <li>
            publier des commentaires hors sujet (le lien avec l’étude des
            diatomées doit être évident);
          </li>
          <li>
            contribuer en associant un lien hypertexte renvoyant vers un site
            commercial;
          </li>
          <li>
            utiliser le site de manière abusive ou malhonnête, sans que cela
            soit limitatif, en multipliant les messages dans le but d’entraver
            ou de fausser le fonctionnement de cet espace (phénomènes
            pollupostage);
          </li>
        </ul>
        En tant qu’utilisateur de ce site, vous reconnaissez avoir pris
        connaissance de la présente charte de modération et vous vous engagez à
        respecter ses règles de bonne conduite.
      </div>
    </>
  );
}
