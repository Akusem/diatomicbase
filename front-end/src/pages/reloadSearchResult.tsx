import React, { useEffect } from "react";
import { useRouter } from "next/router";

/**
 * This page is used to fix the bug in #272, when the searchResult page
 * doesn't update when user make a new search.
 * As I wasn't able to reload ReactTable without crash/infiniteloop
 * and that I'm runnning low in time, I get around the problem by
 * forcing the unmount and remount of ReactTable by changing of page.
 * This page will then immediatly redirect to searchResult
 */
export default function ReloadSearchResult() {
  const router = useRouter();
  useEffect(() => {
    let searchTerm = router.query.searchTerm;
    let searchType = router.query.searchType;
    let species = router.query.species;

    // Add species query if present
    let query: any = { searchTerm, searchType };
    if (species) query.species = species;

    router.push({
      pathname: "/searchResult",
      query: query,
    });
  }, [router]);
  return <div></div>;
}
