import React from "react";
import Head from "next/head";
import { GetStaticProps } from "next";
import { useRouter } from "next/router";
import ReactTable from "react-table-v6";
import styles from "./phaeodactylum_tricornutum_resources.module.scss";
import apiUrl from "utils/getApiUrl";
import download from "utils/download";
import LinkArticle from "components/LinkArticle";
import { TranscriptomicsProps } from "pages/transcriptomics";

interface resourcesProps {
  geneSupportingInformationsData: unknown;
  assemblyPhaeoNanoporeData: unknown;
  rnaSeqMetaData: TranscriptomicsProps;
  repetitiveElementsData: unknown;
  transposableElementGff: unknown;
  phatr2AnnotationsData: unknown;
  phatr3AnnotationsData: unknown;
  proteogenomicsData: unknown;
  histoneMarkData: unknown;
  methylationData: unknown;
  smallRNAsData: unknown;
  lincRNAsData: unknown;
  serverDataUrl: string;
  assemblyData: unknown;
  ecotypeData: unknown;
  contigData: unknown;
}

export default function Resources(props: resourcesProps) {
  const {
    geneSupportingInformationsData,
    assemblyPhaeoNanoporeData,
    repetitiveElementsData,
    transposableElementGff,
    phatr2AnnotationsData,
    phatr3AnnotationsData,
    proteogenomicsData,
    histoneMarkData,
    methylationData,
    rnaSeqMetaData,
    serverDataUrl,
    smallRNAsData,
    lincRNAsData,
    assemblyData,
    ecotypeData,
    contigData,
  } = props;

  return (
    <>
      <Head>
        <title>Resources</title>
      </Head>
      <div className={styles.main}>
        <h1 className={styles.speciesName}>Phaeodactylum tricornutum</h1>
        <Assembly
          assemblyData={assemblyData}
          contigData={contigData}
          assemblyPhaeoNanoporeData={assemblyPhaeoNanoporeData}
        />
        <GeneAnnotation
          phatr3AnnotationsData={phatr3AnnotationsData}
          phatr2AnnotationsData={phatr2AnnotationsData}
          proteogenomicsData={proteogenomicsData}
        />
        <TranscriptomicsTable rnaSeqMetaData={rnaSeqMetaData} />
        <Ecotypes ecotypeData={ecotypeData} serverDataUrl={serverDataUrl} />
        <Epigenetics
          histoneMarkData={histoneMarkData}
          methylationData={methylationData}
        />
        <RepetitiveElements
          repetitiveElementsData={repetitiveElementsData}
          transposableElementGff={transposableElementGff}
        />
        <SmallRNAs smallRNAsData={smallRNAsData} />
        <LincRNAs lincRNAsData={lincRNAsData} />
        <GeneSupportingInformations data={geneSupportingInformationsData} />
      </div>
    </>
  );
}

function Assembly({ assemblyData, contigData, assemblyPhaeoNanoporeData }) {
  return (
    <>
      <h2>Assembly</h2>
      <hr />
      <p style={{ textAlign: "justify" }}>
        An international group of researchers in collaboration with DOE's Joint
        Genome Institute has sequenced the genome of <i>P. tricornutum</i> using
        whole genome shotgun (WGS) sequencing. The clone of{" "}
        <i>P. tricornutum</i> that was sequenced is CCAP1055/1 and is available
        from the Culture collection of Algae and Protozoa
        (http://www.ccap.ac.uk). This clone represents a monoclonal culture
        derived from a fusiform cell in May 2003 from strain CCCP632, which was
        originally isolated in 1956 off Blackpool (U.K.). It has been maintained
        in culture continuously in F/2 medium. The 27.4 Mb genome assembly
        contains 33 chromosomes and 55 scaffolds.
      </p>
      <References href="https://doi.org/10.1038/nature07410">
        Bowler C, Allen AE, Badger JH, et al. The Phaeodactylum genome reveals
        the evolutionary history of diatom genomes. Nature. 2008
        Nov;456(7219):239-244.
      </References>
      <br />
      <div className={styles.assemblyColumnContainer}>
        <div>
          <DownloadList items={assemblyData} black />
        </div>
        <div className={styles.assemblyColumn}>
          <DownloadList items={contigData} />
        </div>
      </div>
      <br />
      <p style={{ textAlign: "justify" }}>
        In 2021, Oxford Nanopore Technologies long-read sequencing was used to
        update and validate the quality and contiguity of the P. tricornutum
        genome. Despite repetitive DNA sequences caused problems for current
        genome assembly algorithms, this sequencing allowed to resolve
        previously uncertain genomic regions and further characterize complex
        structural variation.
      </p>
      <References href="https://doi.org/10.1186/s12864-021-07666-3">
        Filloramo, G. V., Curtis, B. A., Blanche, E., & Archibald, J. M. (2021).
        Re-examination of two diatom reference genomes using long-read
        sequencing. BMC genomics, 22(1), 1-25.
      </References>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={assemblyPhaeoNanoporeData}
        black
      />
    </>
  );
}

function References({ children, href, ...otherProps }) {
  return (
    <div {...otherProps}>
      <span className={styles.underline}>Reference:</span>{" "}
      <a href={href}>{children}</a>
    </div>
  );
}

interface dlListProps {
  items: {
    [key: string]: string;
  };
  black?: boolean;
  style?: React.CSSProperties;
  className?: string;
}

function DownloadList(props: dlListProps) {
  const { items, black = false, ...otherProps } = props;
  return (
    <div {...otherProps}>
      {Object.entries(items).map(([name, href], i) => {
        return (
          <p key={i} className={`${styles.dl} ${black && styles.colorBlack}`}>
            <a href={href}> - Download {name} - </a>
          </p>
        );
      })}
    </div>
  );
}

function GeneAnnotation({
  phatr2AnnotationsData,
  phatr3AnnotationsData,
  proteogenomicsData,
}) {
  return (
    <>
      <br />
      <h2>Gene Annotations</h2>
      <hr />
      <h3 className={styles.underline}>Phatr2</h3>
      <div style={{ textAlign: "justify" }}>
        The genome of <i>P. tricornutum</i> was annotated using the JGI
        annotation pipeline, which combines several gene prediction, annotation
        and analysis tools. 10,402 gene models were predicted, 86% of the genes
        are supported by ESTs and 60-65% show homology to proteins in SwissProt.
        <br />
        <References href="http://doi.org/10.1038/nature07410">
          Bowler C, Allen AE, Badger JH, <i>et al.</i> The Phaeodactylum genome
          reveals the evolutionary history of diatom genomes. Nature. 2008
          Nov;456(7219):239-244
        </References>
        <br />
        There are two parts to the <i>P. tricornutum</i> genome sequence
        assembly and annotation reported here: the Phatr2 "finished chromosomes"
        and the Phatr2_bd "unmapped sequence". The finished chromosomes consist
        of the finished genome sequence that could be reliably assembled into
        chromosomes. The "unmapped sequence" consists of assembled scaffolds
        that could neither be mapped to finished chromosomes nor assigned to
        organelles, but that could be aligned to <i>P. tricornutum</i> ESTs
        which were not represented in the finished chromosomes.
      </div>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={phatr2AnnotationsData}
      />
      <h3 className={styles.underline}>Phatr3</h3>
      <div>
        The <i>Phaeodactylum tricornutum</i> genome is a reannotation with
        12,089 gene models predicted by using existing gene models, expression
        data and protein sequences from related species used to train the SNAP
        and Augustus gene prediction programs using the MAKER2 annotation
        pipeline. The inputs were:
        <ul>
          <li>10,402 gene models from a previous genebuild from JGI</li>
          <li>13,828 non-redundant ESTs</li>
          <li>42 libraries of RNA-Seq generated using Illumina technology</li>
          <li>
            49 libraries of RNA-Seq data generated under various iron conditions
            using SoLiD technology{" "}
          </li>
          <li>
            93,206 Bacillariophyta ESTs from dbEST, 22,502 Bacillariophyta and
            118,041 Stramenopiles protein sequences from UniProt.
          </li>
        </ul>
      </div>
      <References href="https://doi.org/10.1038/s41598-018-23106-x">
        Rastogi, A., Maheswari, U., Dorrell, R.G. et al. Integrative analysis of
        large scale transcriptome data draws a comprehensive landscape of
        <i>Phaeodactylum tricornutum</i> genome and evolutionary origin of
        diatoms. Sci Rep 8, 4834 (2018).
      </References>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={phatr3AnnotationsData}
      />
      <br />
      <h3 className={styles.underline}>Proteogenomics pipeline annotation</h3>
      Using mass spectrometry-based proteomics data, approximately 8300 Phatr2
      genes were confirmed, and 606 novel proteins, 506 revised genes, 94 splice
      variants were identified.
      <br />
      <br />
      <References href="https://doi.org/10.1016/j.molp.2018.08.005">
        Yang, M., Lin, X., Liu, X., Zhang, J., & Ge, F. (2018). Genome
        annotation of a model diatom <i>Phaeodactylum tricornutum</i> using an
        integrated proteogenomic pipeline. Molecular plant, 11(10), 1292-1307.
      </References>
      <br />
      <br />
      <DownloadList className={styles.textCenter} items={proteogenomicsData} />
    </>
  );
}

function TranscriptomicsTable(props) {
  const { rnaSeqMetaData } = props;
  const router = useRouter();
  const transcriptomicsDl = (event: React.MouseEvent<HTMLButtonElement>) => {
    router.push({
      pathname: `/resources/transcriptomics/${event.currentTarget.id}`,
    });
  };

  const columns = [
    {
      Header: "BioProject Accession",
      accessor: "BioProject",
      width: 200,
    },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      width: 500,
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => (
        <div className={styles.descriptionRow}>
          <LinkArticle
            publication={props.value.Publication}
            doi={props.value.doi}
          />
        </div>
      ),
    },
    {
      Header: "Link",
      accessor: "BioProject",
      width: 100,
      Cell: (props) => (
        <button
          id={props.value}
          className={styles.dlButton}
          onClick={transcriptomicsDl}
        >
          Download
        </button>
      ),
    },
  ];
  return (
    <>
      <br />
      <h2>Transcriptomics</h2>
      <hr />
      <ReactTable
        className={(styles.table, "-highlight")}
        defaultPageSize={20}
        columns={columns}
        data={rnaSeqMetaData}
      />
    </>
  );
}

function Ecotypes({ ecotypeData, serverDataUrl }) {
  function vcfDL(ecotype_name: string) {
    const url = `${serverDataUrl}/phaeodactylum_tricornutum/Ecotypes/${ecotype_name}.vcf.gz`;
    download(url);
  }

  const columns = [
    {
      Header: "Accession",
      accessor: "accession_name",
      width: 200,
    },
    {
      Header: "Strain reference in permanent collections",
      accessor: "strain_reference_in_permanent_collections",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      Header: "Characteristics of collection site",
      accessor: "characteristics_of_collection_site",
      classname: styles.descriptionRow,
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      Header: "Major morphotype",
      accessor: "major_morphotype",
    },
    {
      Header: "Accession numbers of characterized strains",
      accessor: "accession_numbers_of_characterized_strains",
    },
    {
      Header: "Download",
      id: "download",
      accessor: (row) => {
        return {
          accession: row.accession_name,
          run: row.run_accession,
        };
      },
      width: 200,
      Cell: (props) => (
        <div className={styles.dlColumnLine}>
          <button
            id={props.value.accession}
            className={styles.dlButton}
            onClick={() => vcfDL(props.value.accession)}
          >
            VCF
          </button>
          <button className={styles.dlButton}>
            <a
              href={`https://trace.ncbi.nlm.nih.gov/Traces/sra/?run=${props.value.run}`}
              target="_blank"
              rel="noreferrer noopener"
            >
              SRA
            </a>
          </button>
        </div>
      ),
    },
  ];

  return (
    <>
      <br />
      <br />
      <h2>Characterized Ecotypes</h2>
      <hr />
      <br />
      <ReactTable
        className={(styles.table, "-highlight")}
        defaultPageSize={10}
        columns={columns}
        data={ecotypeData}
      />
      <br />
      <div style={{ marginLeft: "1em" }}>
        <span className={styles.underline}>References: </span>
        <ul>
          <li>
            Phenotypic caracterisation -{" "}
            <a href="https://doi.org/10.1111/j.1529-8817.2007.00384.x">
              Martino, A. D., Meichenin, A., Shi, J., Pan, K., & Bowler, C.
              (2007). Genetic and phenotypic characterization of Phaeodactylum
              tricornutum (Bacillariophyceae) accessions 1. Journal of
              Phycology, 43(5), 992-1009
            </a>
          </li>
          <li>
            VCF files -{" "}
            <a href="https://doi.org/10.1038/s41396-019-0528-3">
              Rastogi, A., Vieira, F. R. J., Deton-Cabanillas, A. F., Veluchamy,
              A., Cantrel, C., Wang, G., ... & Tirichine, L. (2019). A genomics
              approach reveals the global genetic polymorphism, structure, and
              functional diversity of ten accessions of the marine model diatom
              <i>Phaeodactylum tricornutum</i>. The ISME journal, 14(2),
              347-363.
            </a>
          </li>
        </ul>
      </div>
    </>
  );
}

function Epigenetics({ histoneMarkData, methylationData }) {
  return (
    <div>
      <br />
      <h2>Epigenetics</h2>
      <hr />
      <div style={{ marginLeft: "1em" }}>
        <h3 className={styles.underline}>Methylome:</h3>
        The first whole-genome methylome has been obtained by digestion with the
        methyl-sensitive endonuclease McrBC followed by hybridization to
        McrBc-chip tiling array of the <i>P. tricornutum</i> genome. Next
        bisulfite deep sequencing has been used to compare DNA methylation in
        low and replete nitrogen conditions.
        <br />
        <br />
        <span className={styles.underline}>References: </span>
        <ul>
          <li>
            McrBc chip -{" "}
            <a href="https://doi.org/10.1038/ncomms3091">
              Veluchamy, A., Lin, X., Maumus, F., Rivarola, M., Bhavsar, J.,
              Creasy, T., ... & Tirichine, L. (2013). Insights into the role of
              DNA methylation in diatoms by genome-wide profiling in
              <i>Phaeodactylum tricornutum</i>. Nature communications, 4(1),
              1-10.
            </a>
          </li>
          <li>
            LowN and repleteN -{" "}
            <a href="https://doi.org/10.1186/s13059-015-0671-8">
              Veluchamy, A., Rastogi, A., Lin, X., Lombard, B., Murik, O.,
              Thomas, Y., ... & Tirichine, L. (2015). An integrative analysis of
              post-translational histone modifications in the marine diatom
              <i>Phaeodactylum tricornutum</i>. Genome biology, 16(1), 1-18
            </a>
          </li>
        </ul>
        <DownloadList className={styles.textCenter} items={methylationData} />
        <div>
          <h3 className={styles.underline}>Histone marks:</h3>
          5 histone PTMs were identified using mass spectrometry: H3K4me2,
          H3K9me2, H3K27me3, H4K91Ac ans H3K9me3. Three were used to compare
          histone marks in low and replete nitrogen conditions.
          <br />
          <br />
          <References href="https://doi.org/10.1186/s13059-015-0671-8">
            Veluchamy, A., Rastogi, A., Lin, X., Lombard, B., Murik, O., Thomas,
            Y., ... & Tirichine, L. (2015). An integrative analysis of
            post-translational histone modifications in the marine diatom
            <i>Phaeodactylum tricornutum</i>. Genome biology, 16(1), 1-18
          </References>
          <br />
          <DownloadList className={styles.textCenter} items={histoneMarkData} />
        </div>
      </div>
    </div>
  );
}

function RepetitiveElements({
  repetitiveElementsData,
  transposableElementGff,
}) {
  const columns = [
    {
      Header: "Code",
      accessor: "code",
    },
    {
      Header: "Number",
      accessor: "number",
    },
    {
      Header: "Type",
      accessor: "type",
    },
    {
      Header: "Class",
      accessor: "class",
    },
    {
      Header: "Order",
      accessor: "order",
    },
    {
      Header: "Superfamily",
      accessor: "superfamily",
    },
  ];

  return (
    <>
      <br />
      <h2>Repetitive Elements</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        Repeats were collectively found to contribute ~3.4Mb (12%) of the
        assembly, including transposable elements (TEs), unclassified and tandem
        repeats, as well as fragments of host genes.
        <br />
        <br />
        <References href="https://doi.org/10.1038/s41598-018-23106-x">
          Rastogi, A., Maheswari, U., Dorrell, R.G. et al. Integrative analysis
          of large scale transcriptome data draws a comprehensive landscape of
          <i>Phaeodactylum tricornutum</i> genome and evolutionary origin of
          diatoms. Sci Rep 8, 4834 (2018).
        </References>
      </div>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={transposableElementGff}
      />
      <br />
      <br />
      <ReactTable
        className={(styles.table, "-highlight")}
        defaultPageSize={10}
        columns={columns}
        data={repetitiveElementsData}
      />
    </>
  );
}

function SmallRNAs({ smallRNAsData }) {
  return (
    <div>
      <br />
      <h2>small RNAs</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        Five small RNA libraries within the 18 to 36 nt RNA fraction were
        extracted from cells grown under different conditions: Normal Light
        (NL), High Light (HL), Low Light (LL), Dark (D), and Iron starvation
        (−Fe).
        <br />
        <br />
        <References href="https://doi.org/10.1186/1471-2164-15-698">
          Rogato, A., Richard, H., Sarazin, A., Voss, B., Navarro, S. C.,
          Champeimont, R., ... & Falciatore, A. (2014). The diversity of small
          non-coding RNAs in the diatom <i>Phaeodactylum tricornutum</i>. BMC
          genomics, 15(1), 1-20.
        </References>
        <br />
        <DownloadList className={styles.textCenter} items={smallRNAsData} />
      </div>
    </div>
  );
}

function LincRNAs({ lincRNAsData }) {
  return (
    <>
      <br />
      <h2>lincRNAs</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        In this study related to responses to P depletion, long intergenic
        nonprotein coding RNAs (lincRNAs) were defined as sequences with a
        length of ≥ 200 nucleotides and a predicted open reading frame (ORF) of
        ≤ 100 amino acids.
        <br />
        <br />
        <References href="https://doi.org/10.1111/nph.13787">
          Cruz de Carvalho, M. H., Sun, H. X., Bowler, C., & Chua, N. H. (2016).
          Noncoding and coding transcriptome responses of a marine diatom to
          phosphate fluctuations. New Phytologist, 210(2), 497-510
        </References>
        <br />
        <DownloadList className={styles.textCenter} items={lincRNAsData} />
      </div>
    </>
  );
}

function GeneSupportingInformations({ data }) {
  return (
    <>
      <br />
      <h2>Gene supporting informations</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        In the file below you will find Supporting informations for the Phatr3
        genes: corresponding ID (Phatr2, NCBI...), KEGG, GO, Domains, Targeting
        predictions, Evolutionary origins.
        <br />
        <br />
        <References href="https://dx.doi.org/10.3389%2Ffpls.2020.590949">
          Ait-Mohamed, O., Vanclová, A. M. N., Joli, N., Liang, Y., Zhao, X.,
          Genovesio, A., ... & Dorrell, R. G. (2020). PhaeoNet: A holistic
          RNAseq-based portrait of transcriptional coordination in the model
          diatom <i>Phaeodactylum tricornutum</i>. Frontiers in Plant Science,
          11.
        </References>
        <br />
        <DownloadList className={styles.textCenter} items={data} />
      </div>
    </>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  // Retrieve name and href of file from backend
  // As the page is build on backend, apiUrl will correspond to docker URL (if using Docker).
  // To avoid having href to docker URL, we retrieve from the API the correct path to the data server.
  // Remenber that the server handling files can be different than the API.
  // We also retrieve data from table like ecotype, transcriptomics, and repetitive_elements
  const serverDataUrlResult = await fetch(
    `${apiUrl}/resources/SERVER_DATA_URL`
  );
  const ecotypeResult = await fetch(`${apiUrl}/resources/ecotype`);
  const transposonsResult = await fetch(
    `${apiUrl}/resources/repetitive_elements`
  );
  const transcriptomicsResult = await fetch(
    `${apiUrl}/resources/transcriptomics?species=Phaeodactylum tricornutum&forResourcesPage=true`
  );

  let serverDataUrl = await serverDataUrlResult.json();
  serverDataUrl = serverDataUrl.url;
  const repetitiveElementsData = await transposonsResult.json();
  const rnaSeqMetaData = await transcriptomicsResult.json();
  const ecotypeData = await ecotypeResult.json();

  const assemblyData = {
    "all genomic content": `${serverDataUrl}/phaeodactylum_tricornutum/Tracks/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz`,
    chloroplast: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.chloroplast.fa.gz`,
    mitochondrion: `${serverDataUrl}/phaeodactylum_tricornutum/resources/phatr_mitochondrion.fna`,
    "bottom drawer": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.nonchromosomal.fa.gz`,
  };

  const contigData = {
    "Contig 1	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.1.fa.gz`,
    "Contig 2	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.2.fa.gz`,
    "Contig 3	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.3.fa.gz`,
    "Contig 4	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.4.fa.gz`,
    "Contig 5	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.5.fa.gz`,
    "Contig 6	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.6.fa.gz`,
    "Contig 7	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.7.fa.gz`,
    "Contig 8	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.8.fa.gz`,
    "Contig 9	": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.9.fa.gz`,
    "Contig 10": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.10.fa.gz`,
    "Contig 11": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.11.fa.gz`,
    "Contig 12": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.12.fa.gz`,
    "Contig 13": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.13.fa.gz`,
    "Contig 14": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.14.fa.gz`,
    "Contig 15": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.15.fa.gz`,
    "Contig 16": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.16.fa.gz`,
    "Contig 17": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.17.fa.gz`,
    "Contig 18": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.18.fa.gz`,
    "Contig 19": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.19.fa.gz`,
    "Contig 20": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.20.fa.gz`,
    "Contig 21": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.21.fa.gz`,
    "Contig 22": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.22.fa.gz`,
    "Contig 23": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.23.fa.gz`,
    "Contig 24": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.24.fa.gz`,
    "Contig 25": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.25.fa.gz`,
    "Contig 26": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.26.fa.gz`,
    "Contig 27": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.27.fa.gz`,
    "Contig 28": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.28.fa.gz`,
    "Contig 29": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.29.fa.gz`,
    "Contig 30": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.30.fa.gz`,
    "Contig 31": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.31.fa.gz`,
    "Contig 32": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.32.fa.gz`,
    "Contig 33": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.chromosome.33.fa.gz`,
  };

  const assemblyPhaeoNanoporeData = {
    "All genomic content": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum_CANU_assembly.fasta.gz`,
    Chloroplast: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum_CANU_chloroplast_contig.fasta.gz`,
    Mitochondria: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum_CANU_mitochondria_contig.fasta.gz`,
  };

  const phatr2AnnotationsData = {
    "Filtered Gene models": `${serverDataUrl}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_filtered.gff3.gz`,
    "All Gene models": `${serverDataUrl}/phaeodactylum_tricornutum/Tracks/phatr2_gene_models_unfiltered.gff3.gz`,
    "Transcripts sequence": `${serverDataUrl}/phaeodactylum_tricornutum/resources/phatr2_all_genes.fna.gz`,
    "Proteins sequence": `${serverDataUrl}/phaeodactylum_tricornutum/resources/phatr2_all_genes.faa.gz`,
  };

  const phatr3AnnotationsData = {
    " all genomic content": `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.52.gff3.gz`,
    CDNA: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.cdna.all.fa.gz`,
    CDS: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.cds.all.fa.gz`,
    DNA: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.toplevel.fa.gz`,
    NCRNA: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.ncrna.fa.gz`,
    PEP: `${serverDataUrl}/phaeodactylum_tricornutum/resources/Phaeodactylum_tricornutum.ASM15095v2.pep.all.fa.gz`,
  };

  const histoneMarkData = {
    "H3k4me2 Low Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K4me2_lowN.bw`,
    "H3k4me2 Normal Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K4me2_NormalN.bw`,
    "H3k914Ac Low Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K914Ac_LowN.bw`,
    "H3k914Ac Normal Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K914Ac_NormalN.bw`,
    "H3k9me3 Low Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K9me3_lowN.bw`,
    "H3k9me3 Normal Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K9me3_NormalN.bw`,
    "H3k9me2 Normal Nitrate": `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K9me2_NormalN.bw`,
    H3k27me3: `${serverDataUrl}/phaeodactylum_tricornutum/Histone_Mark/H3K27me3.bw`,
  };

  const transposableElementGff = {
    "Transposable Elements GFF": `${serverDataUrl}/phaeodactylum_tricornutum/Tracks/phatr3_repetitive_sequences.gff3.gz`,
  };

  const smallRNAsData = {
    "Dark condition (.bw)": `${serverDataUrl}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Dark.bw`,
    "Low Light condition (.bw)": `${serverDataUrl}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Low_Light.bw`,
    "Normal Light condition (.bw)": `${serverDataUrl}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Normal_Light.bw`,
    "High Light condition (.bw)": `${serverDataUrl}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_High_Light.bw`,
    "Iron Starvation condition (.bw)": `${serverDataUrl}/phaeodactylum_tricornutum/smallRNAs/smallRNAs_Iron_Starvation.bw`,
  };

  const methylationData = {
    McrBC: `${serverDataUrl}/phaeodactylum_tricornutum/Methylation/McrBC_methylation.bw`,
    "Replete Nitrate BIS": `${serverDataUrl}/phaeodactylum_tricornutum/Methylation/RepleteN_BIS_methylation.bw`,
    "Low Nitrate BIS": `${serverDataUrl}/phaeodactylum_tricornutum/Methylation/LowN_BIS_methylation.bw`,
  };

  const lincRNAsData = {
    "GTF file": `${serverDataUrl}/phaeodactylum_tricornutum/Tracks/phatr_lincRNAs.gtf.gz`,
  };

  const proteogenomicsData = {
    "Gene models (.gff)": `${serverDataUrl}/phaeodactylum_tricornutum/Proteogenomics/phatr_proteogenomics_Yang.gff3.gz`,
  };

  const geneSupportingInformationsData = {
    "Gene Supporting Informations": `${serverDataUrl}/phaeodactylum_tricornutum/resources/phatr3_gene_supp_info.tsv`,
  };

  return {
    props: {
      geneSupportingInformationsData: geneSupportingInformationsData,
      assemblyPhaeoNanoporeData: assemblyPhaeoNanoporeData,
      repetitiveElementsData: repetitiveElementsData,
      transposableElementGff: transposableElementGff,
      phatr2AnnotationsData: phatr2AnnotationsData,
      phatr3AnnotationsData: phatr3AnnotationsData,
      proteogenomicsData: proteogenomicsData,
      histoneMarkData: histoneMarkData,
      methylationData: methylationData,
      rnaSeqMetaData: rnaSeqMetaData,
      smallRNAsData: smallRNAsData,
      serverDataUrl: serverDataUrl,
      assemblyData: assemblyData,
      lincRNAsData: lincRNAsData,
      ecotypeData: ecotypeData,
      contigData: contigData,
    },
  };
};
