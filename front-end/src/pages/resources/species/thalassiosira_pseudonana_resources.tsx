import React from "react";
import Head from "next/head";
import { GetStaticProps } from "next";
import { useRouter } from "next/router";
import ReactTable from "react-table-v6";
import styles from "./thalassiosira_pseudonana_resources.module.scss";
import apiUrl from "utils/getApiUrl";
import download from "utils/download";
import Button from "components/Button";
import LinkArticle from "components/LinkArticle";
import { TranscriptomicsProps } from "pages/transcriptomics";

interface resourcesProps {
  geneSupportingInformationsData: unknown;
  assemblyPhaeoNanoporeData: unknown;
  rnaSeqMetaData: TranscriptomicsProps;
  repetitiveElementsData: unknown;
  phatr2AnnotationsData: unknown;
  phatr3AnnotationsData: unknown;
  proteogenomicsData: unknown;
  histoneMarkData: unknown;
  methylationData: unknown;
  smallRNAsData: unknown;
  lincRNAsData: unknown;
  serverDataUrl: string;
  assemblyData: unknown;
  ecotypeData: unknown;
  contigData: unknown;
}

export default function Resources(props: resourcesProps) {
  const {
    assemblyPhaeoNanoporeData,
    repetitiveElementsData,
    methylationData,
    rnaSeqMetaData,
    smallRNAsData,
    assemblyData,
  } = props;

  return (
    <>
      <Head>
        <title>Resources</title>
      </Head>
      <div className={styles.main}>
        <h1 className={styles.speciesName}>Thalassiosira pseudonana</h1>
        <Assembly
          assemblyData={assemblyData}
          assemblyPhaeoNanoporeData={assemblyPhaeoNanoporeData}
        />
        <TranscriptomicsTable rnaSeqMetaData={rnaSeqMetaData} />
        <Epigenetics methylationData={methylationData} />
        <RepetitiveElements repetitiveElementsData={repetitiveElementsData} />
        <SmallRNAs smallRNAsData={smallRNAsData} />
      </div>
    </>
  );
}

function Assembly({ assemblyData, assemblyPhaeoNanoporeData }) {
  return (
    <>
      <h2>Assembly</h2>
      <hr />
      <p style={{ textAlign: "justify" }}>
        The Thalassiosira pseudonana CCMP 1335 genome is approximately 34 Mb in
        size. The clone of <i>P. pseudonana</i> that was sequenced is CCMP1335
        and is available from the Center for Culture of Marine Phytoplankton
        (http://ccmp.bigelow.org). This clone was collected in 1958 from
        Moriches Bay (Long Island, New York) and has been maintained
        continuously in culture. The Joint Genome Institute has sequenced the
        nuclear genome, under the accession GCA_000149405.2 or ASM14940v2, as
        well as the plastid and mitochondrial genomes of this unicellular algae.
        The 32.4 Mb genome assembly contains 24 chromosomes and 64 scaffolds.
        The Thaps genome was annotated using the JGI annotation pipeline, which
        combines several gene predictions, annotation and analysis tools.
      </p>
      <References href="http://doi.org/10.1126/science.1101156">
        Armbrust, E. V., Berges, J. A., Bowler, C., Green, B. R., Martinez, D.,
        Putnam, N. H., ... & Rokhsar, D. S. (2004). The genome of the diatom
        Thalassiosira pseudonana: ecology, evolution, and metabolism. Science,
        306(5693), 79-86.
      </References>
      <br />
      <References href="https://doi.org/10.1038/nature07410">
        Bowler, C., Allen, A. E., Badger, J. H., Grimwood, J., Jabbari, K., Kuo,
        A., ... & Grigoriev, I. V. (2008). The Phaeodactylum genome reveals the
        evolutionary history of diatom genomes. Nature, 456(7219), 239-244.
      </References>
      <br />
      <DownloadList className={styles.textCenter} items={assemblyData} black />
      <br />
      <p style={{ textAlign: "justify" }}>
        Recently, T. pseudonana genome has been resequenced using Oxford
        Nanopore Technologies long-read sequencing to resolve previously
        uncertain genomic regions, further characterize complex structural
        variation, and re-evaluate the repetitive DNA content. The 33.8 Mb
        genome assembly contains 52 contigs.
      </p>
      <References href="https://doi.org/10.1186/s12864-021-07666-3">
        Filloramo, G. V., Curtis, B. A., Blanche, E., & Archibald, J. M. (2021).
        Re-examination of two diatom reference genomes using long-read
        sequencing. BMC genomics, 22(1), 1-25.
      </References>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={assemblyPhaeoNanoporeData}
        black
      />
    </>
  );
}

function References({ children, href, ...otherProps }) {
  return (
    <div {...otherProps}>
      <span className={styles.underline}>Reference:</span>{" "}
      <a href={href}>{children}</a>
    </div>
  );
}

interface dlListProps {
  items: {
    [key: string]: string;
  };
  black?: boolean;
  style?: React.CSSProperties;
  className?: string;
}

function DownloadList(props: dlListProps) {
  const { items, black = false, ...otherProps } = props;
  return (
    <div {...otherProps}>
      {Object.entries(items).map(([name, href], i) => {
        return (
          <p key={i} className={`${styles.dl} ${black && styles.colorBlack}`}>
            <a href={href}> - Download {name} - </a>
          </p>
        );
      })}
    </div>
  );
}

function GeneAnnotation({
  phatr2AnnotationsData,
  phatr3AnnotationsData,
  proteogenomicsData,
}) {
  return (
    <>
      <br />
      <h2>Gene Annotations</h2>
      <hr />
      <h3 className={styles.underline}>Phatr2</h3>
      <div style={{ textAlign: "justify" }}>
        The genome of <i>P. tricornutum</i> was annotated using the JGI
        annotation pipeline, which combines several gene prediction, annotation
        and analysis tools. 10,402 gene models were predicted, 86% of the genes
        are supported by ESTs and 60-65% show homology to proteins in SwissProt.
        <br />
        <References href="http://doi.org/10.1038/nature07410">
          Bowler C, Allen AE, Badger JH, <i>et al.</i> The Phaeodactylum genome
          reveals the evolutionary history of diatom genomes. Nature. 2008
          Nov;456(7219):239-244
        </References>
        <br />
        There are two parts to the <i>P. tricornutum</i> genome sequence
        assembly and annotation reported here: the Phatr2 "finished chromosomes"
        and the Phatr2_bd "unmapped sequence". The finished chromosomes consist
        of the finished genome sequence that could be reliably assembled into
        chromosomes. The "unmapped sequence" consists of assembled scaffolds
        that could neither be mapped to finished chromosomes nor assigned to
        organelles, but that could be aligned to <i>P. tricornutum</i> ESTs
        which were not represented in the finished chromosomes.
      </div>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={phatr2AnnotationsData}
      />
      <h3 className={styles.underline}>Phatr3</h3>
      <div>
        The <i>Phaeodactylum tricornutum</i> genome is a reannotation with
        12,089 gene models predicted by using existing gene models, expression
        data and protein sequences from related species used to train the SNAP
        and Augustus gene prediction programs using the MAKER2 annotation
        pipeline. The inputs were:
        <ul>
          <li>10,402 gene models from a previous genebuild from JGI</li>
          <li>13,828 non-redundant ESTs</li>
          <li>42 libraries of RNA-Seq generated using Illumina technology</li>
          <li>
            49 libraries of RNA-Seq data generated under various iron conditions
            using SoLiD technology{" "}
          </li>
          <li>
            93,206 Bacillariophyta ESTs from dbEST, 22,502 Bacillariophyta and
            118,041 Stramenopiles protein sequences from UniProt.
          </li>
        </ul>
      </div>
      <References href="https://doi.org/10.1038/s41598-018-23106-x">
        Rastogi, A., Maheswari, U., Dorrell, R.G. et al. Integrative analysis of
        large scale transcriptome data draws a comprehensive landscape of
        <i>Phaeodactylum tricornutum</i> genome and evolutionary origin of
        diatoms. Sci Rep 8, 4834 (2018).
      </References>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={phatr3AnnotationsData}
      />
      <br />
      <h3 className={styles.underline}>Proteogenomics pipeline annotation</h3>
      Using mass spectrometry-based proteomics data, approximately 8300 Phatr2
      genes were confirmed, and 606 novel proteins, 506 revised genes, 94 splice
      variants were identified.
      <br />
      <br />
      <References href="https://doi.org/10.1016/j.molp.2018.08.005">
        Yang, M., Lin, X., Liu, X., Zhang, J., & Ge, F. (2018). Genome
        annotation of a model diatom <i>Phaeodactylum tricornutum</i> using an
        integrated proteogenomic pipeline. Molecular plant, 11(10), 1292-1307.
      </References>
      <br />
      <br />
      <DownloadList className={styles.textCenter} items={proteogenomicsData} />
    </>
  );
}

function TranscriptomicsTable(props) {
  const { rnaSeqMetaData } = props;
  const router = useRouter();
  const transcriptomicsDl = (event: React.MouseEvent<HTMLButtonElement>) => {
    router.push({
      pathname: `/resources/transcriptomics/${event.currentTarget.id}`,
    });
  };

  const columns = [
    {
      Header: "BioProject Accession",
      accessor: "BioProject",
      width: 200,
    },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow}>{props.value}</div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      width: 500,
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => (
        <div className={styles.descriptionRow}>
          <LinkArticle
            publication={props.value.Publication}
            doi={props.value.doi}
          />
        </div>
      ),
    },
    {
      Header: "Link",
      accessor: "BioProject",
      width: 100,
      Cell: (props) => (
        <button
          id={props.value}
          className={styles.dlButton}
          onClick={transcriptomicsDl}
        >
          Download
        </button>
      ),
    },
  ];
  return (
    <>
      <br />
      <h2>Transcriptomics</h2>
      <hr />
      <ReactTable
        className={(styles.table, "-highlight")}
        defaultPageSize={10}
        columns={columns}
        data={rnaSeqMetaData}
      />
    </>
  );
}

function Epigenetics({ methylationData }) {
  return (
    <div>
      <br />
      <h2>Epigenetics</h2>
      <hr />
      <div style={{ marginLeft: "1em" }}>
        <h3 className={styles.underline}>Methylome:</h3>
        Assaying cytosine methylation from genomic DNA by bisulfite sequencing
        <br />
        <br />
        <References href="https://doi.org/10.1016/j.cell.2014.01.029">
          Huff, J. T., & Zilberman, D. (2014). Dnmt1-independent CG methylation
          contributes to nucleosome positioning in diverse eukaryotes. Cell,
          156(6), 1286-1297.
        </References>
        <br />
        <DownloadList className={styles.textCenter} items={methylationData} />
      </div>
    </div>
  );
}

function RepetitiveElements({ repetitiveElementsData }) {
  return (
    <>
      <br />
      <h2>Repetitive Elements</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        Repeats were collectively found to contribute ~3.4 Mb (12%) of the
        assembly, including transposable elements (TEs), unclassified and tandem
        repeats, as well as fragments of host genes.
        <br />
        <br />
        <References href="https://doi.org/10.1038/s41598-018-23106-x">
          Rastogi, A., Maheswari, U., Dorrell, R.G. et al. Integrative analysis
          of large scale transcriptome data draws a comprehensive landscape of
          <i>Phaeodactylum tricornutum</i> genome and evolutionary origin of
          diatoms. Sci Rep 8, 4834 (2018).
        </References>
      </div>
      <br />
      <DownloadList
        className={styles.textCenter}
        items={repetitiveElementsData}
      />
    </>
  );
}

function SmallRNAs({ smallRNAsData }) {
  return (
    <div>
      <br />
      <h2>small RNAs</h2>
      <hr />
      <br />
      <div style={{ marginLeft: "1em" }}>
        Sequencing short RNA transcriptomes provides first experimental evidence
        for the existence of short non-coding RNAs and absence of canonical
        miRNAs. However, the group of tRNA-derived sRNAs seems to be very
        prominent in diatoms.
        <br />
        <br />
        <References href="https://doi.org/10.1186/1471-2164-15-697">
          Lopez-Gomollon, S., Beckers, M., Rathjen, T., Moxon, S., Maumus, F.,
          Mohorianu, I., ... & Mock, T. (2014). Global discovery and
          characterization of small non-coding RNAs in marine microalgae. BMC
          genomics, 15(1), 1-13.
        </References>
        <br />
        <DownloadList className={styles.textCenter} items={smallRNAsData} />
        <br />
        <div className={styles.buttonCenter}>
          <Button
            style={{ marginLeft: "2em" }}
            onClick={() =>
              download(
                "https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE57987"
              )
            }
          >
            Access smallRNA GEO
          </Button>
        </div>
      </div>
    </div>
  );
}

export const getStaticProps: GetStaticProps = async () => {
  // Retrieve name and href of file from backend
  // As the page is build on backend, apiUrl will correspond to docker URL (if using Docker).
  // To avoid having href to docker URL, we retrieve from the API the correct path to the data server.
  // Remenber that the server handling files can be different than the API.
  // We also retrieve data from table like ecotype, transcriptomics, and repetitive_elements
  const serverDataUrlResult = await fetch(
    `${apiUrl}/resources/SERVER_DATA_URL`
  );
  const ecotypeResult = await fetch(`${apiUrl}/resources/ecotype`);
  const transposonsResult = await fetch(
    `${apiUrl}/resources/repetitive_elements`
  );
  const transcriptomicsResult = await fetch(
    `${apiUrl}/resources/transcriptomics?species=Thalassiosira pseudonana&forResourcesPage=true`
  );

  let serverDataUrl = await serverDataUrlResult.json();
  serverDataUrl = serverDataUrl.url;
  const rnaSeqMetaData = await transcriptomicsResult.json();
  const ecotypeData = await ecotypeResult.json();
  const assemblyData = {
    "all genomic content": `${serverDataUrl}/thalassiosira_pseudonana/Tracks/Thalassiosira_pseudonana.ASM14940v2.dna.toplevel.fa.gz`,
    chloroplast: `${serverDataUrl}/thalassiosira_pseudonana/resources/Thalassiosira_pseudonana_chloroplast.fa.gz`,
    mitochondrion: `${serverDataUrl}/thalassiosira_pseudonana/resources/Thalassiosira_pseudonana_mitochondria.fa.gz`,
    // "bottom drawer": `${serverDataUrl}/resources/Phaeodactylum_tricornutum.ASM15095v2.dna.nonchromosomal.fa.gz`,
  };

  const assemblyPhaeoNanoporeData = {
    "All genomic content": `${serverDataUrl}/thalassiosira_pseudonana/resources/Thal_pseudonana_FLYE_assembly.fasta.gz`,
    Chloroplast: `${serverDataUrl}/thalassiosira_pseudonana/resources/Thal_pseudonana_FLYE_chloroplast_contigs.fasta.gz`,
    Mitochondria: `${serverDataUrl}/thalassiosira_pseudonana/resources/Thal_pseudonano_FLYE_mitochondria_contig_73.fasta.gz`,
  };

  const methylationData = {
    "Tpse BSseq CG (.gff)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CG.gff.gz`,
    "Tpse BSseq CG (.bw)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CG.bw`,
    "Tpse BSseq CHG (.gff)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHG.gff.gz`,
    "Tpse BSseq CHG (.bw)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHG.bw`,
    "Tpse BSseq CHH (.gff)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHH.gff.gz`,
    "Tpse BSseq CHH (.bw)": `${serverDataUrl}/thalassiosira_pseudonana/Methylation/GSM1134628_Tpse_BSseq_CHH.bw`,
  };

  const repetitiveElementsData = {
    "LTR-RTs (.csv)": `${serverDataUrl}/thalassiosira_pseudonana/resources/LTR_RTs.csv`,
  };

  const smallRNAsData = {
    "smallRNA (raw data)": `${serverDataUrl}/thalassiosira_pseudonana/smallRNAs/GSE57987_RAW.tar`,
    "smallRNA (.gff)": `${serverDataUrl}/thalassiosira_pseudonana/smallRNAs/GSM1399245_smallRNA.gff.gz`,
  };

  return {
    props: {
      assemblyPhaeoNanoporeData: assemblyPhaeoNanoporeData,
      repetitiveElementsData: repetitiveElementsData,
      methylationData: methylationData,
      rnaSeqMetaData: rnaSeqMetaData,
      smallRNAsData: smallRNAsData,
      assemblyData: assemblyData,
      ecotypeData: ecotypeData,
    },
  };
};
