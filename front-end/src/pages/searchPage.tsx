import React, { useState } from "react";
import Link from "next/link";
import Head from "next/head";
import { useRouter, NextRouter } from "next/router";
import apiUrl from "utils/getApiUrl";
import styles from "./searchPage.module.scss";
import umamiEvent from "utils/umami";
import Description from "components/Description";
import SpeciesDropSelector from "components/SpeciesDropSelector";
import useSearch, { typeSearchType } from "hooks/useSearch";
import { useIsGlobalSpecies } from "hooks/useSpeciesContext";

export default function SearchPage() {
  const router = useRouter();
  const [localSpecies, setLocalSpecies] = useState();
  const globalSpeciesIsSelected = useIsGlobalSpecies();

  return (
    <>
      <Head>
        <title>Gene Page</title>
      </Head>
      <div className={styles.container}>
        <Description>
          On each gene page you will find: cDNA and protein sequences, a genome
          browser (with gene models, ecotype variants, histone marks, non-coding
          RNAs), comparisons of gene expression in public RNA-Seq datasets,
          co-expression networks as well as several domains annotation.
        </Description>
        <div className={styles.column}>
          {!globalSpeciesIsSelected && (
            <SpeciesSelector
              localSpecies={localSpecies}
              setLocalSpecies={setLocalSpecies}
            />
          )}
          <SearchTextField
            label="Gene ID"
            term="gene"
            placeholder="e.g. Phatr3_EG02276"
            species={localSpecies}
            router={router}
            example={
              <div>
                E.g.{" "}
                <Link href="/searchResult?searchTerm=Phatr3_EG02276&searchType=gene">
                  Phatr3_EG02276
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=J23&searchType=gene">
                  J23
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=e_gw1.14.49.1&searchType=gene">
                  e_gw1.14.49.1
                </Link>
              </div>
            }
          />
          <SearchTextField
            label="Gene Ontology"
            term="go"
            placeholder="e.g. GO:0050897"
            species={localSpecies}
            router={router}
            example={
              <div>
                E.g.{" "}
                <Link href="/searchResult?searchTerm=GO%3A0006406&searchType=go">
                  GO:0006406
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=nitrate+assimilation&searchType=go">
                  nitrate assimilation
                </Link>
              </div>
            }
          />
          <SearchTextField
            label="KEGG ID"
            term="kegg"
            placeholder="e.g. Ko00650"
            species={localSpecies}
            router={router}
            example={
              <div>
                E.g.{" "}
                <Link href="/searchResult?searchTerm=ko00650&searchType=kegg">
                  ko00650
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=500&searchType=kegg">
                  500
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=Thiamine&searchType=kegg">
                  Thiamine
                </Link>
              </div>
            }
          />
          <SearchTextField
            label="Keyword"
            term="keyword"
            placeholder="e.g. Hydrogenase"
            species={localSpecies}
            router={router}
            example={
              <div>
                E.g.{" "}
                <Link href="/searchResult?searchTerm=dehydrogenase&searchType=keyword">
                  dehydrogenase
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=Ferritin&searchType=keyword">
                  Ferritin
                </Link>
              </div>
            }
          />
          <SearchTextField
            label="INTERPRO"
            term="interpro"
            placeholder="e.g. IPR003959"
            species={localSpecies}
            router={router}
            example={
              <div>
                E.g.{" "}
                <Link href="/searchResult?searchTerm=IPR003959&searchType=interpro">
                  IPR003959
                </Link>
                ,{" "}
                <Link href="/searchResult?searchTerm=3593&searchType=interpro">
                  3593
                </Link>
              </div>
            }
          />
        </div>
        <div className={styles.column}>
          <BlastSearchField router={router} species={localSpecies} />
        </div>
      </div>
    </>
  );
}

type blastType = "blastn" | "blastp" | "blastx";

function BlastSearchField({
  router,
  species,
}: {
  router: NextRouter;
  species: string;
}) {
  const [blastSelected, setBlastSelected] = useState<blastType>("blastn");
  const [blastSeq, setBlastSeq] = useState("");
  const [blastError, setBlastError] = useState("");
  const [blastnSetting, setBlastnSetting] = useState({
    evalue: 10,
    percIdentity: 0,
    wordSize: 11,
    ungapped: false,
  });
  const [blastxSetting, setBlastxSetting] = useState({
    evalue: 0.001,
    percIdentity: 0,
    subjectCover: 0,
    queryCover: 0,
    minScore: 0,
  });
  const [blastpSetting, setBlastpSetting] = useState({
    evalue: 0.001,
    percIdentity: 0,
    subjectCover: 0,
    queryCover: 0,
    minScore: 0,
  });

  const launchBlastIfValid = (blast_type: blastType) => {
    if (blastSeq === "" || blastSeq.replaceAll(" ", "").length === 0) {
      setBlastError("No sequence inputed");
    } else if (isFasta(blastSeq) && containMultipleFasta(blastSeq)) {
      setBlastError("It should contain only 1 fasta");
    } else if (isFasta(blastSeq) && fastaIsWronglyFormatted(blastSeq)) {
      setBlastError("Fasta wrongly formated, where is the sequence ?");
    } else if (
      (blast_type === "blastn" || blast_type === "blastx") &&
      notDNA(blastSeq)
    ) {
      setBlastError("The Sequence/Fasta inputed doesn't look like DNA");
    } else if (blast_type === "blastp" && isDNA(blastSeq)) {
      setBlastError("It look like a DNA sequence, not AA");
    } else if (blast_type === "blastp" && notAA(blastSeq)) {
      setBlastError("The Sequence/Fasta inputed doesn't look like AA");
    } else {
      setBlastError("");
      // Track blast search using js because class based approach
      // doesn't detect the difference between different blast type
      // as it used the same button (even by dynamically changing
      // className to blastn/blastx/blastp).
      umamiEvent(`submit-${blast_type}`);
      blast(blast_type, blastSeq);
    }
  };

  const isFasta = (fasta: string) => {
    if (fasta.trim().startsWith(">")) return true;
    else return false;
  };

  const containMultipleFasta = (fasta: string) => {
    if (fasta.trim().split(">").length > 2) return true;
    else return false;
  };

  const fastaIsWronglyFormatted = (fasta: string) => {
    if (
      fasta.split("\n").length < 2 ||
      (fasta.split("\n").length === 2 &&
        fasta.split("\n")[1].replace(" ", "").length === 0) // Empty second line
    ) {
      return true;
    }
  };

  const getSeqFromPossibleFasta = (fasta: string) => {
    let lines = fasta.split("\n");
    if (lines[0].trim().startsWith(">")) {
      lines.shift();
    }
    const seq = lines.join("");
    return seq;
  };

  const notDNA = (fasta: string) => {
    const seq = getSeqFromPossibleFasta(fasta);
    const isNotDNA = /[^ATGCN\*]+/i.test(seq);
    return isNotDNA;
  };

  const isDNA = (fasta: string) => {
    return !notDNA(fasta);
  };

  const notAA = (fasta: string) => {
    const seq = getSeqFromPossibleFasta(fasta);
    const isNotAA = /[^ARNDCQEGHILKMFPSTWYV\*]+/i.test(seq);
    return isNotAA;
  };

  const blast = async (blast_type: blastType, blastSeq: string) => {
    let blastSetting;
    if (blast_type === "blastx") {
      blastSetting = blastxSetting;
    } else if (blast_type === "blastn") {
      blastSetting = blastnSetting;
    } else if (blast_type === "blastp") {
      blastSetting = blastpSetting;
    }

    const res = await fetch(apiUrl + "/search/blast/", {
      headers: {
        Accept: "text/plain",
        "Content-Type": "text/plain",
      },
      method: "POST",
      body: JSON.stringify({
        blast_type,
        seq: blastSeq,
        ...blastSetting,
      }),
    });
    const blast_id = await res.json();
    let query: any = { ...blast_id };
    if (species) query.species = species;
    router.push({
      pathname: "/searchBlastResult",
      query,
    });
  };

  const clearBlast = () => {
    setBlastSeq("");
    setBlastError("");
  };

  return (
    <>
      <label htmlFor="BLAST">BLAST</label>
      <div
        className={
          blastError.length === 0 ? styles.blastSearch : styles.blastSearchError
        }
      >
        <textarea
          wrap="soft"
          cols={70}
          rows={15}
          name="BLAST"
          id="BLAST"
          placeholder="Paste your fasta here"
          value={blastSeq}
          onChange={(e) => setBlastSeq(e.target.value)}
        />
        {blastError.length !== 0 && <p id="blastError">{blastError}</p>}
        <button
          id="clearBlast"
          className={`${styles.button} ${styles.secondary}`}
          onClick={clearBlast}
        >
          Clear Blast
        </button>
        <br />
        <div id="tab selector">
          <button
            className={`
            ${styles.button}
            ${blastSelected === "blastn" ? styles.tabSelected : ""}`}
            onClick={() => setBlastSelected("blastn")}
          >
            BLASTn
          </button>
          <button
            className={`
            ${styles.button}
            ${blastSelected === "blastx" ? styles.tabSelected : ""}`}
            onClick={() => setBlastSelected("blastx")}
          >
            BLASTx (Diamond)
          </button>
          <button
            className={`
            ${styles.button}
            ${blastSelected === "blastp" ? styles.tabSelected : ""}`}
            onClick={() => setBlastSelected("blastp")}
          >
            BLASTp (Diamond)
          </button>
        </div>
        {blastSelected === "blastn" ? (
          <div id="blastn options" className={styles.optionRow}>
            <span>
              <label htmlFor="evalue">Evalue</label>
              <input
                type="number"
                name="evalue"
                id="evalue"
                min="0"
                className={styles.option}
                value={blastnSetting.evalue}
                onChange={(e) =>
                  setBlastnSetting({
                    ...blastnSetting,
                    evalue: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="word_size">Word size</label>
              <input
                type="number"
                name="word_size"
                id="word_size"
                min="0"
                className={styles.option}
                value={blastnSetting.wordSize}
                onChange={(e) =>
                  setBlastnSetting({
                    ...blastnSetting,
                    wordSize: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="perc_identity">Minimum identity (%)</label>
              <input
                type="number"
                name="perc_identity"
                id="perc_identity"
                min="0"
                max="100"
                className={styles.option}
                value={blastnSetting.percIdentity}
                onChange={(e) =>
                  setBlastnSetting({
                    ...blastnSetting,
                    percIdentity: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="ungapped">Ungapped</label>
              <input
                type="checkbox"
                name="ungapped"
                id="ungapped"
                className={styles.option}
                checked={blastnSetting.ungapped}
                onChange={(e) =>
                  setBlastnSetting({
                    ...blastnSetting,
                    ungapped: !blastnSetting.ungapped,
                  })
                }
              />
            </span>
          </div>
        ) : blastSelected === "blastx" ? (
          <div id="blastx options" className={styles.optionRow}>
            <span>
              <label htmlFor="evalue">Evalue</label>
              <input
                type="number"
                name="evalue"
                id="evalue"
                min="1e-100"
                step="0.0001"
                className={styles.option}
                value={blastxSetting.evalue}
                onChange={(e) =>
                  setBlastxSetting({
                    ...blastxSetting,
                    evalue: Number(e.target.value.replace(/^0+/, "")),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="query_cover">Minimum Query cover (%)</label>
              <input
                type="number"
                name="query_cover"
                id="query_cover"
                min="0"
                max="100"
                className={styles.option}
                value={blastxSetting.queryCover}
                onChange={(e) =>
                  setBlastxSetting({
                    ...blastxSetting,
                    queryCover: Number(e.target.value.replace(/^0+/, "")),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="perc_identity">Minimum Identity (%)</label>
              <input
                type="number"
                name="perc_identity"
                id="perc_identity"
                min="0"
                max="100"
                className={styles.option}
                value={blastxSetting.percIdentity}
                onChange={(e) =>
                  setBlastxSetting({
                    ...blastxSetting,
                    percIdentity: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="min_score">Minimum Score</label>
              <input
                type="number"
                name="min_score"
                id="min_score"
                min="0"
                className={styles.option}
                value={blastxSetting.minScore}
                onChange={(e) =>
                  setBlastxSetting({
                    ...blastxSetting,
                    minScore: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="subject_cover">Minimum Subject cover (%)</label>
              <input
                type="number"
                name="subject_cover"
                id="subject_cover"
                min="0"
                max="100"
                className={styles.option}
                value={blastxSetting.subjectCover}
                onChange={(e) =>
                  setBlastxSetting({
                    ...blastxSetting,
                    subjectCover: Number(e.target.value),
                  })
                }
              />
            </span>
          </div>
        ) : (
          <div id="blastp option" className={styles.optionRow}>
            <span>
              <label htmlFor="evalue">Evalue</label>
              <input
                type="number"
                name="evalue"
                id="evalue"
                className={styles.option}
                value={blastpSetting.evalue}
                onChange={(e) =>
                  setBlastpSetting({
                    ...blastpSetting,
                    evalue: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="query_cover">Minimum Query cover (%)</label>
              <input
                type="number"
                name="query_cover"
                id="query_cover"
                min="0"
                max="100"
                className={styles.option}
                value={blastpSetting.queryCover}
                onChange={(e) =>
                  setBlastpSetting({
                    ...blastpSetting,
                    queryCover: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="perc_identity">Minimum Identity (%)</label>
              <input
                type="number"
                name="perc_identity"
                id="perc_identity"
                min="0"
                max="100"
                className={styles.option}
                value={blastpSetting.percIdentity}
                onChange={(e) =>
                  setBlastpSetting({
                    ...blastpSetting,
                    percIdentity: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="min_score">Minimum Score</label>
              <input
                type="number"
                name="min_score"
                id="min_score"
                min="0"
                className={styles.option}
                value={blastpSetting.minScore}
                onChange={(e) =>
                  setBlastpSetting({
                    ...blastpSetting,
                    minScore: Number(e.target.value),
                  })
                }
              />
            </span>
            <span>
              <label htmlFor="subject_cover">Minimum Subject cover (%)</label>
              <input
                type="number"
                name="subject_cover"
                id="subject_cover"
                min="0"
                max="100"
                className={styles.option}
                value={blastpSetting.subjectCover}
                onChange={(e) =>
                  setBlastpSetting({
                    ...blastpSetting,
                    subjectCover: Number(e.target.value),
                  })
                }
              />
            </span>
          </div>
        )}
        <br />
        <div className={styles.buttonRight}>
          <button
            id="submitBlast"
            className={styles.button}
            onClick={() => launchBlastIfValid(blastSelected)}
          >
            Search
          </button>
        </div>
      </div>
    </>
  );
}

interface SearchTextFieldProps {
  term: typeSearchType;
  label: string;
  placeholder?: string;
  example?: React.ReactNode;
  species?: string;
  router: NextRouter;
}

function SearchTextField(props: SearchTextFieldProps) {
  const { term, label, placeholder, example, species, router } = props;
  const [termValue, setTermValue, submitTerm] = useSearch(term, router);

  const submitOnEnter = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") submitTerm();
  };

  return (
    <>
      <label htmlFor={term}>{label}</label>
      <div className={styles.search}>
        <input
          type="text"
          name={term}
          id={term}
          placeholder={placeholder !== undefined ? placeholder : "placeholder"}
          value={termValue}
          onChange={(e) => setTermValue(e.target.value)}
          onKeyPress={submitOnEnter}
        />
        <button onClick={() => submitTerm(species)}>Search</button>
      </div>
      {example ? (
        <div className={styles.example}>
          <div className={styles.line}></div>
          <div className={styles.eg}>{example}</div>
        </div>
      ) : (
        <div className={styles.marginBottom}></div>
      )}
    </>
  );
}

function SpeciesSelector(props) {
  const { localSpecies, setLocalSpecies, ...otherProps } = props;
  return (
    <div {...otherProps}>
      <label htmlFor="searchPageSpeciesSelector">Species Selector</label>
      <SpeciesDropSelector
        id="searchPageSpeciesSelector"
        className={styles.speciesSelector}
        localSpecies={localSpecies}
        setLocalSpecies={setLocalSpecies}
      />
      <br />
      <br />
    </div>
  );
}
