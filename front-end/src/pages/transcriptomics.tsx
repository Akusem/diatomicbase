import React, { useState, useMemo, useEffect } from "react";
import { GetStaticProps } from "next";
import styles from "./transcriptomics.module.scss";
import Timer from "components/Timer";
import BioSampleSelector from "components/BioSampleSelector";
import useLoadingStatus from "hooks/useLoadingStatus";
import { useSpeciesSelected } from "hooks/useSpeciesContext";
import apiUrl from "utils/getApiUrl";
import pathJoin from "utils/pathJoin";
import Description from "components/Description";
import LinkArticle from "components/LinkArticle";
import Fuse from "fuse.js";
import Head from "next/head";
import ReactTable from "react-table-v6";

interface BioSamples
  extends Array<{
    id: number;
    name: string;
    run: string;
    Project_Id?: string;
  }> {}

interface BioProjectMetaData {
  BioProject: string;
  Description: string;
  Project_Id: string;
  Nb_runs: number;
  Nb_replicates: number;
  Design: string;
  Publication: string;
  Keywords: string;
  doi: string;
  Strain: string;
  BioSamples: BioSamples;
  Species: string;
}

export interface TranscriptomicsProps {
  initialRnaSeqMetaData: Array<BioProjectMetaData>;
}

export default function Transcriptomics({
  initialRnaSeqMetaData,
}: TranscriptomicsProps) {
  const [rnaSeqMetaData, setRnaSeqMetaData] = useState(initialRnaSeqMetaData);
  const [bioSamples, setBioSamples] = useState<BioSamples>([]);
  const [subset1, setSubset1] = useState<BioSamples>([]);
  const [subset2, setSubset2] = useState<BioSamples>([]);

  const [error, setError] = useState("");
  const [status, response, setRunId] = useLoadingStatus(
    `${apiUrl}/rna_seq/transcriptomics/`
  );
  const [timeStart, setTimeStart] = useState(-1);

  // Filter just the tableData corresponding to selected species if selected
  const speciesSelected = useSpeciesSelected();
  useEffect(() => {
    if (speciesSelected) {
      setRnaSeqMetaData(
        initialRnaSeqMetaData.filter((row) => {
          return row.Species === speciesSelected;
        })
      );
    } else {
      setRnaSeqMetaData(initialRnaSeqMetaData);
    }
  }, [initialRnaSeqMetaData, speciesSelected]);

  // Columns for React Table
  const columns = [
    {
      Header: "Select",
      accessor: "BioProject",
      Cell: (props) => (
        <input
          type="checkbox"
          name={props.value}
          id={props.value}
          onChange={onSelectClick}
        />
      ),
      width: 70,
    },
    { Header: "BioProject Accession", accessor: "BioProject", width: 200 },
    {
      Header: "Description",
      accessor: "Description",
      Cell: (props) => (
        <div className={styles.descriptionRow} id="description">
          {props.value}
        </div>
      ),
    },
    {
      id: "nbRuns",
      Header: "Nb runs",
      accessor: "Nb_runs",
      width: 70,
      Cell: (props) => <div id="nb-runs">{props.value}</div>,
    },
    {
      id: "nbReplicates",
      Header: "Nb replicates",
      accessor: "Nb_replicates",
      width: 120,
      Cell: (props) => <div id="nb-replicates">{props.value}</div>,
    },
    {
      Header: "SRA Experiments",
      accessor: "Design",
      Cell: (props) => (
        <div className={styles.descriptionRow} id="sra-experiment">
          {props.value}
        </div>
      ),
    },
    {
      id: "publication",
      Header: "Publication",
      accessor: (row) => {
        return { Publication: row.Publication, doi: row.doi };
      },
      Cell: (props) => (
        <div className={styles.descriptionRow} id="publication">
          <LinkArticle
            publication={props.value.Publication}
            doi={props.value.doi}
          />
        </div>
      ),
    },
  ];

  // BIOSELECTOR
  // Add or remove Biosample when selected
  const onSelectClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    const bioProjectMetaData = rnaSeqMetaData.filter((obj) => {
      return obj.BioProject === event.target.id;
    })[0];

    if (event.target.checked === true) {
      const bioSamplesToAdd = addProjectIdToBioSamples(bioProjectMetaData);
      setBioSamples([...bioSamples, ...bioSamplesToAdd]);
    } else if (event.target.checked === false) {
      removeDeselected(bioProjectMetaData);
    }
  };

  const addProjectIdToBioSamples = (bioProjectData: BioProjectMetaData) => {
    let bioSamples = bioProjectData.BioSamples;
    for (let bioSample of bioSamples) {
      bioSample.Project_Id = bioProjectData.Project_Id;
    }
    return bioSamples;
  };

  const removeDeselected = (bioProjectMetaData) => {
    const bioSamplesClean = bioSamples.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });
    const subset1Clean = subset1.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });
    const subset2Clean = subset2.filter((obj) => {
      return !bioProjectMetaData.BioSamples.find((objToRemove) => {
        return obj.id === objToRemove.id;
      });
    });

    if (bioSamples !== bioSamplesClean) {
      setBioSamples(bioSamplesClean);
    }
    if (subset1 !== subset1Clean) {
      setSubset1(subset1Clean);
    }
    if (subset2 !== subset2Clean) {
      setSubset2(subset2Clean);
    }
  };

  // SEARCH PART
  const [tableData, setTableData] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  // Use fuse to make fuzzy search in multiple column at the same time
  // UseMemo to not recompile fuse at each re-render
  const fuse = useMemo(() => {
    const fuseOptions = {
      keys: [
        "BioProject",
        "Description",
        "Design",
        "Publication",
        "Keywords",
        "Strain",
      ],
    };
    return new Fuse(rnaSeqMetaData, fuseOptions);
  }, [rnaSeqMetaData]);

  const searchTranscriptomic = () => {
    if (searchValue !== "") {
      setTableData(fuse.search(searchValue).map((obj) => obj.item));
    } else {
      setTableData([]);
    }
  };

  // LAUNCH RUN
  async function submitDiffExpr() {
    const selectError = isThereAnErrorInSelection();
    if (selectError !== "") {
      setError(selectError);
      return;
    }
    // Clean error
    setError("");

    const comparisonTsv = createComparisonTsv();
    const res = await fetch(apiUrl + "/rna_seq/transcriptomics", {
      headers: {
        Accept: "text/plain",
        "Content-Type": "text/plain",
      },
      method: "POST",
      body: JSON.stringify({ comparison: comparisonTsv }),
    });
    const json = await res.json();
    // Set submit error
    if (!res.ok) {
      setError(`Server error: ${json.detail}`);
    }
    setTimeStart(new Date().getTime());
    setRunId(json.id);
  }

  function isThereAnErrorInSelection() {
    if (subset1.length < 2 || subset2.length < 2) {
      return "Select atleast 2 biosamples by Subset";
    } else {
      return "";
    }
  }

  function createComparisonTsv() {
    let comparison_tsv = "project_id\trun\tsample_name\tsubset_group\n";
    subset1.forEach((bioSample) => {
      comparison_tsv += `${bioSample.Project_Id}\t${bioSample.run}\t${bioSample.name}\tsubset1\n`;
    });
    subset2.forEach((bioSample) => {
      comparison_tsv += `${bioSample.Project_Id}\t${bioSample.run}\t${bioSample.name}\tsubset2\n`;
    });
    return comparison_tsv;
  }

  // Redirect to idep
  useEffect(() => {
    if (status !== "done" || response.parameter === undefined) return;
    const idep = process.env.NEXT_PUBLIC_IDEP_URL || false;
    if (!idep) {
      setError("Configuration error, iDEP URL isn't setup");
      console.warn("IDEP URL isn't setup");
      return;
    }
    const fileName = response.parameter;
    // Redirect
    const a = document.createElement("a");
    a.href = pathJoin(idep, `?usePreComp=true&fileName=${fileName}`);
    a.click();
  }, [status, response]);

  return (
    <>
      <Head>
        <title>Transcriptomics</title>
      </Head>
      <div className={styles.container}>
        <Description className={styles.description} id="transcriptomics-help">
          <div style={{ textAlign: "center" }}>
            Select the bioproject you want to analyze by checking the box (like
            those
            <input type="checkbox"></input>). Samples corresponding to this
            bioproject will be placed in the Biosample box at the bottom of the
            page. <br /> Drag and drop the samples you want to compare from the
            biosample box to the subset1 and subset2 boxes. Submit and your
            results will appear in a short time.
          </div>
        </Description>
        <div className={styles.inlineFlex}>
          <p>Select the experiment:</p>
          <div className={styles.search}>
            <input
              type="text"
              id="search-bioproject"
              placeholder="Search"
              value={searchValue}
              onChange={(e) => {
                setSearchValue(e.target.value);
                searchTranscriptomic();
              }}
            />
          </div>
        </div>
        <ReactTable
          className={(styles.table, "-highlight")}
          defaultPageSize={25}
          columns={columns}
          data={tableData.length > 0 ? tableData : rnaSeqMetaData}
        />
        <p className={styles.subsetSentence}>Choose the subsets to compare: </p>
      </div>
      <BioSampleSelector
        submitDiffExpr={submitDiffExpr}
        bioSamples={bioSamples}
        setBioSamples={setBioSamples}
        subset1={subset1}
        setSubset1={setSubset1}
        subset2={subset2}
        setSubset2={setSubset2}
        error={error}
        disabled={
          status !== undefined && status !== "done" && status !== "error"
        }
      />
      {status !== undefined && <hr style={{ width: "70%" }} />}
      {status === "queueing" || status === "running" ? (
        <div className={styles.loadingMessage} id="loading-message">
          Preparing data for iDEP:
          <span>&nbsp;&nbsp;</span>
          <Timer startTime={timeStart} />
        </div>
      ) : status === "error" ? (
        // set result error
        <div
          className={`${styles.loadingMessage} ${styles.errorMessage}`}
          id="loading-error"
        >
          Run Error: {response.detail}
        </div>
      ) : (
        status === "done" && (
          <div className={styles.loadingMessage} id="loading-message">
            Redirecting to{" "}
            <a
              style={{ margin: "0 0.3em" }}
              href={pathJoin(
                process.env.NEXT_PUBLIC_IDEP_URL,
                `?usePreComp=true&fileName=${response.parameter}`
              )}
            >
              DiatOmicBase iDEP
            </a>{" "}
            instance...
          </div>
        )
      )}
    </>
  );
}

export interface exprAnalysisInterface {
  comparison: {
    [key: string]: string;
  };
  data: exprAnalysisDataInterface[];
}

export interface exprAnalysisDataInterface {
  Gene: string;
  baseMean: number;
  log2FoldChange: number;
  lfcSE: number;
  stat: number;
  pvalue: number;
  padj: number;
}

export const getStaticProps: GetStaticProps = async () => {
  const result = await fetch(apiUrl + "/resources/transcriptomics");
  const initialRnaSeqMetaData = await result.json();
  return {
    props: { initialRnaSeqMetaData },
  };
};
