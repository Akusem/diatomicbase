import React from "react";
import styles from "./transcriptomicsChoice.module.scss";
import { useRouter } from "next/router";
import Head from "next/head";
import umamiEvent from "utils/umami";
import useWindowSize from "hooks/useWindowSize";
import { useSpeciesSelected } from "hooks/useSpeciesContext";

export default function TranscriptomicsChoice() {
  const router = useRouter();
  const { width, height } = useWindowSize();
  const speciesSelected = useSpeciesSelected();

  // Adapt image scaling depending of size of screen
  // when the width > 1200, the 2 boxs are side by side, with idep having around 70% and heatmap 30%
  // when the width < 1200, the 2st box is below, so the image can use all the place
  let idepImgWidth;
  let preComputedComparisonImgWidth;
  if (width > 1200) {
    idepImgWidth = width * 0.6 - 100;
    preComputedComparisonImgWidth = width * 0.3 - 100;
  } else {
    idepImgWidth = width * 0.9 - 100;
    preComputedComparisonImgWidth = width * 0.9 - 100;
  }

  const redirectToPublicDataset = () => {
    // Redirect to speciesChoice if not species selected
    if (!speciesSelected)
      router.push("/speciesChoice/?redirectUrl=transcriptomics");
    else router.push(`/transcriptomics/?species=${speciesSelected}`);
  };

  const redirectToTranscriptomicsHeatmap = () => {
    // Redirect to speciesChoice if not species selected
    if (!speciesSelected)
      router.push("/speciesChoice/?redirectUrl=transcriptomicsHeatmap");
    else router.push(`/transcriptomicsHeatmap?species=${speciesSelected}`);
  };

  const redirectToIdep = () => {
    const idepUrl = process.env.NEXT_PUBLIC_IDEP_URL || false;
    if (!idepUrl) {
      console.warn("IDEP URL isn't setup");
      return;
    }
    umamiEvent("personal iDEP analysis");
    const a = document.createElement("a");
    a.href = idepUrl;
    a.click();
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Transcriptomics</title>
      </Head>
      <div className={styles.boxContainer}>
        {/* Transcriptomics */}
        <div className={styles.box1}>
          <h1 className={styles.textCenter}>
            Compare two conditions using iDEP
          </h1>
          <div className={styles.description}>
            Here you can analyze RNA-Seq data using the IDEP Shiny app. iDEP
            (integrated Differential Expression and Pathway analysis) is a
            web-based tool for analyzing RNA-seq data, available at{" "}
            <a href="http://ge-lab.org/idep/">http://ge-lab.org/idep/</a>. It
            reads in gene-level expression data (read counts or FPKM), performs
            exploratory data analysis (EDA), differential expression, pathway
            analysis, biclustering, and co-expression network analysis.
          </div>
          <br />
          <div className={styles.additionalInfo}>
            For more information and help about this tool, please look at:{" "}
            <a href="https://idepsite.wordpress.com/">
              https://idepsite.wordpress.com/
            </a>
          </div>
          <div className={styles.imageContainer}>
            <img
              src="/idep_description.jpg"
              alt="idep_description"
              style={{ maxWidth: idepImgWidth, maxHeight: height }}
            />
          </div>
          <blockquote className={styles.citation}>
            If you use this analytic module, please cite: Ge, S.X., Son, E.W. &
            Yao, R. iDEP: an integrated web application for differential
            expression and pathway analysis of RNA-Seq data. BMC Bioinformatics
            19, 534 (2018).{" "}
            <a href="https://doi.org/10.1186/s12859-018-2486-6">
              https://doi.org/10.1186/s12859-018-2486-6
            </a>
          </blockquote>
          <div className={styles.cardContainer}>
            <section className={styles.card2} onClick={redirectToPublicDataset}>
              Analyse public RNA-Seq dataset
            </section>
            <section className={styles.card2} onClick={redirectToIdep}>
              Analyse your own data <br />
            </section>
          </div>
        </div>

        {/* Heatmap */}
        <div className={styles.box2}>
          <h1 className={styles.textCenter}>
            Visualize multiple precomputed comparisons for a subset of genes
          </h1>
          <div className={styles.imageContainer}>
            <img
              src="/heatmap_example.png"
              alt="heatmap_example"
              style={{
                maxWidth: preComputedComparisonImgWidth,
                maxHeight: height,
              }}
            />
          </div>
          <div
            className={
              // When the width > 1200, the 2 boxs are side by side, and we place the heatmap button
              // by using absolute position so it's align to iDEP button.
              // If the width < 1200, the 2st box is below, so we remove the absolute position,
              // to avoid having the button on the image.
              width > 1200 ? styles.cardContainerHeatmap : styles.cardContainer
            }
          >
            <section
              className={styles.card}
              onClick={redirectToTranscriptomicsHeatmap}
            >
              Choose comparisons and upload your gene subset
            </section>
          </div>
        </div>
      </div>
    </div>
  );
}
