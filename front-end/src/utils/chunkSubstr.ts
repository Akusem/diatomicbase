export default function chunkSubstr(str, size) {
  const chunks = [];
  for (let i = 0, charsLength = str.length; i < charsLength; i += size) {
    chunks.push(str.substring(i, i + size));
  }
  return chunks;
}
