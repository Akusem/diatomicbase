export default function download(url: string, newTab = false) {
  const a = document.createElement("a");
  a.href = url;
  a.download = url.split("/").pop();
  if (newTab) a.target = "_blank";
  document.body.appendChild(a);
  a.click();
  document.body.removeChild(a);
}
