/**
 * As many db name are stored lowercase, this function return the correct case.
 * @param name name to format to have the correct case
 * @returns name with correct case
 */
export function convertCase(name: string) {
  switch (name) {
    case "ncbi":
      return "NCBI";
    case "uniprotkb":
      return "UniProtKB";
    case "symbol":
      return "Symbol";
    case "plaza":
      return "PLAZA";
    case "embl":
      return "EMBL";
    case "ensembl":
      return "Ensembl";
    case "string":
      return "STRING";
    default:
      return name;
  }
}

export function getIdHrefToDb(idType: string, id: string): string {
  switch (idType) {
    case "uniprotkb":
      return `https://www.uniprot.org/uniprot/${id}`;
    case "embl":
      return `https://www.ebi.ac.uk/ena/browser/view/${id}`;
    case "KEGG":
      return `https://www.genome.jp/dbget-bin/www_bget?pathway+${id}`;
    case "go":
      return `https://www.ebi.ac.uk/QuickGO/term/${id}`;
    case "ncbi":
      return `https://www.ncbi.nlm.nih.gov/gene/${id}`;
    case "InterPro":
      return `https://www.ebi.ac.uk/interpro/entry/InterPro/${id}`;
    case "SUPFAM":
      const clean_id = id.replace("SSF", "");
      return `http://supfam.org/SUPERFAMILY/cgi-bin/scop.cgi?sunid=${clean_id}`;
    case "Pfam":
      return `https://pfam.xfam.org/family/${id}`;
    case "PROSITE":
      return `https://prosite.expasy.org/${id}`;
    case "TIGRFAMs":
      return `https://www.ncbi.nlm.nih.gov/Structure/cdd/${id}`;
    case "Gene3D":
      return `http://www.cathdb.info/version/latest/superfamily/${id}`;
    case "PANTHER":
      return `http://www.pantherdb.org/panther/family.do?clsAccession=${id}`;
    case "SMART":
      return `http://smart.embl-heidelberg.de/smart/do_annotation.pl?ACC=${id}`;
    case "HAMAP":
      return `https://hamap.expasy.org/signature/${id}`;
    case "PRINTS":
      return `http://130.88.97.239/cgi-bin/dbbrowser/sprint/searchprintss.cgi?prints_accn=${id}&display_opts=Prints&category=None&queryform=false&regexpr=off`;
    case "CDD":
      return `https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=${id}`;
    case "plaza":
      return `https://bioinformatics.psb.ugent.be/plaza/versions/plaza_diatoms_01/genes/view/${id}`;
    case "ensembl":
      return `https://protists.ensembl.org/Gene/Summary?g=${id}`;
    case "string":
      return `https://string-db.org/network/${id}`;
    default:
      return "";
  }
}
