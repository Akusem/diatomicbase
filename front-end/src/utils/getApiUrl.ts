// For docker-compose, see issue #38:
const apiUrl = process.env.API_URL || process.env.NEXT_PUBLIC_API_URL;

export const webSocketUrl = apiUrl.replace("https", "ws").replace("http", "ws");

export default apiUrl
